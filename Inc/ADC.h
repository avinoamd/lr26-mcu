/*
 * ADC.h
 *
 *  Created on: Jul 6, 2020
 *      Author: avinoam.danieli
 */

#ifndef ADC_H_
#define ADC_H_

#include "CLI.h"

int16_t AdcChannelConversion(ADC_HandleTypeDef *hadc, uint32_t channel, uint32_t samplingTime, uint32_t timeout, int32_t *value);
int32_t Get_Tmpr(CLI_command_T* command);
int32_t CPUTmpr(CLI_command_T* command);
int32_t IntVref(CLI_command_T* command);
int32_t brkcurr(CLI_command_T* command);
int32_t vbatt(CLI_command_T* command);
int32_t vsw(CLI_command_T* command);
int32_t p5v(CLI_command_T* command);
int32_t p24v(CLI_command_T* command);
int32_t p3v3(CLI_command_T* command);
int32_t vswcurr(CLI_command_T* command);
int32_t vcharge(CLI_command_T* command);
int32_t vbrake(CLI_command_T* command);
int32_t  GetV(CLI_command_T* command);
int32_t  GetC(CLI_command_T* command);
int32_t getvbatt(float *VSENSE);
int32_t getvsw(float *VSENSE);
int32_t getp5v(float *VSENSE);
int32_t getp24v(float *VSENSE);
int32_t getp3v3(float *VSENSE);
int32_t getvswcurr(float *VSENSE);
int32_t getvcharge(float *VSENSE);
int32_t diff(CLI_command_T* command);

uint16_t plcvbatt(void);
uint16_t plcvsw(void);
uint16_t plcvswcurr(void);
uint16_t plcvcharge(void);
uint16_t plcp3v3(void);
uint16_t plcp24v(void);
uint16_t plcp5v(void);
uint16_t plcTMPR(void);
int32_t  ADC_DMA_Init(void);


#define ADC_CHANNEL_TMPRINT        ((uint32_t)ADC_CHANNEL_16)
#define V25 0.76
#define Avg_Slope 0.0025
#define Vref 2.048

#define RES_RATIO				( 499.0 / 29900.0 )		// factor resistors 0.01668952138
#define RES_RATIO_5V			(1.0/4.92)
#define RES_RATIO_24V			( 1.0/25.3 )

extern uint16_t ADC_DMA_Buffer[15];

#define INTR_VBATT			0
#define DMA_VBATT				1
#define DMA_VSW				2
#define INTR_TMPR				3
#define DMA_VCHARGER			4
#define Imon					5
#define INTR_VREF				6
#define DMA_P24V				7
#define DMA_BRKCURR			8
#define INTR_VBATT2			9
#define DMA_P5V				10
#define DMA_VBrake			11


#endif /* ADC_H_ */
