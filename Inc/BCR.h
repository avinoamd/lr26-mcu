/*
 * BCR.h
 *
 *  Created on: Aug 25, 2020
 *      Author: avinoam.danieli
 */

#ifndef BCR_H_
#define BCR_H_

#define BCRRXLEN 128
#define MASK (BCRRXLEN -1)
extern uint8_t BcrRxBuff[BCRRXLEN + 2];
extern uint8_t *start;
extern uint8_t *end;

#include "CLI.h"

int32_t BCR_getdata(uint8_t *buff, int32_t size);
int32_t BCR_init(void);
int32_t BCR_en(void);
int32_t BCR_CLI_getdata(CLI_command_T* command);
int32_t BCR_dis(void);
int32_t BCR_CLI_en(CLI_command_T* command);
int32_t BCR_Clear();

#endif /* BCR_H_ */
