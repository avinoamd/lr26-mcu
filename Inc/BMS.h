/*
 * BMS.h
 *
 *  Created on: Sep 5, 2020
 *      Author: avinoam.danieli
 */

#ifndef BMS_H_
#define BMS_H_

uint16_t BMS_Data(uint8_t addr, uint8_t *MSB, uint8_t *LSB);
uint16_t BMS_DAStatus2(int32_t first, int32_t addr, uint8_t *MSB, uint8_t *LSB);
void i2cerrorhandler(int32_t num, char const * FILE, int LINE, char const * PRETTY_FUNCTION);
#include "cli.h"
int32_t BMS_CLI(CLI_command_T* command);

#endif /* BMS_H_ */
