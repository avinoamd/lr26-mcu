/*
 * Battery.h
 *
 *  Created on: 12 Jan 2021
 *      Author: avinoam.danieli
 */

#ifndef BATTERY_H_
#define BATTERY_H_

extern int32_t BMSPresent;
extern int32_t BattLevel;

int32_t Battery_Status(void);
int32_t BMS_Detect(void);
int32_t SOC_CLI(CLI_command_T* command);

#endif /* BATTERY_H_ */
