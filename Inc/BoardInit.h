/*
 * BoardInit.h
 *
 *  Created on: Aug 6, 2020
 *      Author: avinoam.danieli
 */

#ifndef BOARDINIT_H_
#define BOARDINIT_H_

void BoardInit(void);
int32_t CheckBatt(int32_t print);
void PrechargeOff(void);
int32_t PrechargeOn(void);
void VSWOff(void);
void DCDC24VOff();
void P5VOff();
void P24VOff();
void PullerOff();
void BrakeOff(void);
void ChargeOff();
void DriverOff(void);
void LockerOff(void);

int32_t VSWOn(void);
int32_t DCDC24VOn(void);
int32_t P5VOn(void);
int32_t PullerOn(void);
int32_t BrakeOn(void);
int32_t P24VOn(void);
int32_t ChargeOn();
int32_t DriverOn(void);
int32_t LockerOn(void);
int32_t ShutDown(void);
int32_t OnMode(void);
int32_t LowPowerMode(void);

extern int32_t PowerState;

#define ADC_CHANNEL_TMPRINT        ((uint32_t)ADC_CHANNEL_16)
#define PRECHARGE_LIMIT    33    	// 1V
#define PRECHARGEON 			1
#define VSWON 					2
#define DCDC24V 				4
#define P5V						8
#define P24V					16
#define P3V3					32
#define BRAKE					64
#define POWEROFF 0
#define BATT_LOW_LIMIT			(int32_t)(24.0 * RES_RATIO * 2000.0)
#define BATT_HIGH_LIMIT			(int32_t)(59.0 * RES_RATIO * 2000.0)
#define DCDC24V_LOW_LIMIT		(int32_t)((22.0-2.0) * (1.0/25.3) * 2000.0)
#define DCDC24V_LOW_LIMIT_F	(22.0-2.0)
#define DCDC24V_HIGH_LIMIT		(int32_t)((22.0+2.0) * (1.0/25.3) * 2000.0)
#define DCDC24V_HIGH_LIMIT_F	(22.0+2.0)
#define P5V_LOW_LIMIT			(int32_t)((5.0-0.5) * (1.0/4.92) * 2000.0)
#define P5V_LOW_LIMIT_F			(5.0-0.5)
#define P5V_HIGH_LIMIT			(int32_t)((5.0+0.5) * (1.0/4.92) * 2000.0)
#define P5V_HIGH_LIMIT_F		(5.0+0.5)
#define P3V3_LOW_LIMIT			3000
#define P3V3_HIGH_LIMIT			3600

#define VSW_OFF					(int32_t)(29.0 * RES_RATIO * 2000.0)

#endif /* BOARDINIT_H_ */
