/**
  ******************************************************************************
  * File Name          : cli.h
  * Author             : Ariel
  * Date               : 28/07/2014 13:06:09
  * Description        : This file provides code for protocol
  ******************************************************************************
  *
  * COPYRIGHT(c) 2014 V-Gen
  *
  *
  *
  ******************************************************************************
  */

#ifndef ___CLI_H___
#define ___CLI_H___

#include <stdint.h>
//#include "laser.h"

#define COMMAND_MAX_LENGTH 64
#define ARGUMENT_MAX_LENGTH 64
#define CLI_COMMANDS        200

typedef enum {

  SETPAR 	= 0,
  GETPAR 	,
  HELP		,
  LOCALHELP

}
ArgType_T;

typedef enum
{
   OKr                    =0,
   COMMAND_NOT_FOUND     ,
   NO_AUTHORIZATION     ,
   OUT_OF_LIMITS        ,
   CALL_ERROR		,
   NO_permission		, //repeated	,
   UNKNOWN		 //7


}CLIRetCode_t;


typedef struct
{
    char        sCommand  [COMMAND_MAX_LENGTH];
    char        sParameter[ARGUMENT_MAX_LENGTH];
    ArgType_T   ArgType;
}CLI_command_T;

typedef enum {

  CLIENT 	= 1,
  TECHNICIAN 	,
  RND,
  DNP
}
PERMIT_LEVEL_T;

typedef struct
{
    /// command name, as it should be typed on CLI prompt.
    char    *pName;

    /// pointer to CLI command handler function.
    int32_t     (*pHandler)(CLI_command_T* command);

    /// max number of command parameters
    int     argCount;

    /// which condition command can execute
    PERMIT_LEVEL_T    permission;


    /// a short command description string to be dislayed by "help" command.
    char    *pDescription;

} CLI_COMMAND_LIST_T;

//CLIRetCode_t CLI_ConvertStringToCommand(char* sStringRecive,CLI_Exe_command_T* pCommand );
CLIRetCode_t CLI_ExecutCommand(CLI_command_T* pCommand ,  CLI_COMMAND_LIST_T const * pCommandList);
//void CLI_init(CLI_COMMAND_LIST_T const* *pCommandList);
void CLIErrorHandler(CLIRetCode_t errorFromList);
void errorHandler(int32_t num, char const * FILE, int LINE, char const * PRETTY_FUNCTION);
//int32_t LoadDefaultVal (CLI_command_T* command);
//int32_t ArmLaser(CLI_command_T* command);

int32_t	help		(CLI_command_T* command)	;

extern const CLI_COMMAND_LIST_T gCliCommandsList[] /*@ "FLASH"*/ ;
//extern char msg[MSG_MAX_LEN];


//int 	SerialNumber 		(CLI_Exe_command_T* command)	;
//int32_t	GetVersion      	(CLI_Exe_command_T* command)	;
//int 	Debug	        	(CLI_Exe_command_T* command)	;
//int		UpdateParams    	(CLI_Exe_command_T* command)	;
//int 	MCU_Reset 			(CLI_Exe_command_T* command)	;
//int 	MCU_BOOT 			(CLI_Exe_command_T* command)	;
//int 	IO_Test 			(CLI_Exe_command_T* command)	;
//int     LogData                 (CLI_Exe_command_T* command)    ;
//int     LogFullData             (CLI_Exe_command_T* command)    ;
//int     LogTimeDiff             (CLI_Exe_command_T* command)    ;
//int     LogsEraseAll            (CLI_Exe_command_T* command)    ;
//int     HardFaultRetAddress     (CLI_Exe_command_T* command)    ;
//int     TestAll                 (CLI_Exe_command_T* command)    ;
//int     TestIO                  (CLI_Exe_command_T* command)    ;
//void transmitMemBlock(unsigned char *baseAddr, int size);

#define BYTE_TO_BINARY_PATTERN %c%c%c%c%c%c%c%c
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0')


#endif
