/*
 * CLIcmnds.h
 *
 *  Created on: Jul 1, 2020
 *      Author: avinoam.danieli
 */

#ifndef CLICMNDS_H_
#define CLICMNDS_H_

#include "CLI.h"
int32_t GetVersion(CLI_command_T* command);
int32_t Uptime(CLI_command_T* command);
int32_t help(CLI_command_T* command);
int32_t cli_powermode(CLI_command_T* command);

int32_t gettxbuffer(void);
int32_t sendtxbuffer(void);

void datetimeformain(void);
void versionformain(void);


#endif /* CLICMNDS_H_ */
