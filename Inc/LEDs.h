/*
 * LEDs.h
 *
 *  Created on: Jul 6, 2020
 *      Author: avinoam.danieli
 */

#ifndef LEDS_H_
#define LEDS_H_

typedef struct
{
	int32_t num;
	int32_t power;
	int32_t on;
	int32_t off;
	int32_t cnt;
} typeLEDs;

int32_t LEDcmd(CLI_command_T* command);
void syncleds(void);

#endif /* LEDS_H_ */
