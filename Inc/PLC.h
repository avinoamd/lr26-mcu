/*
 * PLC.h
 *
 *  Created on: Aug 17, 2020
 *      Author: avinoam.danieli
 */

#ifndef PLC_H_
#define PLC_H_

#define PLCINLEN 128
#define PLCOUTLEN 128
extern uint8_t PLCinbuff[PLCINLEN];					// for PLC
extern uint8_t PLCoutbuff[PLCOUTLEN];					// for PLC
extern int PLCcnt;
extern int PLCcntOut;

void uS_Delay(uint32_t us);
#include "CLI.h"
int32_t plc_timeout(CLI_command_T* command);

#endif /* PLC_H_ */
