/*
 * PushButton.h
 *
 *  Created on: Sep 21, 2020
 *      Author: avinoam.danieli
 */

#ifndef PUSHBUTTON_H_
#define PUSHBUTTON_H_

typedef enum
{
  ON = 0,
  ON1,
  W1,
  W2,
  LPM,
  LPM1,
  W3,
  W4,
  OFF,
  SHORTPRESS,
  LONGPRESS
} Power_State;;

extern TaskHandle_t xTaskToNotifyPBTask;

#endif /* PUSHBUTTON_H_ */
