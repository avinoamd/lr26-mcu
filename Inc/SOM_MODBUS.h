/*
 * SOM_MODBUS.h
 *
 *  Created on: Aug 17, 2020
 *      Author: avinoam.danieli
 */

#ifndef SOM_MODBUS_H_
#define SOM_MODBUS_H_

#define SOMINLEN 128
//#define PLCOUTLEN 128
extern uint8_t SOMinbuff[SOMINLEN];							// for SOM
//extern uint8_t PLCoutbuff[];					// for PLC
extern int32_t SOMrxflg;
extern int SOMcnt;

void uSecDelay(uint16_t us);
void SOM_MODBUS(void);

#endif /* SOM_MODBUS_H_ */
