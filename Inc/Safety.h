/*
 * Safety.h
 *
 *  Created on: Aug 18, 2020
 *      Author: avinoam.danieli
 */

#ifndef SAFETY_H_
#define SAFETY_H_

void SafetyRST(void *argument);
void SafetySET(void *argument);
int32_t Safety_SET(void);
int32_t Safety_RST(void);
int32_t SafetySETCLI(CLI_command_T* command);
int32_t SafetyRSTCLI(CLI_command_T* command);
int32_t SafetyAlertCLI(CLI_command_T* command);
void safety_init(void);

#endif /* SAFETY_H_ */
