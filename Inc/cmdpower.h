/*
 * cmdpower.h
 *
 *  Created on: 12 Aug 2020
 *      Author: avinoam.danieli
 */

#ifndef CMDPOWER_H_
#define CMDPOWER_H_

int32_t prechrg_en(CLI_command_T* command);
int32_t vsw_en(CLI_command_T* command);
int32_t DCDC24V_en(CLI_command_T* command);
int32_t P5V_en(CLI_command_T* command);
int32_t P24V_en(CLI_command_T* command);
int32_t Puller_en(CLI_command_T* command);
int32_t brake_en(CLI_command_T* command);
int32_t charge_en(CLI_command_T* command);
int32_t drv_en(CLI_command_T* command);
int32_t locker_en(CLI_command_T* command);
int32_t som_rst(CLI_command_T* command);
int32_t getkey(CLI_command_T* command);
int32_t getsto(CLI_command_T* command);


#endif /* CMDPOWER_H_ */
