/*
 * eeprom_utils.h
 *
 *  Created on: Oct 8, 2020
 *      Author: avinoam.danieli
 */

#ifndef INC_EEPROM_UTILS_H_
#define INC_EEPROM_UTILS_H_


void load_params(int32_t update);
void ee_write_param(uint16_t addr, uint16_t data);
int32_t params_update(CLI_command_T* command);


#endif /* INC_EEPROM_UTILS_H_ */
