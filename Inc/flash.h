/*
 * flash.h
 *
 *  Created on: 25 Oct 2020
 *      Author: avinoam.danieli
 */

#ifndef FLASH_H_
#define FLASH_H_

int32_t flash_init(int32_t force);

#endif /* FLASH_H_ */
