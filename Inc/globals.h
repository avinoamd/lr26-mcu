/*
 * globals.h
 *
 *  Created on: Jul 1, 2020
 *      Author: avinoam.danieli
 */

#ifndef GLOBALS_H_
#define GLOBALS_H_

#include "cmsis_os.h"

extern ADC_HandleTypeDef hadc1;
extern ADC_HandleTypeDef hadc2;
extern ADC_HandleTypeDef hadc3;

extern I2C_HandleTypeDef hi2c1;

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart3;				// terminal CLI
extern UART_HandleTypeDef huart4;				// PLC
extern UART_HandleTypeDef huart5;
extern UART_HandleTypeDef huart2;
extern UART_HandleTypeDef huart6;

extern IWDG_HandleTypeDef hiwdg;

extern char msg[];

extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim2;					// microsecond delay timer, 1 MHz
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim4;
extern TIM_HandleTypeDef htim5;
extern TIM_HandleTypeDef htim6;
extern TIM_HandleTypeDef htim10;
extern TIM_HandleTypeDef htim11;
extern TIM_HandleTypeDef htim13;
extern TIM_HandleTypeDef htim14;

extern I2C_HandleTypeDef hi2c1;
extern I2C_HandleTypeDef hi2c2;
extern I2C_HandleTypeDef hi2c3;

extern TIM_OC_InitTypeDef sgConfigOC;
extern osMessageQueueId_t LEDqHandle;

extern osSemaphoreId_t BSem01Handle;
extern osSemaphoreId_t BSemUART1_RX_ISRHandle;
extern osMessageQueueId_t PLCtoBCRHandle;

extern osTimerId_t SAFETY_SETHandle;
extern osTimerId_t SAFETY_RSTHandle;
extern osTimerId_t PLC_TIMERHandle;

extern DAC_HandleTypeDef hdac;

#endif /* GLOBALS_H_ */
