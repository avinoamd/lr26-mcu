/*
 * lsm6ds3.h
 *
 *  Created on: Sep 8, 2020
 *      Author: avinoam.danieli
 */

#ifndef LSM6DS3_CLI_H_
#define LSM6DS3_CLI_H_

int32_t IMU_CLI_getdata(CLI_command_T* command);
int32_t IMU_init(void);

extern uint8_t IMU_Data[16];			// 14
int32_t IMU_Getdata();


#endif /* LSM6DS3_CLI_H_ */
