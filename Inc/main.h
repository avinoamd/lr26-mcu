/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "globals.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define STO_Pin GPIO_PIN_5
#define STO_GPIO_Port GPIOF
#define ADC_BRAKE_CURR_C_Pin GPIO_PIN_0
#define ADC_BRAKE_CURR_C_GPIO_Port GPIOC
#define ADC_VBAT_Pin GPIO_PIN_0
#define ADC_VBAT_GPIO_Port GPIOA
#define ADC_VSW_Pin GPIO_PIN_1
#define ADC_VSW_GPIO_Port GPIOA
#define ADC_CURR_C_Pin GPIO_PIN_2
#define ADC_CURR_C_GPIO_Port GPIOA
#define ADC_VCHARGER_C_Pin GPIO_PIN_7
#define ADC_VCHARGER_C_GPIO_Port GPIOA
#define ADC_P24V_C_Pin GPIO_PIN_0
#define ADC_P24V_C_GPIO_Port GPIOB
#define PRECHRG_FBO_Pin GPIO_PIN_12
#define PRECHRG_FBO_GPIO_Port GPIOF
#define P24V_PG_Pin GPIO_PIN_13
#define P24V_PG_GPIO_Port GPIOF
#define VSW_FAULT_Pin GPIO_PIN_14
#define VSW_FAULT_GPIO_Port GPIOF
#define P3V3_PG_Pin GPIO_PIN_15
#define P3V3_PG_GPIO_Port GPIOF
#define LED0_Pin GPIO_PIN_0
#define LED0_GPIO_Port GPIOG
#define LED1_Pin GPIO_PIN_1
#define LED1_GPIO_Port GPIOG
#define MCU_M1_Pin GPIO_PIN_8
#define MCU_M1_GPIO_Port GPIOE
#define SERVICE_OUT_Pin GPIO_PIN_10
#define SERVICE_OUT_GPIO_Port GPIOE
#define SERVICE_IN_Pin GPIO_PIN_12
#define SERVICE_IN_GPIO_Port GPIOE
#define MCU_M2_Pin GPIO_PIN_13
#define MCU_M2_GPIO_Port GPIOE
#define MCU_M3_Pin GPIO_PIN_14
#define MCU_M3_GPIO_Port GPIOE
#define MCU_M4_Pin GPIO_PIN_15
#define MCU_M4_GPIO_Port GPIOE
#define SW1_Pin GPIO_PIN_12
#define SW1_GPIO_Port GPIOB
#define SW2_Pin GPIO_PIN_13
#define SW2_GPIO_Port GPIOB
#define SW3_Pin GPIO_PIN_14
#define SW3_GPIO_Port GPIOB
#define SW4_Pin GPIO_PIN_15
#define SW4_GPIO_Port GPIOB
#define DRV_OUT_Pin GPIO_PIN_8
#define DRV_OUT_GPIO_Port GPIOD
#define PULLER_OUT_Pin GPIO_PIN_9
#define PULLER_OUT_GPIO_Port GPIOD
#define STP_OUT_Pin GPIO_PIN_10
#define STP_OUT_GPIO_Port GPIOD
#define SOM_nMR_Pin GPIO_PIN_11
#define SOM_nMR_GPIO_Port GPIOD
#define BREAKESPWM_Pin GPIO_PIN_12
#define BREAKESPWM_GPIO_Port GPIOD
#define LED2_Pin GPIO_PIN_2
#define LED2_GPIO_Port GPIOG
#define IMU_INT_Pin GPIO_PIN_3
#define IMU_INT_GPIO_Port GPIOG
#define VSW_EN_Pin GPIO_PIN_4
#define VSW_EN_GPIO_Port GPIOG
#define PRECHRG_EN_Pin GPIO_PIN_5
#define PRECHRG_EN_GPIO_Port GPIOG
#define P24V_EN_Pin GPIO_PIN_6
#define P24V_EN_GPIO_Port GPIOG
#define DISCHRG_Pin GPIO_PIN_7
#define DISCHRG_GPIO_Port GPIOG
#define P5V_EN_Pin GPIO_PIN_8
#define P5V_EN_GPIO_Port GPIOG
#define BarCode_Reader_Tx_Pin GPIO_PIN_12
#define BarCode_Reader_Tx_GPIO_Port GPIOC
#define BarCode_Reader_Rx_Pin GPIO_PIN_2
#define BarCode_Reader_Rx_GPIO_Port GPIOD
#define DRV_EN_Pin GPIO_PIN_3
#define DRV_EN_GPIO_Port GPIOD
#define PULLER_EN_Pin GPIO_PIN_4
#define PULLER_EN_GPIO_Port GPIOD
#define STP_EN_Pin GPIO_PIN_7
#define STP_EN_GPIO_Port GPIOD
#define CHARGE_DIS_Pin GPIO_PIN_9
#define CHARGE_DIS_GPIO_Port GPIOG
#define SAFETY_SET_Pin GPIO_PIN_10
#define SAFETY_SET_GPIO_Port GPIOG
#define SAFETY_RST_Pin GPIO_PIN_11
#define SAFETY_RST_GPIO_Port GPIOG
#define SAFETY_ALERT_Pin GPIO_PIN_12
#define SAFETY_ALERT_GPIO_Port GPIOG
#define SAFETY_ALERT_EXTI_IRQn EXTI15_10_IRQn
#define MCU_HOLD_Pin GPIO_PIN_13
#define MCU_HOLD_GPIO_Port GPIOG
#define KEY_MCU_Pin GPIO_PIN_14
#define KEY_MCU_GPIO_Port GPIOG
#define KEY_MCU_EXTI_IRQn EXTI15_10_IRQn
#define PBtoMCU_Pin GPIO_PIN_15
#define PBtoMCU_GPIO_Port GPIOG
#define PBtoMCU_EXTI_IRQn EXTI15_10_IRQn
#define DCDC24V_EN_Pin GPIO_PIN_5
#define DCDC24V_EN_GPIO_Port GPIOB
void   MX_IWDG_Init(void);
/* USER CODE BEGIN Private defines */
#define ADC_VBAT_C 			ADC_CHANNEL_0
#define ADC_VBAT_CV 			&hadc2
#define ADC_VSW_C 			ADC_CHANNEL_1
#define ADC_VSW_CV 			&hadc3
#define ADC_CURR_C 			ADC_CHANNEL_2
#define ADC_CURR_CV 			&hadc3
#define ADC_VCHARGER_C 		ADC_CHANNEL_7
#define ADC_VCHARGER_CV 	&hadc2
#define ADC_P24V_C 			ADC_CHANNEL_8
#define ADC_P24V_CV 			&hadc2
#define ADC_P5V_C 			ADC_CHANNEL_9
#define ADC_P5V_CV 			&hadc2
#define ADC_BRAKE_CURR_C 	ADC_CHANNEL_10
#define ADC_BRAKE_CURR_CV	&hadc3
#define ADC_VBRAKE_C 		ADC_CHANNEL_11
#define ADC_VBRAKE_CV		&hadc3

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
