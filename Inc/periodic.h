/*
 * periodic.h
 *
 *  Created on: 22 Dec 2020
 *      Author: avinoam.danieli
 */

#ifndef PERIODIC_H_
#define PERIODIC_H_

int32_t Brake_Current(CLI_command_T* command);
void measure_brake_zero(void);
extern uint16_t Bcurrentzero;


#endif /* PERIODIC_H_ */
