/*
 * print.h
 *
 *  Created on: Sep 14, 2020
 *      Author: avinoam.danieli
 */

#ifndef PRINT_H_
#define PRINT_H_

#include "CLI.h"

#define MSGUART3LEN 200

extern char Msguart3[MSGUART3LEN];
extern char Msguart3[MSGUART3LEN];

int32_t pplc_uart3(CLI_command_T* command);
int32_t pbms_uart3(CLI_command_T* command);
int32_t pplc_swo(CLI_command_T* command);
int32_t plc_cnt(CLI_command_T* command);
int32_t psom_uart3(CLI_command_T* command);
int32_t psom_swo(CLI_command_T* command);
int32_t som_cnt(CLI_command_T* command);


#define EEPROM_LEN		10
extern uint16_t EEPROM_Array[EEPROM_LEN];

#define PLCPrintToUart3		EEPROM_Array[0]
#define PLCPrintToSWO		EEPROM_Array[1]
#define SOMPrintToUart3		EEPROM_Array[2]
#define SOMPrintToSWO		EEPROM_Array[3]
#define BMSPrintToUart3		EEPROM_Array[4]
#define BrakeCurrent			EEPROM_Array[5]
#define PLCTimeout			EEPROM_Array[6]
#define EESpare2				EEPROM_Array[7]
#define EESpare3				EEPROM_Array[8]
#define EESpare4				EEPROM_Array[9]

#endif /* PRINT_H_ */
