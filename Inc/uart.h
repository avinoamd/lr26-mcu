/**
  ******************************************************************************
  * File Name          : UART.h
  * Date               :
  * Description        :
  *
  ******************************************************************************
  *
  * COPYRIGHT(c) 2014 V-Gen
  *
  ******************************************************************************
  */

#ifndef __UART
#define __UART

#define INUSE	1
#define FREE	0
#define TXMSG_MAX_LEN 512
#define RXBUFF_MAX_LEN 16
#define MSG_MAX_LEN 256

//extern char rxbuff[RXBUFF_MAX_LEN];						// rx buffer
extern char txbuff[TXMSG_MAX_LEN];							// tx buffer for CLI
extern int32_t txbuff_semaphor;								// semaphor
extern char txebuff[TXMSG_MAX_LEN];							// tx buffer for errors to CLI
extern char msg[MSG_MAX_LEN];									// rx buffer for CLI

extern TaskHandle_t xTaskToNotifyU5Rx;


/*******************************************************************************
*	user callback
*******************************************************************************/
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart);

#endif /* __UART */
