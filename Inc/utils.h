/*
 * Utils.h
 *
 *  Created on: Sep 10, 2020
 *      Author: avinoam.danieli
 */


int32_t reset(CLI_command_T* command);

int32_t IWDG_en(CLI_command_T* command);

uint32_t DWT_Delay_Init(void);
int32_t tasklist(CLI_command_T* command);
int32_t rts(CLI_command_T* command);
int32_t rrts(CLI_command_T* command);
int32_t message(CLI_command_T* command);
int32_t bootloader(CLI_command_T* command);
