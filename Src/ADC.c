/*
 * ADC.C
 *
 *  Created on: Jul 5, 2020
 *      Author: avinoam.danieli
 */

#include "main.h"
#include "defs.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "CLI.h"
#include "globals.h"
#include "clicmnds.h"
#include "uart.h"
#include "stm32f4xx_ll_adc.h"
#include "queue.h"
#include "LEDs.h"
#include "ADC.h"
#include "periodic.h"

/*******************************************************************************
*	AdcChannelConversion
*
*******************************************************************************/

/*
int16_t AdcChannelConversion(ADC_HandleTypeDef *hadc, uint32_t channel, uint32_t samplingTime, uint32_t timeout, int32_t *value)
{
  	ADC_ChannelConfTypeDef sConfig;
	HAL_StatusTypeDef retval;

  	sConfig.Channel = channel;
  	sConfig.Rank = 1;
	sConfig.SamplingTime = samplingTime;
  	if(HAL_ADC_ConfigChannel(hadc, &sConfig) != HAL_OK)
  	{
		printf("error %ld\r\n",  channel);//errorHandler();
  	}
    retval = HAL_ADC_Start(hadc);
	// This function will work only if enterd while conversion in proccess
	retval = HAL_ADC_PollForConversion(hadc, timeout);
  	*value = HAL_ADC_GetValue(hadc);

	return retval;
}
*/

/*******************************************************************************
*	AdcChannelConversion
*
*******************************************************************************/

int16_t AdcChannelConversion(ADC_HandleTypeDef *hadc, uint32_t channel, uint32_t samplingTime, uint32_t timeout, int32_t *value)
{
		//HAL_StatusTypeDef retval;

		switch (channel)
		{
		case ADC_CHANNEL_TMPRINT :
			*value = ADC_DMA_Buffer[INTR_TMPR];
			return HAL_OK;
		case ADC_CHANNEL_VREFINT :
			*value = ADC_DMA_Buffer[INTR_VREF];
			return HAL_OK;
		case ADC_CHANNEL_VBAT :
			*value = ADC_DMA_Buffer[INTR_VBATT];
			return HAL_OK;
		case ADC_VBAT_C :
			*value = ADC_DMA_Buffer[DMA_VBATT];
			return HAL_OK;
		case ADC_VCHARGER_C :
			*value = ADC_DMA_Buffer[DMA_VCHARGER];
			return HAL_OK;
		case ADC_P24V_C :
			*value = ADC_DMA_Buffer[DMA_P24V];
			return HAL_OK;
		case ADC_P5V_C :
			*value = ADC_DMA_Buffer[DMA_P5V];
			return HAL_OK;
		case ADC_VSW_C :
			*value = ADC_DMA_Buffer[DMA_VSW];
			return HAL_OK;
		case ADC_CURR_C :
			*value = ADC_DMA_Buffer[Imon];
			return HAL_OK;
		case ADC_BRAKE_CURR_C :
			*value = ADC_DMA_Buffer[DMA_BRKCURR];
			return HAL_OK;
		case ADC_VBRAKE_C :
			*value = ADC_DMA_Buffer[DMA_VBrake];
			return HAL_OK;
		default :
			return 0xffff;
		}
		return 0xffff;
}

/*******************************************************************************
*	getbrkcurr
*
*******************************************************************************/

int32_t getbrkcurr(float *VSENSE)
{
	int32_t val;

	val = ADC_DMA_Buffer[DMA_BRKCURR];

	*VSENSE = (val - Bcurrentzero) * 5;
	return TRUE;
}

/*******************************************************************************
*	brkcurr 10
*
*******************************************************************************/

int32_t brkcurr(CLI_command_T* command)
{
	int32_t val;
	float VSENSE;

	if ( command->ArgType == GETPAR)
  	{
		if (AdcChannelConversion(ADC_BRAKE_CURR_CV, ADC_BRAKE_CURR_C, ADC_SAMPLETIME_480CYCLES, 100L, &val) != HAL_OK)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		   return -11;
		}
		else
		{
			getbrkcurr(&VSENSE);
			if (gettxbuffer() == FALSE)
			{
			   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			   return -9;
			}
			snprintf (txbuff, TXMSG_MAX_LEN, "%s = %3.0f \r\n",command->sCommand, VSENSE);
			txbuff_semaphor = 1;
			sendtxbuffer();
			return TRUE;
		}
  	}
   return TRUE;
}

/*******************************************************************************
*	getvbatt
*
*******************************************************************************/

int32_t getvbatt(float *VSENSE)
{
	int32_t val;

	if (AdcChannelConversion(ADC_VBAT_CV, ADC_VBAT_C, ADC_SAMPLETIME_28CYCLES, 100L, &val) != HAL_OK)
	{
	   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	   return FALSE;
	}
	else
	{
		*VSENSE = (Vref/4095.0) / RES_RATIO * val;
	   return TRUE;
	}
}

/*******************************************************************************
*	plcvbatt
*
*******************************************************************************/

uint16_t plcvbatt(void)
{
	float VSENSE;

	if (getvbatt(&VSENSE))
	{
		return ( (uint16_t)(VSENSE * 1000.0) );
	}
	return FALSE;
}

/*******************************************************************************
*	vbatt
*	0
*
*******************************************************************************/

int32_t vbatt(CLI_command_T* command)
{
	float VSENSE;

	if ( command->ArgType == GETPAR)
  	{
		if (getvbatt(&VSENSE))
		{
			if (gettxbuffer() == FALSE)
			{
			   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			   return -9;
			}

			snprintf (txbuff, TXMSG_MAX_LEN, "%s=%3.3f  \r\n",command->sCommand, VSENSE);
			txbuff_semaphor = 1;
			sendtxbuffer();
			return TRUE;
		}
		else
			return FALSE;
   }
   return TRUE;
}

/*******************************************************************************
*	getvsw
*
*******************************************************************************/

int32_t getvsw(float *VSENSE)
{
	int32_t val;

	if (AdcChannelConversion(ADC_VSW_CV, ADC_VSW_C, ADC_SAMPLETIME_28CYCLES, 100L, &val) != HAL_OK)
	{
	   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	   return FALSE;
	}
	else
	{
		*VSENSE = (Vref/4095.0) / RES_RATIO * val;
	   return TRUE;
	}
}

/*******************************************************************************
*	getdiff
*
*******************************************************************************/

int32_t getdiff(float *VSENSE)
{
	float fvbatt, fvsw;

	getvsw(&fvsw);
	getvbatt(&fvbatt);
	*VSENSE = fvbatt - fvsw;

   return TRUE;
}

/*******************************************************************************
*	plcvsw
*
*******************************************************************************/

uint16_t plcvsw(void)
{
	float VSENSE;

	if (getvsw(&VSENSE))
	{
		return ( (uint16_t)(VSENSE * 1000.0) );
	}
	return FALSE;
}

/*******************************************************************************
*	vsw 1
*	CLI
*
*******************************************************************************/

int32_t vsw(CLI_command_T* command)
{
	float VSENSE;

	if ( command->ArgType == GETPAR)
  	{
		if (getvsw(&VSENSE))
		{
			if (gettxbuffer() == FALSE)
			{
			   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			   return -9;
			}

			snprintf (txbuff, TXMSG_MAX_LEN, "%s=%3.3f  \r\n",command->sCommand, VSENSE);
			txbuff_semaphor = 1;
			sendtxbuffer();
			return TRUE;
		}
	}
    return TRUE;
}

/*******************************************************************************
*	diff
*	CLI
*
*******************************************************************************/

int32_t diff(CLI_command_T* command)
{
	float VSENSE;

	if ( command->ArgType == GETPAR)
  	{
		if (getdiff(&VSENSE))
		{
			if (gettxbuffer() == FALSE)
			{
			   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			   return -9;
			}

			snprintf (txbuff, TXMSG_MAX_LEN, "%s=%3.3f  \r\n",command->sCommand, VSENSE);
			txbuff_semaphor = 1;
			sendtxbuffer();
			return TRUE;
		}
	}
    return TRUE;
}

/*******************************************************************************
*	getp5v
*
*******************************************************************************/

int32_t getp5v(float *VSENSE)
{
	int32_t val;

	if (AdcChannelConversion(ADC_P5V_CV, ADC_P5V_C, ADC_SAMPLETIME_28CYCLES, 100L, &val) != HAL_OK)
	{
	   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	   return FALSE;
	}
	else
	{
		*VSENSE = (Vref/4095.0) / RES_RATIO_5V * val;
	   return TRUE;
	}
}

/*******************************************************************************
*	plcp5v
*
*******************************************************************************/

uint16_t plcp5v(void)
{
	float VSENSE;

	if (getp5v(&VSENSE))
	{
		return ( (uint16_t)(VSENSE * 1000.0) );
	}
	return FALSE;
}

/*******************************************************************************
*	p5v 9
*
*******************************************************************************/

int32_t p5v(CLI_command_T* command)
{
	float VSENSE;

	if ( command->ArgType == GETPAR)
  	{
		if (getp5v(&VSENSE))
		{
			if (gettxbuffer() == FALSE)
			{
			   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			   return -9;
			}

			snprintf (txbuff, TXMSG_MAX_LEN, "%s=%3.3f  \r\n",command->sCommand, VSENSE);
			txbuff_semaphor = 1;
			sendtxbuffer();
			return TRUE;
		}
   }
   return TRUE;
}

/*******************************************************************************
*	getp24v
*
*******************************************************************************/

int32_t getp24v(float *VSENSE)
{
	int32_t val;

	if (AdcChannelConversion(ADC_P24V_CV, ADC_P24V_C, ADC_SAMPLETIME_28CYCLES, 100L, &val) != HAL_OK)
	{
	   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	   return FALSE;
	}
	else
	{
		*VSENSE = (Vref/4095.0) / RES_RATIO_24V * val;
	   return TRUE;
	}
}

/*******************************************************************************
*	plcp24v
*
*******************************************************************************/

uint16_t plcp24v(void)
{
	float VSENSE;

	if (getp24v(&VSENSE))
	{
		return ( (uint16_t)(VSENSE * 1000.0) );
	}
	return FALSE;
}

/*******************************************************************************
*	p24v 8
*
*******************************************************************************/

int32_t p24v(CLI_command_T* command)
{
	float VSENSE;

	if ( command->ArgType == GETPAR)
  	{
		if (getp24v(&VSENSE))
		{
			if (gettxbuffer() == FALSE)
			{
			   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			   return -9;
			}

			snprintf (txbuff, TXMSG_MAX_LEN, "%s=%3.3f  \r\n",command->sCommand, VSENSE);
			txbuff_semaphor = 1;
			sendtxbuffer();
			return TRUE;
   	}
  	}
    return TRUE;
}

/*******************************************************************************
*	getp3v3
*
*******************************************************************************/

int32_t getp3v3(float *VSENSE)
{
	int32_t val;

	if (AdcChannelConversion(&hadc1, ADC_CHANNEL_VBAT, ADC_SAMPLETIME_144CYCLES, 100L, &val) != HAL_OK)
	{
	   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	   return FALSE;
	}
	else
	{
		*VSENSE = (Vref/4095.0) * 2.0 * val;
	   return TRUE;
	}
}

/*******************************************************************************
*	plcp3v3
*
*******************************************************************************/

uint16_t plcp3v3(void)
{
	float VSENSE;

	if (getp3v3(&VSENSE))
	{
		return ( (uint16_t)(VSENSE * 1000.0) );
	}
	return FALSE;
}

/*******************************************************************************
*	p3v3
* 	18
*
*******************************************************************************/

int32_t p3v3(CLI_command_T* command)
{
	float VSENSE;

	if ( command->ArgType == GETPAR)
  	{
		if (getp3v3(&VSENSE))
		{
			if (gettxbuffer() == FALSE)
			{
			   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			   return -9;
			}

			sprintf (txbuff, "%s=%3.3f  \r\n",command->sCommand, VSENSE);
			txbuff_semaphor = 1;
			sendtxbuffer();
			return TRUE;
		}
   }
   return TRUE;
}

/*******************************************************************************
*	getvswcurr
*
*******************************************************************************/

int32_t getvswcurr(float *VSENSE)
{
	int32_t val;

	if (AdcChannelConversion(ADC_CURR_CV, ADC_CURR_C, ADC_SAMPLETIME_144CYCLES, 100L, &val) != HAL_OK)
	{
	   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	   return FALSE;
	}
	else
	{
		*VSENSE = (Vref/4095.0) * 2.0 * val;
	   return TRUE;
	}
}

/*******************************************************************************
*	plcvswcurr
*
*******************************************************************************/

uint16_t plcvswcurr(void)
{
	float VSENSE;

	if (getvswcurr(&VSENSE))
	{
		return ( (uint16_t)(VSENSE / 10.0) );
	}
	return FALSE;
}

/*******************************************************************************
*	vswcurr 2
*	CLI
*
*******************************************************************************/

int32_t vswcurr(CLI_command_T* command)
{
	float VSENSE;

	if ( command->ArgType == GETPAR)
  	{
		if (getvswcurr(&VSENSE))
		{
			if (gettxbuffer() == FALSE)
			{
			   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			   return -9;
			}

			sprintf (txbuff, "%s=%3.3f  \r\n",command->sCommand, VSENSE);
			txbuff_semaphor = 1;
			sendtxbuffer();
			return TRUE;
		}
   }
   return TRUE;
}

/*******************************************************************************
*	getvcharge
*
*******************************************************************************/

int32_t getvcharge(float *VSENSE)
{
	int32_t val;

	if (AdcChannelConversion(ADC_VCHARGER_CV, ADC_VCHARGER_C, ADC_SAMPLETIME_28CYCLES, 100L, &val) != HAL_OK)
	{
	   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	   return FALSE;
	}
	else
	{
		*VSENSE = (Vref/4095.0) / RES_RATIO  * val;
	   return TRUE;
	}
}

/*******************************************************************************
*	plcvcharge
*
*******************************************************************************/

uint16_t plcvcharge(void)
{
	float VSENSE;

	if (getvcharge(&VSENSE))
	{
		return ( (uint16_t)(VSENSE / 10.0) );
	}
	return FALSE;
}

/*******************************************************************************
*	vcharge 7
*
*******************************************************************************/
int32_t vcharge(CLI_command_T* command)
{
	float VSENSE;

	if ( command->ArgType == GETPAR)
  	{
		if (getvcharge(&VSENSE))
		{
			if (gettxbuffer() == FALSE)
			{
			   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			   return -9;
			}

			sprintf (txbuff, "%s=%3.3f  \r\n",command->sCommand, VSENSE);
			txbuff_semaphor = 1;
			sendtxbuffer();
			return TRUE;
		}
   }
   return TRUE;
}

/*******************************************************************************
*	vbrake 11
*
*******************************************************************************/
int32_t vbrake(CLI_command_T* command)
{
	int32_t val;
	float VSENSE;

	if ( command->ArgType == GETPAR)
  	{
		if (AdcChannelConversion(ADC_VBRAKE_CV, ADC_VBRAKE_C, ADC_SAMPLETIME_480CYCLES, 100L, &val) != HAL_OK)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		   return -11;
		}
		else
		{
			VSENSE = (Vref/4095.0) / RES_RATIO  * val;

			if (gettxbuffer() == FALSE)
			{
			   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			   return -9;
			}

			sprintf (txbuff, "%s=%3.3f  \r\n",command->sCommand, VSENSE);
			txbuff_semaphor = 1;
			sendtxbuffer();
			return TRUE;
		}
   }
   return TRUE;
}
/*******************************************************************************
*	IntVref
*
*******************************************************************************/
int32_t IntVref(CLI_command_T* command)
{
	int32_t val;
	float VSENSE;

	if ( command->ArgType == GETPAR)
  	{
		if (AdcChannelConversion(&hadc1, ADC_CHANNEL_VREFINT, ADC_SAMPLETIME_480CYCLES, 100L, &val) != HAL_OK)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		   return -11;
		}
		else
		{
			VSENSE = (Vref/4095.0) * val;

			if (gettxbuffer() == FALSE)
			{
			   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			   return -9;
			}

			sprintf (txbuff, "%s=%3.3f  \r\n",command->sCommand, VSENSE);
			txbuff_semaphor = 1;
			sendtxbuffer();
			return TRUE;
		}
   }
   return TRUE;
}

/*******************************************************************************
*	CPUTmpr
*******************************************************************************/
int32_t CPUTmpr(CLI_command_T* command)
{
	int32_t val;
	float /*inttmpr, VSENSE, */Temp;
	float TS_CAL1, TS_CAL2;

	if ( command->ArgType == GETPAR)
  	{
		if (AdcChannelConversion(&hadc1, ADC_CHANNEL_TMPRINT, ADC_SAMPLETIME_480CYCLES, 100L, &val) != HAL_OK)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		   return -11;
		}
		else
		{
			// VSENSE = (Vref/4095.0) * val;
			// inttmpr = ((VSENSE - V25) / Avg_Slope) + 25.0;

			if (gettxbuffer() == FALSE)
			{
			   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			   return -9;
			}
			TS_CAL1 = (float)(*TEMPSENSOR_CAL1_ADDR * VREFINT_CAL_VREF) / ( (float)Vref * 1000.0 );
#ifdef PRINT
			printf("%d\n", *TEMPSENSOR_CAL1_ADDR);
			printf("%3.1f\n", (float)(*TEMPSENSOR_CAL1_ADDR) );
			printf("%d\n", *TEMPSENSOR_CAL2_ADDR);
			printf("%3.1f\n", (float)(*TEMPSENSOR_CAL2_ADDR) );

			printf("%d\t %3.3f\n", *VREFINT_CAL_ADDR, (float)(*VREFINT_CAL_ADDR) * Vref * 1000.0 / (float)(VREFINT_CAL_VREF) );
#endif
			TS_CAL2 = (float)(*TEMPSENSOR_CAL2_ADDR * VREFINT_CAL_VREF) / ( (float)Vref * 1000.0 );
			Temp = ( (float)( (TEMPSENSOR_CAL2_TEMP - TEMPSENSOR_CAL1_TEMP)*(val - TS_CAL1) ) / (float)(TS_CAL2 - TS_CAL1) ) + TEMPSENSOR_CAL1_TEMP;

			sprintf (txbuff, "%s=%3.1f  \r\n",command->sCommand, Temp);
			txbuff_semaphor = 1;
			sendtxbuffer();
			return TRUE;
		}
   }
   return TRUE;
}

/*******************************************************************************
*	plcTMPR
*
*******************************************************************************/

uint16_t plcTMPR(void)
{
	int32_t val;
	float TS_CAL1, TS_CAL2, Temp;

	if (AdcChannelConversion(&hadc1, ADC_CHANNEL_TMPRINT, ADC_SAMPLETIME_480CYCLES, 100L, &val) != HAL_OK)
	{
	   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	   return -11;
	}
	else
	{

		TS_CAL1 = (float)(*TEMPSENSOR_CAL1_ADDR * VREFINT_CAL_VREF) / ( (float)Vref * 1000.0 );
		TS_CAL2 = (float)(*TEMPSENSOR_CAL2_ADDR * VREFINT_CAL_VREF) / ( (float)Vref * 1000.0 );
		Temp = ( (float)( (TEMPSENSOR_CAL2_TEMP - TEMPSENSOR_CAL1_TEMP)*(val - TS_CAL1) ) / (float)(TS_CAL2 - TS_CAL1) ) + TEMPSENSOR_CAL1_TEMP;
		Temp = Temp * 100.0;

		return ( (int16_t)(Temp) );
	}
	return FALSE;
}

/*******************************************************************************
*	GetV
*******************************************************************************/
int32_t  GetV(CLI_command_T* command)
{
	int32_t val;
	float fval1, fval2, fval3, fval4, fval5, fval6, fval7;

	if ( command->ArgType == GETPAR)
  	{
		AdcChannelConversion(ADC_VBAT_CV, ADC_VBAT_C, ADC_SAMPLETIME_28CYCLES, 100L, &val);
		fval1 = (Vref/4095.0) / RES_RATIO * val;
		AdcChannelConversion(ADC_VSW_CV, ADC_VSW_C, ADC_SAMPLETIME_28CYCLES, 100L, &val);
		fval2 = (Vref/4095.0) / RES_RATIO * val;
		AdcChannelConversion(ADC_VCHARGER_CV, ADC_VCHARGER_C, ADC_SAMPLETIME_28CYCLES, 100L, &val);
		fval3 = (Vref/4095.0) / RES_RATIO * val;
		AdcChannelConversion(ADC_P5V_CV, ADC_P5V_C, ADC_SAMPLETIME_28CYCLES, 100L, &val);
		fval4 = (Vref/4095.0) / RES_RATIO_5V * val;
		AdcChannelConversion(ADC_P24V_CV, ADC_P24V_C, ADC_SAMPLETIME_28CYCLES, 100L, &val);
		fval5 = (Vref/4095.0) / RES_RATIO_24V * val;
		AdcChannelConversion(ADC_VBRAKE_CV, ADC_VBRAKE_C, ADC_SAMPLETIME_144CYCLES, 100L, &val);
		fval6 = (Vref/4095.0) / RES_RATIO * val;
		AdcChannelConversion(&hadc1, ADC_CHANNEL_VBAT, ADC_SAMPLETIME_144CYCLES, 100L, &val);
		fval7 = (Vref/4095.0) * 2.0 * val;

		if (gettxbuffer() == FALSE)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		   return -17;
		}
		sprintf(txbuff, "%s   VBATT=%3.3f, VSW=%3.3f, VCHARGER=%3.3f, P5V=%3.3f, P24V=%3.3f, VBRAKES=%3.3f, P3V3=%3.3f  \r\n",
				command->sCommand, fval1, fval2, fval3, fval4, fval5, fval6, fval7) ;
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
	}
	return TRUE;
}

/*******************************************************************************
*	GetC
*******************************************************************************/
int32_t  GetC(CLI_command_T* command)
{
	int32_t val;
	float fval1, fval2;

	if ( command->ArgType == GETPAR)
  	{
		val = getvswcurr(&fval1);
		AdcChannelConversion(ADC_BRAKE_CURR_CV, ADC_BRAKE_CURR_C, ADC_SAMPLETIME_144CYCLES, 100L, &val);

		fval2 = (Vref/4095.0) * val;
		if (gettxbuffer() == FALSE)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		   return -17;
		}
		sprintf(txbuff, "%s   VSW Current=%3.3f, Brakes Current=%3.3f  \r\n",
				command->sCommand, fval1, fval2) ;
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
	}
	return TRUE;
}

/*******************************************************************************
*	ADC_DMA_Init
*
*******************************************************************************/

uint16_t ADC_DMA_Buffer[15];

int32_t  ADC_DMA_Init(void)
{
	int32_t status;

	printf("%ld\n", status = HAL_ADC_GetState(&hadc3));
	if ( HAL_ADC_Start(&hadc3) != HAL_OK )
	{
	   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	   return -17;
	}
	if ( HAL_ADC_Start(&hadc2) != HAL_OK )
	{
	   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	   return -17;
	}
	if ( HAL_ADCEx_MultiModeStart_DMA(&hadc1, (uint32_t *)ADC_DMA_Buffer, 12) != HAL_OK )
	{
	   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	   return -17;
	}
	HAL_Delay(2);
	return TRUE;
}

/* brake current measurement

 #include <math.h>
	int16_t value;
	uint32_t mean, sq ,t;
	while (1)
	{
		mean = 0;
		sq = 0;
		for (i = 0;i < 16;i ++)
		{
			value = ADC_DMA_Buffer[DMA_BRKCURR];
			mean += value;
			sq += (value * value);
			vTaskDelay(22);
		}
		mean = mean;
		t = mean*mean;
		t = t >> 4;
		sq = sq;
		printf("%ld   %ld   %3.2f\n", mean >> 4, (sq - t) >> 4, sqrt( (sq - t) >> 4 ));
	}



  */

/*
#define VREFINT_CAL_ADDR                   ((uint16_t*) (0x1FFF7A2AU))  Internal voltage reference, address of parameter VREFINT_CAL: VrefInt ADC raw data acquired at temperature 30 DegC (tolerance: +-5 DegC), Vref+ = 3.3 V (tolerance: +-10 mV).
#define VREFINT_CAL_VREF                   ( 3300U)                     Analog voltage reference (Vref+) value with which temperature sensor has been calibrated in production (tolerance: +-10 mV) (unit: mV).
 Temperature sensor
#define TEMPSENSOR_CAL1_ADDR               ((uint16_t*) (0x1FFF7A2CU))  Internal temperature sensor, address of parameter TS_CAL1: On STM32F4, temperature sensor ADC raw data acquired at temperature  30 DegC (tolerance: +-5 DegC), Vref+ = 3.3 V (tolerance: +-10 mV).
#define TEMPSENSOR_CAL2_ADDR               ((uint16_t*) (0x1FFF7A2EU))  Internal temperature sensor, address of parameter TS_CAL2: On STM32F4, temperature sensor ADC raw data acquired at temperature 110 DegC (tolerance: +-5 DegC), Vref+ = 3.3 V (tolerance: +-10 mV).
#define TEMPSENSOR_CAL1_TEMP               (( int32_t)   30)            Internal temperature sensor, temperature at which temperature sensor has been calibrated in production for data into TEMPSENSOR_CAL1_ADDR (tolerance: +-5 DegC) (unit: DegC).
#define TEMPSENSOR_CAL2_TEMP               (( int32_t)  110)            Internal temperature sensor, temperature at which temperature sensor has been calibrated in production for data into TEMPSENSOR_CAL2_ADDR (tolerance: +-5 DegC) (unit: DegC).
#define TEMPSENSOR_CAL_VREFANALOG          ( 3300U)                     Analog voltage reference (Vref+) voltage with which temperature sensor has been calibrated in production (+-10 mV) (unit: mV).
*/

