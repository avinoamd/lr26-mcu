/*
 * BCR.c
 *
 *  Created on: Aug 25, 2020
 *      Author: avinoam.danieli
 */

#include "main.h"
#include "cmsis_os.h"
#include "defs.h"
#include <stdio.h>
#include <string.h>
#include <tgmath.h>
#include "queue.h"
#include "globals.h"
#include "CLI.h"
#include "BCR.h"
#include "uart.h"
#include "CLIcmnds.h"

#define BCRSTART 0
#define BCRSTOP 1
typedef struct
{
	int32_t len;
	uint8_t *data;
} BCR_type;

const uint8_t SCAN_ENABLE[] = {0x04, 0xE9, 0x04, 0x00, 0xFF, 0x0F};
const uint8_t SCAN_DISABLE[] = {0x04, 0xEA, 0x04, 0x00, 0xFF, 0x0E};
const uint8_t ACK[] = {0x04, 0xD0, 0x00, 0x00, 0xFF, 0x2C};
const uint8_t NAK[] = {0x04, 0xD1, 0x00, 0x00, 0xFF, 0x2B};

const uint8_t GAT[] = {0x0B, 0xC6, 0x04, 0x08, 0x00, 0x69, 0x00, 0x68, 0x0D, 0x6A, 0x0A, 0xFD, 0xD1};    // The prefix/suffix value
const uint8_t GAR[] = {0x07, 0xC6, 0x04, 0x08, 0x00, 0xEB, 0x01, 0xFE, 0x3B};										// prefix
uint8_t BcrRxBuff[BCRRXLEN+2];
uint8_t tmpbuff[50];
int32_t BCRstatus = 0;

/*******************************************************************************
*	BCR_en
*
*
*******************************************************************************/

int32_t BCR_en(void)
{
	int32_t val;

	xTaskToNotifyU5Rx = xTaskGetCurrentTaskHandle();							// for ISR
	HAL_UART_AbortReceive_IT(&huart5);												// clear
	huart5.hdmarx->Instance->CR &=~ DMA_SxCR_CIRC;								// not circular
	if ( HAL_UART_Receive_DMA( &huart5, BcrRxBuff, 6 ) != HAL_OK )			// wait for data from TF-540
	{
	   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	   return -121;
	}
	else
	{
		if(HAL_UART_Transmit_DMA(&huart5, (uint8_t *)SCAN_ENABLE, sizeof(SCAN_ENABLE)) != HAL_OK)
		{
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			return -19;
		}
		else
		{
		// If xClearCountOnExit is set to pdTRUE, then the calling task’s notification value will be cleared to zero before the call to ulTaskNotifyTake() returns.
			val = ulTaskNotifyTake( pdTRUE, 1000 );												// wait for acknowledge
			if ( val != 0 )																				// no timeout
			{
				if ( memcmp(ACK, BcrRxBuff, 6) == 0 )												// received ACK
				{
					huart5.hdmarx->Instance->CR |= DMA_SxCR_CIRC;								// circular buffer
					xTaskToNotifyU5Rx = NULL;															// for ISR
					if ( HAL_UART_Receive_DMA( &huart5, BcrRxBuff, BCRRXLEN ) != HAL_OK )			// wait for data from TF-540
					{
					   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
					   return -121;
					}
					else
					{
						BCRstatus = 1;
						return TRUE;
					}
				}
				else
					return FALSE;
			}
			else
				return FALSE;
		}
	}
}

/*******************************************************************************
*	BCR_CLI_getdata
*	CLI
*
*******************************************************************************/

int32_t BCR_CLI_getdata(CLI_command_T* command)
{
	int32_t len;
	if ( command->ArgType == GETPAR)
  	{
		len = BCR_getdata(tmpbuff, 32);
		if (gettxbuffer() == FALSE)
		{
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			return -9;
		}

		sprintf (txbuff, "%s=%ld\t%s  \r\n",command->sCommand, len, tmpbuff);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
  	}
	return TRUE;
}

/*******************************************************************************
*	BCR_getdata
*
*
*******************************************************************************/

int32_t BCR_getdata(uint8_t *buff, int32_t size)
{
	uint32_t start, end, last, len, i;

	memset(buff, 0, BCRRXLEN);													// clear input buffer
	//printf("%ld\n", huart5.hdmarx->Instance->NDTR );
	last = BCRRXLEN - huart5.hdmarx->Instance->NDTR;

	for (i = 0;i < BCRRXLEN -1;i ++)
	{
	  	if (BcrRxBuff[(last - i) & MASK] == '\r')
		{
		  	break;
		}
	}
	if (i >= (BCRRXLEN - 2))
		return 0;
	end = (last-i) & MASK;

  	for (i = 0;i < BCRRXLEN - 1;i ++)
	{
	  	if (BcrRxBuff[(end - 1 - i) & MASK] == '\r')
		{
		  	break;
		}
	}
	if (i == BCRRXLEN - 1)
	  	start = 0;
	else
		start = (end - i) & MASK;
	if (end > start)
	{
		len = (end - start) & MASK;
		memcpy(buff, BcrRxBuff+start, len);
		return (len);
	}
	if (end < start)
	{
	  	len = BCRRXLEN - start;
		memcpy(buff, BcrRxBuff+start, fmin(len, size));
		if (size > len)
			memcpy(buff + len, BcrRxBuff, fmin(end, size-len));
		return (len+end);
	}
	return 0;
}

/*******************************************************************************
*	BCR_CLI_en
*	CLI
*
*******************************************************************************/

int32_t BCR_CLI_en(CLI_command_T* command)
{
	uint16_t stam;

	if (command->ArgType == GETPAR)
  	{

		if (gettxbuffer() == FALSE)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		   return -3;
		}
    	snprintf (txbuff, TXMSG_MAX_LEN, "%s=%ld\r\n",command->sCommand, BCRstatus );
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
  	}
  	else if ( command->ArgType == SETPAR)
  	{
		sscanf(command->sParameter, "%hd", &stam);
		if (stam == 0)
		{
			BCR_dis();
		}
		else
		{
			BCR_en();
		}
		if (gettxbuffer() == FALSE)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		   return -3;
		}
    	snprintf (txbuff, TXMSG_MAX_LEN, "%s=%ld\r\n",command->sCommand, BCRstatus );
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   }

   return 1;
}

/*******************************************************************************
*	BCR_dis
*
*
*******************************************************************************/

int32_t BCR_dis(void)
{
	int32_t val;

	xTaskToNotifyU5Rx = xTaskGetCurrentTaskHandle();							// for ISR
	HAL_UART_AbortReceive_IT(&huart5);												// clear
	huart5.hdmarx->Instance->CR &=~ DMA_SxCR_CIRC;								// not circular
	if ( HAL_UART_Receive_DMA( &huart5, BcrRxBuff, 6 ) != HAL_OK )			// wait for data from TF-540
	{
	   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	   return -121;
	}
	else
	{
		if(HAL_UART_Transmit_DMA(&huart5, (uint8_t *)SCAN_DISABLE, sizeof(SCAN_DISABLE)) != HAL_OK)
		{
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			return -19;
		}
		else
		{
		// If xClearCountOnExit is set to pdTRUE, then the calling task’s notification value will be cleared to zero before the call to ulTaskNotifyTake() returns.
			val = ulTaskNotifyTake( pdTRUE, 1000 );												// wait for acknowledge
			if ( val != 0 )																				// no timeout
			{
				if ( memcmp(ACK, BcrRxBuff, 6) == 0 )												// received ACK
				{
					BCRstatus = 0;
					return TRUE;
				}
				else
					return FALSE;
			}
			else
				return FALSE;
		}
	}
}

/*******************************************************************************
*	BCR_init
*
*
*******************************************************************************/

int32_t BCR_init(void)
{
	int32_t val;

	xTaskToNotifyU5Rx = xTaskGetCurrentTaskHandle();							// for ISR
	HAL_UART_AbortReceive_IT(&huart5);												// clear
	huart5.hdmarx->Instance->CR &=~ DMA_SxCR_CIRC;								// not circular
	printf("enter bcrinit\n");
	if ( HAL_UART_Receive_DMA( &huart5, BcrRxBuff, 6 ) != HAL_OK )			// wait for data from TF-540
	{
	   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	   return -121;
	}
	else
	{
		printf("bcrinit 0\n");
		if(HAL_UART_Transmit_DMA(&huart5, (uint8_t *)GAT, sizeof(GAT)) != HAL_OK)
		{
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			return -19;
		}
		else
		{
		// If xClearCountOnExit is set to pdTRUE, then the calling task’s notification value will be cleared to zero before the call to ulTaskNotifyTake() returns.
			printf("bcrinit 0.5\n");
			val = ulTaskNotifyTake( pdTRUE, 3000 );												// wait for acknowledge
			if ( val != 0 )																				// no timeout
			{
				printf("bcrinit 1\n");
				if ( memcmp(ACK, BcrRxBuff, 6) == 0 )												// received ACK
				{
					//HAL_UART_AbortReceive_IT(&huart5);												// clear
					if ( HAL_UART_Receive_DMA( &huart5, BcrRxBuff, 6 ) != HAL_OK )			// wait for data from TF-540
					{
					   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
					   return -121;
					}
					else
					{
						printf("bcrinit 2\n");
						if(HAL_UART_Transmit_DMA(&huart5, (uint8_t *)GAR, sizeof(GAR)) != HAL_OK)
						{
							errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
							return -19;
						}
						else
						{
						// If xClearCountOnExit is set to pdTRUE, then the calling task’s notification value will be cleared to zero before the call to ulTaskNotifyTake() returns.
							printf("bcrinit 3\n");
							val = ulTaskNotifyTake( pdTRUE, 3000 );												// wait for acknowledge
							if ( val != 0 )																				// no timeout
							{
								if ( memcmp(ACK, BcrRxBuff, 6) == 0 )												// received ACK
								{
									BCRstatus = 3;
									printf("bcrinit done\n");
									return TRUE;
								}
								else
									return FALSE;
							}
							else
								return FALSE;
						}
					}
				}
				else
					return FALSE;
			}
			else
				return FALSE;
		}
	}
}

/*******************************************************************************
*	BCR_Clear
*
*
*******************************************************************************/

int32_t BCR_Clear(void)
{
	memset(BcrRxBuff, 0, BCRRXLEN);											// clear input buffer
	return TRUE;
}



/*

int32_t BCR_getdata(uint8_t *buff)
{
	int32_t last, start, len, end, i;

	static int32_t firstmessage = TRUE;

	memset(buff, 0, BCRRXLEN);													// clear input buffer
	printf("%ld\n", huart5.hdmarx->Instance->NDTR );
	last = BCRRXLEN - huart5.hdmarx->Instance->NDTR;
	if ( (last == 0) && firstmessage )
		return 0;

	firstmessage = FALSE;
	for (i = 0;i < BCRRXLEN;i ++)												// seartch for \r
	{
		printf("%ld\n", (last - i - 1 + BCRRXLEN) % BCRRXLEN);
		if (BcrRxBuff[(last - i - 1 + BCRRXLEN) % BCRRXLEN] == '\r')
		{
			printf("Found 1\n");
			end = (last - i - 1 + BCRRXLEN) % BCRRXLEN;
			break;
		}
	}
	for (i = 0;i < BCRRXLEN-1;i ++)												// seartch for \r
	{
		printf("%ld\n", (end - i - 1 + BCRRXLEN) % BCRRXLEN);
		if (BcrRxBuff[(end - i - 1 + BCRRXLEN) % BCRRXLEN] == '\r')
		{
			printf("Found 2\n");
			break;
		}
	}
	if (i >= BCRRXLEN-2)
		start = 0;
	else
		start = i+1;
	len = end - start-1;

	memcpy(buff, BcrRxBuff + start, len);
	return 0;
}
*/
