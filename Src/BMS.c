/*
 * BMS.c
 *
 *  Created on: 30 Aug 2020
 *      Author: avinoam.danieli
 */

#include "main.h"
#include "cmsis_os.h"
#include "defs.h"
#include <stdio.h>
#include <string.h>
#include <tgmath.h>
#include "uart.h"
#include "CLI.h"
#include "CLIcmnds.h"
#include "print.h"

static uint8_t data[40] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
//uint8_t data1[40];

/*******************************************************************************
*	i2cerrorhandler
*
*******************************************************************************/

void i2cerrorhandler(int32_t num, char const * FILE, int LINE, char const * PRETTY_FUNCTION)
{
	snprintf(txebuff, TXMSG_MAX_LEN, "File = %s  ", FILE);
	snprintf(txebuff+strlen(txebuff), TXMSG_MAX_LEN,  "Error line = %d  ", LINE);
	snprintf(txebuff+strlen(txebuff), TXMSG_MAX_LEN, "%s\n\r",  PRETTY_FUNCTION);
	printf("%s", txebuff);
	if (BMSPrintToUart3 == TRUE)
	{
		HAL_UART_Transmit(&huart3, (uint8_t *)txebuff, strlen(txebuff), 1000L);
	}
	//flasherr(); lederror();
	HAL_I2C_DeInit(&hi2c3);
	if ( HAL_I2C_Init(&hi2c3) != HAL_OK )
	{
	   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	   while(1);
	}
}

/*******************************************************************************
*	BMS_DAStatus2
*
*******************************************************************************/

uint16_t BMS_DAStatus2(int32_t first, int32_t addr, uint8_t *MSB, uint8_t *LSB)
{
	static uint8_t data[20];
	HAL_StatusTypeDef ret;

	if (first == 0)
	{
		ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X72, 1, data, 17, 320);
		if (ret != HAL_OK)
		{
			i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			return (-1);
		}
	}
	*MSB = data[2 + addr + addr];
	*LSB = data[1 + addr + addr];
	return TRUE;
}

/*******************************************************************************
*	BMS_Data
*
*******************************************************************************/

uint16_t BMS_Data(uint8_t addr, uint8_t *MSB, uint8_t *LSB)
{
	uint8_t data[2];
	HAL_StatusTypeDef ret;

	ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, addr, 1, data, 2, 200);
	if (ret != HAL_OK)
	{
		i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		return (-1);
	}
	else
	{
	  *MSB = data[1];
	  *LSB = data[0];
	  return TRUE;
	}
}

/*******************************************************************************
*	BMS_CLI
*
*******************************************************************************/

int32_t BMS_CLI(CLI_command_T* command)
{
	HAL_StatusTypeDef ret;
	int32_t len, i;

   if (command->ArgType == GETPAR)
   {
	  if (gettxbuffer() == FALSE)
	  {
	     errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	     return -3;
     }

	  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X20, 1, data, 10, 2000);
	  if (ret != HAL_OK)
	  {
		   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	  }
	  else
	  {
		  len = data[0];
		  data[len+1] = 0;
		  data[10] = 0;
		  snprintf(txbuff, TXMSG_MAX_LEN, "\n\nManufacturerName = %s\n", data+1);
		  txbuff_semaphor = 1;
		  sendtxbuffer();
	  }
	  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X21, 1, data, 20, 2000);
	  if (ret != HAL_OK)
	  {
		   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	  }
	  else
	  {
		  gettxbuffer();
		  len = data[0];
		  data[len+1] = 0;
		  data[20] = 0;
		  snprintf(txbuff, TXMSG_MAX_LEN, "DeviceName = %s\n", data+1);
		  txbuff_semaphor = 1;
		  sendtxbuffer();
	  }
	  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X22, 1, data, 10, 2000);
	  if (ret != HAL_OK)
	  {
		   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	  }
	  else
	  {
		  gettxbuffer();
		  len = data[0];
		  data[len+1] = 0;
		  data[10] = 0;
		  snprintf(txbuff, TXMSG_MAX_LEN, "DeviceChemistry = %s\n", data+1);
		  txbuff_semaphor = 1;
		  sendtxbuffer();
	  }
	  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X1C, 1, data, 2, 2000);
	  if (ret != HAL_OK)
	  {
		   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	  }
	  else
	  {
		  gettxbuffer();
		  len = (data[1] << 8) + data[0];
		  snprintf(txbuff, TXMSG_MAX_LEN, "SerialNumber = %ld\n", len);
		  txbuff_semaphor = 1;
		  sendtxbuffer();
	  }
	  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X03, 1, data, 2, 2000);
	  if (ret != HAL_OK)
	  {
		   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	  }
	  else
	  {
		  gettxbuffer();
		  len = (data[1] << 8) + data[0];
		  snprintf(txbuff, TXMSG_MAX_LEN, "BatteryMode = %lX\n", len);
		  txbuff_semaphor = 1;
		  sendtxbuffer();
	  }
	  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X1A, 1, data, 2, 2000);
	  if (ret != HAL_OK)
	  {
		   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	  }
	  else
	  {
		  gettxbuffer();
		  len = (data[1] << 8) + data[0];
		  snprintf(txbuff, TXMSG_MAX_LEN, "SpecificationInfo = %lX\n", len);
		  txbuff_semaphor = 1;
		  sendtxbuffer();
	  }
	  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X08, 1, data, 2, 2000);
	  if (ret != HAL_OK)
	  {
		   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	  }
	  else
	  {
		  gettxbuffer();
		  len = (data[1] << 8) + data[0];
		  snprintf(txbuff, TXMSG_MAX_LEN, "Temperature = %g\n", (float)len/10.0 -273.16);
		  txbuff_semaphor = 1;
		  sendtxbuffer();
	  }
	  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X09, 1, data, 2, 2000);
	  if (ret != HAL_OK)
	  {
		   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	  }
	  else
	  {
		  gettxbuffer();
		  len = (data[1] << 8) + data[0];
		  snprintf(txbuff, TXMSG_MAX_LEN, "Voltage = %ld\n", len);
		  txbuff_semaphor = 1;
		  sendtxbuffer();
	  }

	  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X0a, 1, data, 2, 2000);
	  if (ret != HAL_OK)
	  {
		   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	  }
	  else
	  {
		  gettxbuffer();
		  len = (data[1] << 8) + data[0];
		  snprintf(txbuff, TXMSG_MAX_LEN, "Current = %d\n", (int16_t)len);
		  txbuff_semaphor = 1;
		  sendtxbuffer();
	  }
	  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X10, 1, data, 2, 2000);
	  if (ret != HAL_OK)
	  {
		   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	  }
	  else
	  {
		  gettxbuffer();
		  len = (data[1] << 8) + data[0]; // ad
		  snprintf(txbuff, TXMSG_MAX_LEN, "FullChargeCapacity = %ld\n", len);
		  txbuff_semaphor = 1;
		  sendtxbuffer();
	  }
	  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X18, 1, data, 2, 2000);
	  if (ret != HAL_OK)
	  {
		   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	  }
	  else
	  {
		  gettxbuffer();
		  len = (data[1] << 8) + data[0]; // ad
		  snprintf(txbuff, TXMSG_MAX_LEN, "DesignCapacity = %ld\n", len);
		  txbuff_semaphor = 1;
		  sendtxbuffer();
	  }
	  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X4F, 1, data, 2, 2000);
	  if (ret != HAL_OK)
	  {
		   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	  }
	  else
	  {
		  gettxbuffer();
		  len = (data[1] << 8) + data[0]; // ad
		  snprintf(txbuff, TXMSG_MAX_LEN, "StateOfHealth = %ld \n", len);
		  txbuff_semaphor = 1;
		  sendtxbuffer();
	  }
	  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X17, 1, data, 2, 2000);
	  if (ret != HAL_OK)
	  {
		   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	  }
	  else
	  {
		  gettxbuffer();
		  len = (data[1] << 8) + data[0]; // ad
		  snprintf(txbuff, TXMSG_MAX_LEN, "CycleCount = %ld\n", len);
		  txbuff_semaphor = 1;
		  sendtxbuffer();
	  }
	  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X0E, 1, data, 2, 2000);
	  if (ret != HAL_OK)
	  {
		   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	  }
	  else
	  {
		  gettxbuffer();
		  len = (data[1] << 8) + data[0]; // ad
		  snprintf(txbuff, TXMSG_MAX_LEN, "AbsoluteStateOfCharge = %ld\n", len);
		  txbuff_semaphor = 1;
		  sendtxbuffer();
	  }
	  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X0D, 1, data, 2, 2000);
	  if (ret != HAL_OK)
	  {
		   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	  }
	  else
	  {
		  gettxbuffer();
		  len = (data[1] << 8) + data[0]; // ad
		  snprintf(txbuff, TXMSG_MAX_LEN, "RelativeStateOfCharge = %ld\n", len);
		  txbuff_semaphor = 1;
		  sendtxbuffer();
	  }
	  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X16, 1, data, 2, 2000);
	  if (ret != HAL_OK)
	  {
		   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	  }
	  else
	  {
		  gettxbuffer();
		  len = (data[1] << 8) + data[0]; // ad
		  snprintf(txbuff, TXMSG_MAX_LEN, "BatteryStatus = %lx\n", len);
		  txbuff_semaphor = 1;
		  sendtxbuffer();
	  }
	  // ----------------------------------------------------------------------------
	  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X0F, 1, data, 2, 2000);
	  if (ret != HAL_OK)
	  {
		   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	  }
	  else
	  {
		  gettxbuffer();
		  len = (data[1] << 8) + data[0];
		  snprintf(txbuff, TXMSG_MAX_LEN, "RemainingCapacity = %ld\n", len);
		  txbuff_semaphor = 1;
		  sendtxbuffer();
	  }
	  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X0b, 1, data, 2, 2000);
	  if (ret != HAL_OK)
	  {
		   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	  }
	  else
	  {
		  gettxbuffer();
		  len = (data[1] << 8) + data[0];
		  snprintf(txbuff, TXMSG_MAX_LEN, "AverageCurrent = %d\n", (int16_t)len);
		  txbuff_semaphor = 1;
		  sendtxbuffer();
	  }
	  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X1c, 1, data, 2, 2000);
	  if (ret != HAL_OK)
	  {
		   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	  }
	  else
	  {
		  gettxbuffer();
		  len = (data[1] << 8) + data[0];
		  snprintf(txbuff, TXMSG_MAX_LEN, "MaxError = %ld\n", len);
		  txbuff_semaphor = 1;
		  sendtxbuffer();
	  }

	  gettxbuffer();
	  snprintf(txbuff, TXMSG_MAX_LEN, "Cells Voltages = ");
	  for (i = 0x31;i <= 0x3f;i++)
	  {
		  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, i, 1, data, 2, 2000);
		  if (ret != HAL_OK)
		  {
			   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		  }
		  else
		  {
			  len = (data[1] << 8) + data[0];
			  snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN-strlen(txbuff), "%ld\t", len);
			  //HAL_Delay(20);
		  }
	  }
	  snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN-strlen(txbuff), "\n");
	  txbuff_semaphor = 1;
	  sendtxbuffer();
	  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X72, 1, data, 17, 3200);
	  if (ret != HAL_OK)
	  {
		   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	  }
	  else
	  {
		  gettxbuffer();
		  len = (data[2] << 8) + data[1];
		  snprintf(txbuff, TXMSG_MAX_LEN, "ExtAveCellVoltage = %ld\n", len);
		  len = (data[4] << 8) + data[3];
		  snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN-strlen(txbuff), "VAUX Voltage = %ld\n", len);
		  for (i = 2;i < 8;i ++)
		  {
			  len = (data[i+i+2] << 8) + data[i+i+1];
			  snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN-strlen(txbuff), "Temperature = %2.2f\n", (float)len / 10.0 - 273.16);
		  }
	  }
	  txbuff_semaphor = 1;
	  sendtxbuffer();

	  return TRUE;
   }
   return TRUE;
}

void BMS(void)
{

  HAL_StatusTypeDef ret;
  int len, i;
  float f;

  while (1) {
  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X20, 1, data, 10, 2000);
  if (ret != HAL_OK)
  {
	   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
  }
  else
  {
	  len = data[0];
	  printf("%d\t%s\n", len, data+1);
	  memset(data, 0, 40);
  }
  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X21, 1, data, 10, 2000);
  if (ret != HAL_OK)
  {
	   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
  }
  else
  {
	  len = data[0];
	  printf("%d\t%s\n", len, data+1);
	  memset(data, 0, 40);
  }
  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X22, 1, data, 10, 2000);
  if (ret != HAL_OK)
  {
	   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
  }
  else
  {
	  len = data[0];
	  printf("%d\t%s\n", len, data+1);
	  memset(data, 0, 40);
  }
  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X08, 1, data, 2, 2000);
  if (ret != HAL_OK)
  {
	   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
  }
  else
  {
	  len = (data[1] << 8) + data[0];
	  f = len;
	  HAL_Delay(100);
	  printf("%g\n", f/10.0 -273.1);
  }
  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X09, 1, data, 2, 2000);
  if (ret != HAL_OK)
  {
	   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
  }
  else
  {
	  len = (data[1] << 8) + data[0];
	  printf("%d\n", len);
  }

  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X0a, 1, data, 2, 2000);
  if (ret != HAL_OK)
  {
	   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
  }
  else
  {
	  len = (data[1] << 8) + data[0];
	  printf("%d\n", len);
  }
  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X0b, 1, data, 2, 2000);
  if (ret != HAL_OK)
  {
	   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
  }
  else
  {
	  len = (data[1] << 8) + data[0];
	  printf("%d\n", len);
  }
  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X1c, 1, data, 2, 2000);
  if (ret != HAL_OK)
  {
	   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
  }
  else
  {
	  len = (data[1] << 8) + data[0];
	  printf("%d\n", len);
  }
  for (i = 0x31;i <= 0x3f;i++)
  {
	  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, i, 1, data, 2, 2000);
	  if (ret != HAL_OK)
	  {
		   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	  }
	  else
	  {
		  len = (data[1] << 8) + data[0];
		  printf("%d\t", len);
		  HAL_Delay(20);
	  }
  }
  printf("\n");
  memset(data, 0, 40);
  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X72, 1, data, 17, 3200);
  if (ret != HAL_OK)
  {
	   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
  }
  else
  {
	  float f;
	  printf("len = %d\n", data[0]);
	  len = (data[2] << 8) + data[1];
	  printf("ExtAveCellVoltage = %d\n", len);
	  HAL_Delay(20);
	  len = (data[4] << 8) + data[3];
	  printf("VAUX Voltage = %d\n", len);
	  HAL_Delay(20);
	  for (i = 2;i < 8;i ++)
	  {
		  len = (data[i+i+2] << 8) + data[i+i+1];
		  f = len;
		  printf("%2.2f\n", f / 10.0 - 273.16);
		  HAL_Delay(20);
	  }
  }
  }
}

/*
uint16_t BMS_Voltage()
{
	  ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X09, 1, data, 2, 2000);
	  if (ret != HAL_OK)
	  {
		   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	  }
	  else
	  {
		  len = (data[1] << 8) + data[0];
		  f = len;
		  HAL_Delay(100);
		  printf("%g\n", f/10.0 -273.1);
	  }

}
uint16_t BMS_Current()
{
   ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X0A, 1, data, 2, 2000);
   if (ret != HAL_OK)
   {
	   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
   }
   else
   {
	  len = (data[1] << 8) + data[0];
	  f = len;
	  HAL_Delay(100);
	  printf("%g\n", f/10.0 -273.1);
   }
}
uint16_t BMS_Average Current()
{
   ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X0A, 1, data, 2, 2000);
   if (ret != HAL_OK)
   {
	   i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
   }
   else
   {
	  len = (data[1] << 8) + data[0];
	  f = len;
	  HAL_Delay(100);
	  printf("%g\n", f/10.0 -273.1);
   }

}
uint16_t BMS_Max Error()
{

}
uint16_t BMS_Relative State of Charge()
{

}
uint16_t BMS_Absolute State of Charge()
{

}
BMS_Remaning Capacity()
{

}
BMS_Full Charge Capacity()
{

}
BMS_Charging Current()
{

}
BMS_Charging Voltage()
{

}
BMS_Cycle Count()
{

}
BMS_State of Health()
//BMS_Ext Avg Cell Voltage()
//BMS_TS1 Temperature()
//BMS_TS2 Temperature()
//BMS_TS3 Temperature()
//BMS_Cell Temperature()
//BMS_FET Temperature()
//BMS_Gauge Internal Temperature()

DAStatus2()







*/

