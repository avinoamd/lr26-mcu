/*
 * Battery.c
 *
 *  Created on: 8 Jan 2021
 *      Author: avinoam.danieli
 */

#include "main.h"
#include "cmsis_os.h"
#include <stdio.h>
#include <string.h>
#include "defs.h"
#include "ADC.h"
#include "BMS.h"
#include "uart.h"
#include "CLIcmnds.h"

#define ERRVAL 0x80000000

int32_t BMSPresent = FALSE;
int32_t BattLevel = 50;
static uint8_t data[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

/*******************************************************************************
*	BMS_Detect
*	Detect bms communication by reading DeviceChemistry
*	3 times retry
*
*******************************************************************************/

int32_t BMS_Detect(void)
{
	int i;
	HAL_StatusTypeDef ret;

	for (i = 0;i < 3;i ++)
	{
		ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X22, 1, data, 10, 2000);
		if (ret != HAL_OK)
		{
			i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		}
		else
		{
			break;
		}
	}
	if (ret != HAL_OK)
	{
		BMSPresent = FALSE;
		return FALSE;
	}
	if (data[0] >= 4)
	{
		BMSPresent = TRUE;
		return TRUE;
	}
	else
	{
		BMSPresent = FALSE;
		return FALSE;
	}
}

/*******************************************************************************
*	BMS_Voltage
*	Parameters		:
*	Description 	: Read BMS voaltage [mV]
*	Return			: OK - BMS voaltage [mV], Fail - -1
*
*******************************************************************************/

int32_t BMS_Voltage(void)
{
	HAL_StatusTypeDef ret;

	ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X09, 1, data, 2, 2000);
	if (ret != HAL_OK)
	{
		i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		return ERRVAL;
	}
	else
	{
		return (data[1] << 8) + data[0];
	}
}

/*******************************************************************************
*	BMS_Current
*	Parameters		:
*	Description 	: Read BMS current
*	Return			: OK - BMS current [mA], Fail - 0x8000000
*
*******************************************************************************/

int32_t BMS_Current(void)
{
	HAL_StatusTypeDef ret;

	ret = HAL_I2C_Mem_Read(&hi2c3, 0x16, 0X0A, 1, data, 2, 2000);
	if (ret != HAL_OK)
	{
		i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		return ERRVAL;
	}
	else
	{
		return 10L * (int32_t)(data[1] << 8) + data[0];
	}
}

/*******************************************************************************
*	BMS_Cycles
*	Parameters		:
*	Description 	: Read BMS cycles
*	Return			: OK - BMS cycles, Fail - 0x8000000
*
*******************************************************************************/

int32_t BMS_Cycles(void)
{
	HAL_StatusTypeDef ret;

	ret = HAL_I2C_Mem_Read(&hi2c3, 0x17, 0X0A, 1, data, 2, 2000);
	if (ret != HAL_OK)
	{
		i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		return ERRVAL;
	}
	else
	{
		return (int32_t)(data[1] << 8) + data[0];
	}
}

/*******************************************************************************
*	BMS_Diff
*	Parameters		:
*	Description 	: BMS voltage - VBatt
*	Return			: OK - Diff, Fail - 0x8000000
*
*******************************************************************************/

int32_t BMS_Diff(void)
{
	float VSENSE;
	int32_t bmsv, battv;

	bmsv = BMS_Voltage();
	if (bmsv == ERRVAL)
		return ERRVAL;
	if ( getvbatt(&VSENSE) == FALSE)
		return ERRVAL;
	battv = 1000.0 * VSENSE;
	return bmsv - battv;
}

#define FULLCHARGE 	56700

/*
 * LR26 is in charging state when
 * charge enabled and vcharge > 42V and bmsc > 0
 */

int32_t Battery_Status(void)
{
	int32_t bmsv;
	int32_t bmsc;
	int32_t bmscycles;
	float chrgv;
	GPIO_PinState cdisable;

	if (BMSPresent != TRUE)
		return FALSE;
	else
	{
		bmsv = BMS_Voltage();
		if (bmsv == ERRVAL)
			return ERRVAL;
		bmsc = BMS_Current();
		if (bmsc == ERRVAL)
			return ERRVAL;
		bmscycles = BMS_Cycles();
		if (bmscycles == ERRVAL)
			return ERRVAL;
		if (bmscycles > 10000)
			bmscycles = 0;
		cdisable = HAL_GPIO_ReadPin(CHARGE_DIS_GPIO_Port, CHARGE_DIS_Pin);
		getvcharge(&chrgv);
//		identify charging state
		if ( (cdisable == FALSE) && (chrgv > 42.0) )							// charging
		{
			if (bmsv >= 56000)						// near end of charge
			{
				if (bmsc < 20000)						// end of charge CV
				{
					BattLevel = 100;					// end of charge
				}
				else										// near end of charge CC
				{
					BattLevel = 79;
				}
			}
			else											// normal charging CC
			{
				BattLevel = 20 - 1 + ( ( ( (bmsv >> 1) - 24000 ) * 15 ) / 1000 );
			}
		}
		else																			// driving
		{
			BattLevel = 20 - (bmscycles / 80) + ( ( ( (bmsv >> 1) - 24000 ) * 15 ) / 1000 );
		}
	}	// BMSPresent
	return BattLevel;
}


/*******************************************************************************
*	SOCCLI
*	CLI
*
*******************************************************************************/

int32_t SOC_CLI(CLI_command_T* command)
{

	if ( command->ArgType == GETPAR)
  	{
		if (gettxbuffer() == FALSE)
		{
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			return -9;
		}

		if (BMSPresent == FALSE)
		{
			snprintf (txbuff, TXMSG_MAX_LEN, "%s = BMS Not Present \r\n",command->sCommand);
		}
		else
		{
			snprintf (txbuff, TXMSG_MAX_LEN, "%s = %ld \r\n",command->sCommand, BattLevel);
		}
		txbuff_semaphor = 1;
		sendtxbuffer();
	}
   return TRUE;
}

