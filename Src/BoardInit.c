/*
 * BoardInit.c
 *
 *  Created on: Aug 6, 2020
 *      Author: avinoam.danieli
 */

#include "main.h"
#include "FreeRTOS.h"
#include "timers.h"
#include "globals.h"
#include "defs.h"
#include "CLI.h"
#include "ADC.h"
#include <stdio.h>
#include <string.h>
#include "BoardInit.h"
#include "init.h"
#include "bit.h"
#include "uart.h"
#include "periodic.h"
#include "uart.h"
#include "CLIcmnds.h"

int32_t PowerState = POWEROFF;

/*******************************************************************************
*	CheckBatt
*
*******************************************************************************/

int32_t CheckBatt(int32_t print)
{
	uint16_t vbatt;

	vbatt = ADC_DMA_Buffer[1];
	if (print == 1)
	{
		gettxbuffer();
		txbuff_semaphor = 1;
		snprintf(txbuff, TXMSG_MAX_LEN, "Battery = %d", vbatt);
	}

	if ( (vbatt > BATT_LOW_LIMIT) && (vbatt < BATT_HIGH_LIMIT))
	{
		if (print == 1)
		{
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "....OK\n");
			HAL_UART_Transmit_DMA(&huart3, (uint8_t *)txbuff, strlen(txbuff));
		}
		return(TRUE);
	}
	else
	{
		if (print == 1)
		{
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "....Fail\n");
			HAL_UART_Transmit_DMA(&huart3, (uint8_t *)txbuff, strlen(txbuff));
		}
		return(FALSE);
	}
}

/*******************************************************************************
*	PrechargeOff
*
*******************************************************************************/

void PrechargeOff()
{
   HAL_GPIO_WritePin(PRECHRG_EN_GPIO_Port, PRECHRG_EN_Pin, GPIO_PIN_RESET);		// precharge disable
   HAL_GPIO_WritePin(LED0_GPIO_Port, LED0_Pin, GPIO_PIN_SET);
   PowerState &=~ PRECHARGEON;
   HAL_Delay(10);									// wait more time
}

/*******************************************************************************
*	VSWOff
*
*******************************************************************************/

void VSWOff(void)
{
   HAL_GPIO_WritePin(VSW_EN_GPIO_Port, VSW_EN_Pin, GPIO_PIN_RESET);					// VSW disable
   HAL_GPIO_WritePin(LED0_GPIO_Port, LED0_Pin, GPIO_PIN_SET);
   PowerState &=~ VSWON;
   HAL_Delay(10);									// wait more time
}

/*******************************************************************************
*	DCDC24VOff
*
*******************************************************************************/

void DCDC24VOff()
{
   HAL_GPIO_WritePin(DCDC24V_EN_GPIO_Port, DCDC24V_EN_Pin, GPIO_PIN_RESET);		// DCDC24V disable
   HAL_GPIO_WritePin(LED0_GPIO_Port, LED0_Pin, GPIO_PIN_SET);
   PowerState &=~ DCDC24V;
   HAL_Delay(10);									// wait more time
}

/*******************************************************************************
*	P5VOff
*
*******************************************************************************/

void P5VOff()
{
   PowerState &=~ P5V;
   HAL_GPIO_WritePin(P5V_EN_GPIO_Port, P5V_EN_Pin, GPIO_PIN_RESET);					// P5V disable
   HAL_GPIO_WritePin(LED0_GPIO_Port, LED0_Pin, GPIO_PIN_SET);
   HAL_Delay(10);									// wait more time
}

/*******************************************************************************
*	P24VOff
*
*******************************************************************************/

void P24VOff()
{
   HAL_GPIO_WritePin(P24V_EN_GPIO_Port, P24V_EN_Pin, GPIO_PIN_SET);		//
   HAL_GPIO_WritePin(LED0_GPIO_Port, LED0_Pin, GPIO_PIN_SET);
   HAL_Delay(10);									// wait more time
   PowerState &=~ P24V;
}

/*******************************************************************************
*	PullerOff
*
*******************************************************************************/

void PullerOff()
{
   HAL_GPIO_WritePin(PULLER_EN_GPIO_Port, PULLER_EN_Pin, GPIO_PIN_RESET);
   HAL_Delay(10);									// wait more time
   //PowerState &=~ P24V;
}

/*******************************************************************************
*	BrakeOff
*
*******************************************************************************/

void BrakeOff(void)
{
	HAL_StatusTypeDef retval;

	retval = HAL_TIM_PWM_Stop(&htim4, TIM_CHANNEL_1);
	if (retval != HAL_OK)
		 errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
   vTaskDelay (10);
	HAL_GPIO_WritePin(DRV_OUT_GPIO_Port, DRV_OUT_Pin, GPIO_PIN_RESET);	// indication to driver
	PowerState &=~ BRAKE;

	//PowerState |= P24V;
	//return(val);
}

/*******************************************************************************
*	ChargerOff
*
*******************************************************************************/

void ChargeOff()
{
   HAL_GPIO_WritePin(CHARGE_DIS_GPIO_Port, CHARGE_DIS_Pin, GPIO_PIN_SET);
   HAL_Delay(10);									// wait more time
   //PowerState &=~ P24V;
}

/*******************************************************************************
*	DriverOff
*
*******************************************************************************/

void DriverOff()
{
	HAL_GPIO_WritePin(DRV_EN_GPIO_Port, DRV_EN_Pin, GPIO_PIN_RESET);
}

/*******************************************************************************
*	LockerOff
*	Locker - Stepper off
*
*******************************************************************************/

void LockerOff()
{
	HAL_GPIO_WritePin(STP_EN_GPIO_Port, STP_EN_Pin, GPIO_PIN_RESET);
}

/*******************************************************************************
*	PrechargeOn
*
*******************************************************************************/

int32_t PrechargeOn(void)
{
   uint32_t start = 0;
   int32_t vbatt = 0x7fffffff, vsw = 0;
   int32_t cnt = 0, val;

//	verify ADC are working properly
	if ( !(HAL_ADC_GetState(&hadc3) & HAL_ADC_STATE_REG_BUSY) )
		return FALSE;
	if ( !(HAL_ADC_GetState(&hadc2) & HAL_ADC_STATE_REG_BUSY) )
		return FALSE;
	if ( !(HAL_ADC_GetState(&hadc1) & HAL_ADC_STATE_REG_BUSY) )
		return FALSE;

   val = HAL_GPIO_ReadPin(P3V3_PG_GPIO_Port, P3V3_PG_Pin);						// 1
   if ( val == 1)
   {
	   PowerState |= P3V3;
	   HAL_GPIO_WritePin(PULLER_EN_GPIO_Port, PULLER_EN_Pin, GPIO_PIN_RESET);
	   HAL_GPIO_WritePin(PRECHRG_EN_GPIO_Port, PRECHRG_EN_Pin, GPIO_PIN_SET);		// enable
	   start = xTaskGetTickCount();
	   while( ( ( vbatt - vsw ) > PRECHARGE_LIMIT ) && 									// compare voltages
			   ( ( xTaskGetTickCount() - start ) < 2000 ) )									// check timeout
	   {
	   	vsw = ADC_DMA_Buffer[DMA_VSW];
	   	vbatt = ADC_DMA_Buffer[DMA_VBATT];
			cnt ++;
	   }
	   PrechargeTime = xTaskGetTickCount() - start;
		gettxbuffer();
		txbuff_semaphor = 1;
		snprintf(txbuff, TXMSG_MAX_LEN, "voltage difference  %ld\n", vbatt - vsw);
		snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "precharge time  %ld", xTaskGetTickCount() - start);
	   HAL_Delay(500);																				// wait more time
	   if (( ( xTaskGetTickCount() - start ) < 2500 ))
	   {
		  if ( 0 == HAL_GPIO_ReadPin(PRECHRG_FBO_GPIO_Port, PRECHRG_FBO_Pin) )
		  {
			  	PowerState |= PRECHARGEON;
   			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "....OK\n");
				HAL_UART_Transmit_DMA(&huart3, (uint8_t *)txbuff, strlen(txbuff));
				return(TRUE);
		  }
		  else
		  {
			  PrechargeOff();
			  snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "....Fail\n");
			  HAL_UART_Transmit_DMA(&huart3, (uint8_t *)txbuff, strlen(txbuff));
			  return(FALSE);
		  }
	   }
	   else
	   {
	   	PrechargeOff();
	   	snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "....Fail\n");
			HAL_UART_Transmit_DMA(&huart3, (uint8_t *)txbuff, strlen(txbuff));
	   	return(FALSE);
	   }
   }
   else
   {
		gettxbuffer();
		txbuff_semaphor = 1;
   	snprintf(txbuff, TXMSG_MAX_LEN, ".... P3V3_PGOOD Fail\n");
		HAL_UART_Transmit_DMA(&huart3, (uint8_t *)txbuff, strlen(txbuff));
   	return(FALSE);
   }
}

/*******************************************************************************
*	VSWOn
*
*******************************************************************************/

int32_t VSWOn()
{
   int32_t vbatt = 0x7fffffff, vsw = 0;
//	verify ADC are working properly
	if ( !(HAL_ADC_GetState(&hadc3) & HAL_ADC_STATE_REG_BUSY) )
		return FALSE;
	if ( !(HAL_ADC_GetState(&hadc2) & HAL_ADC_STATE_REG_BUSY) )
		return FALSE;
	if ( !(HAL_ADC_GetState(&hadc1) & HAL_ADC_STATE_REG_BUSY) )
		return FALSE;

	gettxbuffer();
	txbuff_semaphor = 1;
	if (PowerState | PRECHARGEON)															// check that precharge work
   {
   	HAL_GPIO_WritePin(VSW_EN_GPIO_Port, VSW_EN_Pin, GPIO_PIN_SET);			// vsw enable
   	HAL_Delay(10);																			// wait more time
   	PrechargeOff();
   	HAL_Delay(100);																		// wait more time
   	vsw = ADC_DMA_Buffer[DMA_VSW];
   	vbatt = ADC_DMA_Buffer[DMA_VBATT];
   	if ( ( vbatt - vsw ) < PRECHARGE_LIMIT )  									// compare voltages
   	{
   		PowerState |= VSWON;
   		snprintf(txbuff, TXMSG_MAX_LEN, "voltage difference  %ld....OK\n", vbatt - vsw);
   		HAL_UART_Transmit_DMA(&huart3, (uint8_t *)txbuff, strlen(txbuff));
   		return(TRUE);
   	}
   	else
   	{
   		VSWOff();
   		snprintf(txbuff, TXMSG_MAX_LEN, "voltage difference  %ld....Fail\n", vbatt - vsw);
   		HAL_UART_Transmit_DMA(&huart3, (uint8_t *)txbuff, strlen(txbuff));
   		return(FALSE);
   	}
   }
   else
   {
		snprintf(txbuff, TXMSG_MAX_LEN, "voltage difference  %ld....Fail\n", vbatt - vsw);
		HAL_UART_Transmit_DMA(&huart3, (uint8_t *)txbuff, strlen(txbuff));
   	return(FALSE);
   }
}

/*******************************************************************************
*	DCDC24VOn
*
*******************************************************************************/

int32_t DCDC24VOn()
{
   int32_t val;

	if (PowerState | VSWON)																			// check
   {
   	HAL_GPIO_WritePin(P24V_EN_GPIO_Port, P24V_EN_Pin, GPIO_PIN_SET);				// !!!!
      HAL_GPIO_WritePin(DCDC24V_EN_GPIO_Port, DCDC24V_EN_Pin, GPIO_PIN_SET);		// enable
      HAL_Delay(100);																				// wait more time

		gettxbuffer();
		txbuff_semaphor = 1;
   	val = ADC_DMA_Buffer[DMA_P24V];
      if ( (val > DCDC24V_LOW_LIMIT) && (val < DCDC24V_HIGH_LIMIT) )
      {
		  if ( 1 == HAL_GPIO_ReadPin(P24V_PG_GPIO_Port, P24V_PG_Pin) )
		  {
			  snprintf(txbuff, TXMSG_MAX_LEN, "P24V = %ld....OK\n", val);
			  HAL_UART_Transmit_DMA(&huart3, (uint8_t *)txbuff, strlen(txbuff));
			  PowerState |= DCDC24V;
			  return(TRUE);
		  }
		  else
		  {
			  snprintf(txbuff, TXMSG_MAX_LEN, "P24V = %ld....OK\n", val);
			  HAL_UART_Transmit_DMA(&huart3, (uint8_t *)txbuff, strlen(txbuff));
			  DCDC24VOff();
			  return(FALSE);
		  }
      }
      else
      {
      	snprintf(txbuff, TXMSG_MAX_LEN, "P24V = %ld....OK\n", val);
   		HAL_UART_Transmit_DMA(&huart3, (uint8_t *)txbuff, strlen(txbuff));
      	DCDC24VOff();
      	return(FALSE);
      }
   }
}

/*******************************************************************************
*	P5VOn
*
*******************************************************************************/

int32_t P5VOn()
{
   int32_t val;

   if (PowerState | DCDC24V)															 // check
   {
	  HAL_GPIO_WritePin(P5V_EN_GPIO_Port, P5V_EN_Pin, GPIO_PIN_SET);		// enable
	  HAL_Delay(100);																		// wait more time
     gettxbuffer();
	  txbuff_semaphor = 1;

	  val = ADC_DMA_Buffer[DMA_P5V];
	  if ( (val > P5V_LOW_LIMIT) && (val < P5V_HIGH_LIMIT))
	  {
		  snprintf(txbuff, TXMSG_MAX_LEN, "P5V = %ld....OK\n", val);
		  HAL_UART_Transmit_DMA(&huart3, (uint8_t *)txbuff, strlen(txbuff));
		  PowerState |= P5V;
		  return(TRUE);
	  }
	  else
	  {
		  snprintf(txbuff, TXMSG_MAX_LEN, "P5V = %ld....Fail\n", val);
		  HAL_UART_Transmit_DMA(&huart3, (uint8_t *)txbuff, strlen(txbuff));
		  P5VOff();
		  return(FALSE);
	  }
   }
}

/*******************************************************************************
*	PullerOn
*
*******************************************************************************/

int32_t PullerOn(void)
{
   if (PowerState | P24V)															// check
   {
   	HAL_GPIO_WritePin(PULLER_EN_GPIO_Port, PULLER_EN_Pin, GPIO_PIN_SET);
	  	vTaskDelay(25);																	// wait more time
	  	return(TRUE);
   }
   else
   {
   	return(FALSE);
   }
}

/*******************************************************************************
*	BrakeOn
*
*******************************************************************************/

int32_t BrakeOn(void)
{
   HAL_StatusTypeDef retval;

   if (PowerState | P24V)															// check
   {
   	retval = HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_1);
   	if (retval != HAL_OK)
   	{
   		errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
   	}
		osDelay (25);
   	HAL_GPIO_WritePin(DRV_OUT_GPIO_Port, DRV_OUT_Pin, GPIO_PIN_SET);	// indication to driver
		PowerState |= BRAKE;
		return(TRUE);
   }
   else
   {
      //P24VOff();
   	return(FALSE);
   }
}

/*******************************************************************************
*	P24VOn
*
*******************************************************************************/

void P24V_Pulse(void);
int32_t P24VOn(void)
{
   if (PowerState | DCDC24V)																// check
   {
	  HAL_GPIO_WritePin(P24V_EN_GPIO_Port, P24V_EN_Pin, GPIO_PIN_SET);		// enable
	  P24V_Pulse();
	  HAL_Delay(10);																			// wait more time
	  PowerState |= P24V;
	  return(TRUE);
   }
   else
   {
      P24VOff();
	  return(FALSE);
   }
}

/*******************************************************************************
*	BoardInit
*	Task
*
*******************************************************************************/

void charging_init(void);
void BoardInit(void)
{
   int32_t val;

	if ( ADC_DMA_Init() == TRUE )
	{
		val = CheckBatt(1);
		xTimerChangePeriod( SAFETY_SETHandle, 500, 100 );
		xTimerChangePeriod( SAFETY_RSTHandle, 500, 100 );
		charging_init();
		if (val == TRUE)
		{
		   val = PrechargeOn();
			if (val == TRUE)
			{
			   val = VSWOn();
				if (val == TRUE)
				{
					HAL_Delay(100);
				   val = DCDC24VOn();
					if (val == TRUE)
					{
					   val = P5VOn();
						if (val == TRUE)
						{
							val = P24VOn();
							if (val == TRUE)
							{
								measure_brake_zero();
								device_init();
							}
						}
					}
				}
			}
		}
	}
   else
	   PowerState = POWEROFF;
   while(1)
	  vTaskDelay(0x7fffff00 );
}

/*******************************************************************************
*	Low Power Mode
*	close P5V
*	Close P24V
*	close DCDC24V
*	close VSW
*
*******************************************************************************/

int32_t LowPowerMode(void)
{
	int32_t vsw;

   DriverOff();
   LockerOff();
   BrakeOff();
   PullerOff();
   P5VOff();
   P24VOff();
   DCDC24VOff();
   PrechargeOff();
   VSWOff();
   vTaskDelay(2000);
	vsw = ADC_DMA_Buffer[DMA_VSW];
	if (vsw < VSW_OFF)
	{
		return TRUE;
	}
	else
	{
		return vsw;
	}
}

/*******************************************************************************
*	On Mode
*
*
*******************************************************************************/

int32_t OnMode(void)
{
   int32_t val;

   val = CheckBatt(1);

   if (val == TRUE)
   {
	   val = PrechargeOn();
	   if (val == TRUE)
	   {
		   val = VSWOn();
		   if (val == TRUE)
		   {
		   	HAL_Delay(100);
			   val = DCDC24VOn();
			   if (val == TRUE)
			   {
				   val = P5VOn();
				   if (val == TRUE)
				   {
					   val = P24VOn();
					   if (val == TRUE)
					   {
					   	return TRUE;
					   }
				   }
			   }
		   }
	   }
   }
   else
	   return FALSE;
   return val;
}

/*******************************************************************************
*	ShutDown
*
*******************************************************************************/

int32_t ShutDown(void)
{
	LowPowerMode();

   HAL_GPIO_WritePin(MCU_HOLD_GPIO_Port, MCU_HOLD_Pin, GPIO_PIN_RESET);						// enable
   HAL_Delay(10);																								// wait more time
   return(TRUE);
}

/*******************************************************************************
*	DriverOn
*
*******************************************************************************/

int32_t DriverOn(void)
{
	if ( (PowerState & VSWON) )
	{
		HAL_GPIO_WritePin(DRV_EN_GPIO_Port, DRV_EN_Pin, GPIO_PIN_SET);							// enable
      osDelay (25);
		return(TRUE);
	}
	else
	{
		return(FALSE);
	}
}

/*******************************************************************************
*	LockerOn
*	Locker - Stepper On
*
*******************************************************************************/

int32_t LockerOn(void)
{
	if ( (PowerState & VSWON) )
	{
		HAL_GPIO_WritePin(STP_EN_GPIO_Port, STP_EN_Pin, GPIO_PIN_SET);							// enable
      osDelay (10);
		return(TRUE);
	}
	else
	{
		return(FALSE);
	}
}


/*******************************************************************************
*	charging_init
*	initialization charger input is enabled '0'
*	Measure voltage on charging input is lower than vbatt
*	if > 30V than connected to charger
*		leave enabled
*	else
*		disable
*
*******************************************************************************/

void charging_init(void)
{
	int32_t val;

	val = ADC_DMA_Buffer[4];

	if ( val < 1000 )																						// 30V
	{
		HAL_GPIO_WritePin(CHARGE_DIS_GPIO_Port, CHARGE_DIS_Pin, GPIO_PIN_SET);			// disable
		HAL_Delay(10);																						// wait more time
	}
}

/*******************************************************************************
*	ChargeOn
*	Measure voltage on charging input is lower than vbatt
*	*VSENSE = (Vref/4095.0) / RES_RATIO  * val;
*
*******************************************************************************/

int32_t ChargeOn(void)
{
	int32_t val;

	val = ADC_DMA_Buffer[DMA_VCHARGER];
	if ( val > 1000 )																						// 30V
	{
		return FALSE;
	}
	else
	{
		HAL_GPIO_WritePin(CHARGE_DIS_GPIO_Port, CHARGE_DIS_Pin, GPIO_PIN_RESET);		// enable
		HAL_Delay(10);																						// wait more time
		return(TRUE);
	}
}

// control shift make  /*   */
/*******************************************************************************
*	P24V_Pulse
*
*******************************************************************************/

void P24V_Pulse(void)
{
  //volatile int k, p, r = 7;

//  HAL_GPIO_WritePin(P24V_EN_GPIO_Port, P24V_EN_Pin, GPIO_PIN_SET);
  HAL_Delay(10);
/*
  for (k = 0;k < 20000;k ++)
  {
	  HAL_GPIO_WritePin(P24V_EN_GPIO_Port, P24V_EN_Pin, GPIO_PIN_RESET);
	  HAL_GPIO_WritePin(P24V_EN_GPIO_Port, P24V_EN_Pin, GPIO_PIN_SET);
	  if (k > 14000)
		 for (p = 0;p < 3;p ++)
			r = r * 13 + 11;
	  if (k > 14600)
		 for (p = 0;p < 9;p ++)
			r = r * 13 + 11;
	  if (k > 15000)
		 for (p = 0;p < 9;p ++)
			r = r * 30 + 11;
  }
 */
}

/*
retval = AdcChannelConversion(ADC_VSW_CV, ADC_VSW_C, ADC_SAMPLETIME_28CYCLES, 100L, &val1);
retval = AdcChannelConversion(ADC_VBAT_CV, ADC_VBAT_C, ADC_SAMPLETIME_28CYCLES, 100L, &val2);
retval = (AdcChannelConversion(&hadc1, ADC_CHANNEL_VREFINT, ADC_SAMPLETIME_480CYCLES, 100L, &val3));
retval = (AdcChannelConversion(&hadc1, ADC_CHANNEL_TMPRINT, ADC_SAMPLETIME_480CYCLES, 100L, &val4));
retval = (AdcChannelConversion(&hadc1, ADC_CHANNEL_VBAT, ADC_SAMPLETIME_480CYCLES, 100L, &val5));
*/

