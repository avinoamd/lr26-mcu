/*
 * CLI.c
 *
 *  Created on: Jul 1, 2020
 *      Author: avinoam.danieli
 */

/* includes ----------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include <stdio.h>
#include <string.h>
#include "cli.h"
#include "defs.h"
#include "globals.h"
#include "uart.h"

CLI_command_T CommandToExe;

char nullbuf[100];

/********************************************************************
*
********************************************************************/
int32_t CLI_ConvertStringToCommand(char* sRecieve)
{
	char *cp;

	//printf(sRecieve);
	//printf("\t\t\t");
	if ( strlen(sRecieve) < 3)
		return (OKr);

	cp = strchr(sRecieve, '=');
	if (cp == NULL)
	{
		cp = strchr(sRecieve, ' ');
		if (cp != NULL)
		{
			if ( EOF == sscanf(cp, "%s", nullbuf) )
			{
				sscanf(sRecieve, "%s", CommandToExe.sCommand);
				CommandToExe.ArgType = GETPAR;
			}
			else
			{
				sscanf(sRecieve, "%s", CommandToExe.sCommand);
				strcpy(CommandToExe.sParameter, cp);
				CommandToExe.ArgType = SETPAR;
			}
		}
		else
		{
			sscanf(sRecieve, "%s", CommandToExe.sCommand);
			CommandToExe.ArgType = GETPAR;
		}
	}
	else		// =
	{
		*cp = ' ';
		sscanf(sRecieve, "%s", CommandToExe.sCommand);
		strcpy(CommandToExe.sParameter, cp + 1);
		CommandToExe.ArgType = SETPAR;
	}
	//printf("Command %s\tParams %s\tArgType %d\n", CommandToExe.sCommand, CommandToExe.sParameter, CommandToExe.ArgType);
//	HAL_Delay(500);
	return (OKr);
}
char linmsg[MSG_MAX_LEN];
void CLIErrorHandler(CLIRetCode_t errorFromList);
/********************************************************************
*
********************************************************************/
void CLI()
{
	static int32_t next = 0;
	int32_t	i;
	int32_t crflg = 0;
	int32_t eom = 0;

	HAL_UART_Receive_DMA(&huart3, (void *)msg, (uint16_t)MSG_MAX_LEN);  	// start uart dma
	printf("CLI\n");
	vTaskDelay(100);
	for(;;)
	{
		vTaskDelay(100);
		for (i = 0;i < MSG_MAX_LEN; i ++)									// copy cyclic buffer to linear buffer
		{
			linmsg[i] = msg[(i + next) & (MSG_MAX_LEN-1)];					// cyclic
			if (linmsg[i] == '\r')											// check end of message
			{
				crflg = 1;
				eom = i;
				break;
			}
			if (linmsg[i] == 0)												// no new message
			{
				crflg = 0;
				break;
			}
		}
		if (crflg == 0)
			continue;
		else																// eom found
		{
			linmsg[eom + 1] = 0;											// terminate string
			for (i = 0;i <= eom; i ++)										// clear cyclic buffer
			{
				msg[(i + next) & (MSG_MAX_LEN-1)] = 0;
			}
			next = (next + eom + 1) & (MSG_MAX_LEN-1);

			if (OKr==CLI_ConvertStringToCommand(linmsg))
			{
				CLIErrorHandler(CLI_ExecutCommand(&CommandToExe,  gCliCommandsList));
				//printf("\nExcecute\n");
			}
		}
	}
}

/********************************************************************
*
********************************************************************/
//char lastmsg[5][16];
//int32_t buffcnt = 0;
int32_t lastsize;
int32_t msgcnt = 0;
CLIRetCode_t CLI_ExecutCommand(CLI_command_T* pCommand ,  CLI_COMMAND_LIST_T const * pCommandList)
{
  	uint8_t i =0  ;
  	CLIRetCode_t RetCode=OKr;

  	while (pCommandList[i].pName != NULL)
  	{
		if (strncasecmp(pCommand->sCommand, pCommandList[i].pName, MSG_MAX_LEN) == 0)
		{
			// Call the function pointer in the command record.
			(void)pCommandList[i].pHandler(pCommand);
			RetCode =  OKr;
			break;
		}
		i++;
  	}
  	if (pCommandList[i].pName == NULL)
    	RetCode =  COMMAND_NOT_FOUND;
	msgcnt ++;
	return RetCode;
}

/********************************************************************
* errorHandler
*
* Write to SWO
* Write to CLI UART3
* Save on Flash
* Disable interrupts
* Flash Blue LED and external red LED
*
********************************************************************/

void errorHandler(int32_t num, char const * FILE, int LINE, char const * PRETTY_FUNCTION)
{
	sprintf(txebuff,  "File = %s  ", FILE);
	sprintf(txebuff+strlen(txebuff),  "Error line = %d  ", LINE);
	sprintf(txebuff+strlen(txebuff), "%s  ",  PRETTY_FUNCTION);
	sprintf(txebuff+strlen(txebuff), "UPTIME = %lu \n\r", xTaskGetTickCount() / 1000);
	printf("%s", txebuff);
	HAL_UART_Transmit(&huart3, (uint8_t *)txebuff, strlen(txebuff), 1000L);
	// leds, flash, _disable
	while(1);
}

/***************************************************************
*	CLIErrorHandler
***************************************************************/
void CLIErrorHandler(CLIRetCode_t errorFromList)
{
  	if (errorFromList)
  	{

  		while (txbuff_semaphor != 0);			// wait for buffer
		txbuff_semaphor = 1;

	   	switch(errorFromList)
	   	{
	    	case OKr:
//				sprintf(txbuff, "OK\r\n");
	      	break;
	      	case COMMAND_NOT_FOUND:
				sprintf(txbuff, "COMMAND_NOT_FOUND\r\n");
	      	break;
	      	case NO_AUTHORIZATION:
//				sprintf(txbuff, "NO_AUTHORIZATION\r\n");
	      	break;
	      	case OUT_OF_LIMITS:
//				sprintf(txbuff, "OUT_OF_LIMITS\r\n");
	      break;
	      case NO_permission:
//			  sprintf(txbuff, "COMMAND_NOT_ALLOWED\r\n");
	      break;
	      default:
			  sprintf(txbuff, "ABNORMAL_BEHAVIOR\r\n");
	      break;
	   }
	   while(HAL_UART_Transmit_DMA(&huart3, (uint8_t *)txbuff, strlen(txbuff)));
  }
}


