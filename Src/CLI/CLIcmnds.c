/**
  ******************************************************************************
  * File Name          : CLIcmnds.c
  * Author             :
  * Date               :
  * Description        : This file provides code for protocol
  ******************************************************************************
  *
  * COPYRIGHT(c) 2014 V-Gen
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/

#include "main.h"
#include "cmsis_os.h"
#include "stm32f4xx_hal.h"
#include "defs.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "CLIcmnds.h"
#include "CLI.h"
#include "uart.h"
#include "globals.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/

/* External variables --------------------------------------------------------*/

/* Private functions -------------------------------------------------------------------------------*/
/* Exported types ----------------------------------------------------------------------------------*/
/*******************************************************************************
*	gettxbuffer
*	wait on semaphor for 500mSec
*	HAL_UART_TxCpltCallback() at uart.c
*
*******************************************************************************/
int32_t gettxbuffer(void)
{
	uint32_t cnt = 0;

	while ( txbuff_semaphor != 0 )  			// wait for buffer
	{
		vTaskDelay(10);
		if ( (cnt++) > 50 ) // timeout
			return FALSE;
	}
	return TRUE;
}
/*******************************************************************************
*	sendtxbuffer
*	blocking
*
*******************************************************************************/
int32_t sendtxbuffer(void)
{
	uint32_t tickstart;

	tickstart = HAL_GetTick(); 		/* Get tick */
	while(HAL_UART_Transmit_DMA(&huart3, (uint8_t *)txbuff, strlen(txbuff)))
		if ( (HAL_GetTick() - tickstart) > 500 ) // timeout
		{
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			break;
		}
	return TRUE;
}

/*******************************************************************************
*	Uptime
*******************************************************************************/
int32_t Uptime(CLI_command_T* command)
{

	uint32_t tickstart;

	tickstart = HAL_GetTick(); 				// Get tick
	while ( txbuff_semaphor != 0 )  		// wait for buffer
		if ( (HAL_GetTick() - tickstart) > 500 ) // timeout
		{
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			break;
		}
	txbuff_semaphor = 1;
	//sprintf(txbuff,  "uptime=%d    \n\rcycnt=%d   \n\r", uptime, DWT->CYCCNT);
	sprintf(txbuff,  "UPTIME=%lu    \n\r", HAL_GetTick() / 1000);
	tickstart = HAL_GetTick(); 								// Get tick
	while(HAL_UART_Transmit_DMA(&huart3, (uint8_t *)txbuff, strlen(txbuff)))
		if ( (HAL_GetTick() - tickstart) > 500 ) 			// timeout
		{
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			break;
		}

	return TRUE;
}

void versionformain(void)
{
	sprintf(txbuff,  "Version 1.10  ");
}

void datetimeformain(void)
{
	sprintf(txbuff+strlen(txbuff), " %s",  __DATE__);
	sprintf(txbuff+strlen(txbuff), " %s",  __TIME__);
}

/*******************************************************************************
*	GetVersion
*******************************************************************************/
int32_t GetVersion(CLI_command_T* command)
{
	while (txbuff_semaphor != 0)			// wait for buffer
		txbuff_semaphor = 1;
	versionformain();
	sprintf(txbuff+strlen(txbuff), " PC-0019-01");
	datetimeformain();
	sprintf(txbuff+strlen(txbuff), " \n\r");
	//sprintf(txbuff+strlen(txbuff), " %ld\n\r",  __TID__);
	while(HAL_UART_Transmit_DMA(&huart3, (uint8_t *)txbuff, strlen(txbuff)));

	return TRUE;
}
/*********************************************************************
*	help
*********************************************************************/
int32_t help(CLI_command_T* command)
{
  	CLI_COMMAND_LIST_T *p = (CLI_COMMAND_LIST_T*)gCliCommandsList;
	uint32_t tickstart;

   while(p->pName)
   {
   	if ( p->permission != DNP)
   	{
  			tickstart = HAL_GetTick(); 		/* Get tick */
			while ( txbuff_semaphor != 0 )  	// wait for buffer
			{
				vTaskDelay(2);
				if ( (HAL_GetTick() - tickstart) > 1000 ) // timeout
				{
					errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
					break;
				}
			}
			txbuff_semaphor = 1;
        	sprintf(txbuff, "%-20s %s\n\r", p->pName, p->pDescription);
  			tickstart = HAL_GetTick(); 		/* Get tick */
			while(HAL_UART_Transmit_DMA(&huart3, (uint8_t *)txbuff, strlen(txbuff)))
			{
				vTaskDelay(2);
				if ( (HAL_GetTick() - tickstart) > 1000 ) // timeout
				{
					errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
					break;
				}
			}
   	}
		p++;
   }
   return 0;
}

/*******************************************************************************
*	PulsePeriod
*	PRR
*	RS parameter
*******************************************************************************/
int32_t PulsePeriod(CLI_command_T* command)
{
/*
	uint16_t stam;
	float fl;
	int32_t var;

  	if (command->ArgType == GETPAR)
  	{

		if (gettxbuffer() == FALSE)
		{
			errorHandler(-3, __LINE__, __PRETTY_FUNCTION__);
			return -3;
		}
		//CPLD_SendRec16(0, prr_period, (uint16_t *)&Seeder.period, 3, 100L);
		if (Prrsrc == EXTERNAL)
		{
			fl = htim3.Instance->CCR1;
			fl =  (fl + 2.0) * (100.0 / 48.0);
			var = (int32_t)fl;
	    	sprintf (txbuff, "%s=%d\r\n",command->sCommand, var );
		}
		else
    		sprintf (txbuff, "%s=%d\r\n",command->sCommand, Seeder.period );
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
  	}
  	else if ( command->ArgType == SETPAR)
  	{
		sscanf(command->sParameter, "%hd", &stam);
		if (stam >= Maxprr)								// verify value is legal
		{
			if ( (stam - 10) > Seeder.invt_period )
			{
				Seeder.period = stam;
				Seeder.pcw_period = Seeder.period - Seeder.invt_period;
				if 	(LaserState != IDLE_ST)
				{
					CPLD_SendRec16(Seeder.pcw_period, cw_period+1, &stam, 3, 100L);
					CPLD_SendRec16(Seeder.period, prr_period+1, &stam, 3, 100L);
				}
			}
		}
		if (gettxbuffer() == FALSE)
		{
			errorHandler(-3, __LINE__, __PRETTY_FUNCTION__);
			return -3;
		}
    	sprintf (txbuff, "%s=%d\r\n", command->sCommand, Seeder.period);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   	}
*/
    return 1;
}

