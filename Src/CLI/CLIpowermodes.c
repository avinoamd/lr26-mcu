/*
 * CLIpowermodes.c
 *
 *  Created on: 16 Sep 2020
 *      Author: avinoam.danieli
 */

#include "main.h"
//#include "FreeRTOS.h"
//#include "timers.h"
//#include "globals.h"
#include "defs.h"
#include "CLI.h"
#include "CLIcmnds.h"
//#include "ADC.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "BoardInit.h"
#include "uart.h"
#include "PushButton.h"
//#include "init.h"

/*******************************************************************************
*	CLI_LPM
*	CLI
*
*******************************************************************************/

int32_t cli_powermode(CLI_command_T* command)
{
   int32_t stam;

   if (command->ArgType == GETPAR)
   {
	  if (gettxbuffer() == FALSE)
      {
 		 errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
         return -3;
      }
	  //itoa (PowerState,txbuff+50,2);
	  snprintf (txbuff, TXMSG_MAX_LEN, "%s=%c%c%c%c%c%c%c%c \r\n",command->sCommand, BYTE_TO_BINARY(PowerState));
	  txbuff_semaphor = 1;
	  sendtxbuffer();
	  return TRUE;
   }
   else if ( command->ArgType == SETPAR)
   {
	   sscanf(command->sParameter, "%ld", (int32_t *)&stam);
	   if (stam == 0)
	   {
	   	xTaskNotify( xTaskToNotifyPBTask, OFF, eSetValueWithOverwrite );
//	   	stat = ShutDown();
	   }
	   else if (stam == 1)
	   {
	   	xTaskNotify( xTaskToNotifyPBTask, LPM, eSetValueWithOverwrite );
//	   	stat = LowPowerMode();
	   }
	   else { if (stam == 2)
	   {
	   	xTaskNotify( xTaskToNotifyPBTask, ON, eSetValueWithOverwrite );

//	   	stat = OnMode();
	   }}

		if (gettxbuffer() == FALSE)
		{
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			return -43;
		}
		vTaskDelay(1);																					// enable PowerState to update
		snprintf (txbuff, TXMSG_MAX_LEN, "%s=%c%c%c%c%c%c%c%c \r\n",command->sCommand, BYTE_TO_BINARY(PowerState));
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   }
   return 1;
}

