/*
 * CommandList.c
 *
 *  Created on: Jul 1, 2020
 *      Author: avinoam.danieli
 */

#include "main.h"
#include "defs.h"
#include <stdio.h>
#include "CLI.h"
#include "CLIcmnds.h"
#include "ADC.h"
#include "LEDs.h"
#include "cmdpower.h"
#include "safety.h"
#include "BCR.h"
#include <lsm6ds3.h>
#include "utils.h"
#include "print.h"
#include "BMS.h"
#include "eeprom_utils.h"
#include "PLC.h"
#include "bit.h"
#include "periodic.h"
#include "Battery.h"

const CLI_COMMAND_LIST_T gCliCommandsList[] /*@ "FLASH"*/ =
{
	// name			function		#arg	permission	description
	//--------------------------------------------------------------------------
    { "??",			help,			0,		CLIENT,		"list commands" },           		 /*1*/
    { "HELP",		help,			0,		CLIENT,		"list commands" },            		/*2*/
    { "RESET", 	reset,		0,		CLIENT,		"Do not preferom this command !!!!"},
    { "RESET", 	reset,		0,		DNP,			"Reset!!"},
    { "IWDG", 		IWDG_en,		0,		CLIENT,		"IWDG Enable"},
    { "BL", 		bootloader,	0,		CLIENT,		"Enter BoorLoader"},
    { "PPLCSWO", 	pplc_swo,	0,		CLIENT,		"Print PLC messages to SWO, EEPROM"},
	 { "PPLCUART3",pplc_uart3,	0,		CLIENT,		"Print PLC messages to UART3, EEPROM"},
	 { "PBMSUART3",pbms_uart3,	0,		CLIENT,		"Print BMS messages to UART3, EEPROM"},
	 { "PLCCNT",	plc_cnt,		0,		CLIENT,		"Print PLC messages counter"},
	 { "PLCTIMEOUT",plc_timeout,0,	CLIENT,		"PLC comm timeout"},
    { "PSOMSWO", 	psom_swo,	0,		CLIENT,		"Print SOM messages to SWO, EEPROM"},
	 { "PSOMUART3",psom_uart3,	0,		CLIENT,		"Print SOM messages to UART3, EEPROM"},
	 { "SOMCNT",	som_cnt,		0,		CLIENT,		"Print SOM messages counter"},
	 { "TaskList",	tasklist,	0,		CLIENT,		"Task List"},
	 { "RTS",		rts,			0,		CLIENT,		"Run time Status"},
	 { "RRTS",		rrts,			0,		CLIENT,		"Reset Run time Status"},
	 { "MSG",		message,		0,		CLIENT,		"Hello Avinoam"},
//  { "ERRORS", 	GetLaserErrors,	1,		CLIENT,		"Send string of error"},
	 { "VER",		GetVersion,	0,		CLIENT,		"get Version"},
	 { "UPTIME",	Uptime,		0,		CLIENT,		"UPTIME"},
	 { "params_update", params_update, 0, CLIENT,"Update parameters RAM->EEPROM"},
	 { "BIT", 		BIT_CLI, 	0, 	CLIENT,		"Run full diagnostics"},


	 { "     ",		NULL,			0,		CLIENT,		"      "},
	//---------- 	ADC    ---------------------------------------------------------
	{ "TMPR",		CPUTmpr,			0,		CLIENT,		"Get CPU Temperature"},
	{ "VREF",		IntVref,			0,		CLIENT,		"Get CPU Internal voltage reference"},
	{ "BRKCURR",	brkcurr,			0,		CLIENT,		"Get Brakes Current"},
	{ "vbatt",		vbatt,			0,		CLIENT,		"Get Battery Voltage"},
	{ "vsw",			vsw,				0,		CLIENT,		"Get Switch Voltage"},
	{ "diff",		diff,				0,		CLIENT,		"Get Drain Source Voltage"},
	{ "p5v",			p5v,				0,		CLIENT,		"Get P5V Voltage"},
	{ "p24v",		p24v,				0,		CLIENT,		"Get P24V Voltage"},
	{ "p3v3",		p3v3,				0,		CLIENT,		"Get P3V3 Voltage"},
	{ "vcharge",	vcharge,			0,		CLIENT,		"Get Charger Voltage"},
	{ "vswcurr",	vswcurr,			0,		CLIENT,		"Get Switch Current"},
	{ "vbrake",		vbrake,			0,		CLIENT,		"Get Brakes Voltage"},
	{ "GetV",		GetV,				0,		CLIENT,		"Get Voltages"},
	{ "GetC",		GetC,				0,		CLIENT,		"Get Currents"},
	{ "     ",		NULL,				0,		CLIENT,		"      "},
	//---------- 	Control    ---------------------------------------------------------
	{ "LEDcmd",		LEDcmd,			0,		CLIENT,		"Set LED (num, power, on, off)"},
	{ "DrvEn",		drv_en,			0,		CLIENT,		"Driver Enable"},
	{ "LockerEn",	locker_en,		0,		CLIENT,		"Locker Enable"},
	{ "     ",		NULL,				0,		CLIENT,		"      "},
	//---------- 	Power      ---------------------------------------------------------
	{ "prechrg_en",	prechrg_en,		0,		CLIENT,		"PreCharge Enable"},
	{ "vsw_en",			vsw_en,			0,		CLIENT,		"VSW Enable"},
	{ "DCDC24V_en",	DCDC24V_en,		0,		CLIENT,		"DCDC24V Enable"},
	{ "P5V_en",			P5V_en,			0,		CLIENT,		"P5V Enable"},
	{ "P24V_en",		P24V_en,			0,		CLIENT,		"P24V Enable"},
	{ "Puller_en",		Puller_en,		0,		CLIENT,		"Puller Enable, 0 - Shut, 1 - On"},
	{ "brake_open",	brake_en,		0,		CLIENT,		"Brake Open"},
	{ "Charge_dis",	charge_en,		0,		CLIENT,		"Charging Disable, 1 - disable, 0 - enable"},
	{ "Pwrmd",			cli_powermode,	0,		CLIENT,		"0 -shutdown, 1 - low power mode , 2- on"},
	{ "SOMrst",			som_rst,			0,		CLIENT,		"0 -SOM go, 1 - SOM Reset"},
	{ "Key",				getkey,			0,		CLIENT,		"0 - Normal, 1 - Override"},
	{ "STO",				getsto,			0,		CLIENT,		"STO, 0 - Stop, 1 - Drive"},
	{ "       ",		NULL,				0,		CLIENT,		"      "},
	//---------- 	Safety   ----------------------------------------------------------
	{ "Safety_SET",	SafetySETCLI,		0,		CLIENT,	"Safety SET"},
	{ "Safety_RST",	SafetyRSTCLI,		0,		CLIENT,	"Safety RST"},
	{ "Sfs",				SafetySETCLI,		0,		DNP,		"Safety SET"},
	{ "Sfr",				SafetyRSTCLI,		0,		DNP,		"Safety RST"},
	{ "Safety_ALERT",	SafetyAlertCLI,	0,		CLIENT,	"Safety Alert"},
	//---------- Barcode Reader ----------------------------------------------------------
	{ "BCR_getdata",	BCR_CLI_getdata,	0,		CLIENT,	"BCR getdata"},
	{ "BCR",				BCR_CLI_getdata,	1,		DNP,		"BCR getdata"},
	{ "BCREN",			BCR_CLI_en,			0,		CLIENT,	"BCR Enable"},
	{ "IMU",				IMU_CLI_getdata,	0,		CLIENT,	"IMU getdata"},
	{ "BMS",				BMS_CLI,				0,		CLIENT,	"BMS data"},
	{ "SOC",				SOC_CLI,				0,		CLIENT,	"State of Charge"},
	{ "       ",		NULL,					0,		CLIENT,		"      "},
	//---------- 	Parameters   ----------------------------------------------------------
	{ "BrakeCurrent",	Brake_Current,		0,		CLIENT,	"Set Brake Current"},
	{ "BC",				Brake_Current,		0,		DNP,		"Set Brake Current"},
    {NULL},   // __line
};

/*

const CLI_COMMAND_LIST_T gCliCommandsList[CLI_COMMANDS] @ "FLASH" =
{
	// name			function		#arg	permission	description
	//--------------------------------------------------------------------------
    { "?",			help,			0,		CLIENT,		"list commands" },           		 1
    { "HELP",		help,			0,		CLIENT,		"list commands" },            		2
    { "RESETT", 	MCU_Reset,		1,		CLIENT,		"Do not preferom this command !!!!"},
    { "ERRORS", 	GetLaserErrors,	1,		CLIENT,		"Send string of error"},
//    { "TEST", 	        TestAll,		1,RND,		"Test all drivers, TEST USE ONLY !!!!"},
//    { "TESTIO", 	TestIO,		        1,RND,		"Test IO, TEST USE ONLY !!!!"},
//    { "BOOT",	 	MCU_BOOT,		1,RND,		"Load new code"},
//    { "UNLOCK",		LaserSuperUserPermission,1,CLIENT,	"set/get user permission"},
	{ "VER",		GetVersion,		0,		CLIENT,		"get Version"},
	{ "VERCPLD",	GetCPLDVer,		0,		CLIENT,		"CPLD Version"},
	{ "CPLDID",		GetCPLDID,		0,		CLIENT,		"CPLD ID"},
	{ "UPTIME",		Uptime,			0,		CLIENT,		"UPTIME"},
//---------- Seeder Temperature  commands---------------------------------------
	{  "T1",		TecTmpr,		1,		CLIENT,		"Get TEC setpoint"},
	{  "TECTSET",	TecTmpr,		1,		CLIENT,		"Get TEC setpoint"},
	{  "TECT",		GetTecTmpr,		1,		CLIENT,		"Get TEC temperature"},						//	adc ticks
	{  "TECTMIN",	TEC_Min,		1,		CLIENT,		"TEC min working temperature"},
	{  "TECTMAX",	TEC_Max,		1,		CLIENT,		"TEC max working temperature"},
	{  "TECP",		TecP,			1,		CLIENT,		"TEC P parameter of PID"},
	{  "TECI",		TecI,			1,		CLIENT,		"TEC I parameter of PID"},
//---------- Seeder  commands---------------------------------------------------
	{  "PRR",			PulsePeriod,	1,	CLIENT,		"Set/Get laser PRR "},
	{  "SDRPW",			SeedPulseWidth,	1,	CLIENT,		"Get/Set seeder pw "},
	{  "AOMPW",			CWPeriod,		1,	CLIENT,		"Get/Set aom width "},
	{  "INVTIME",		Invtime,		1,	CLIENT,		"Get/Set inversion width "},
	{  "WDACA",			WriteDacA,		1,	CLIENT,		"Write seeder DAC A "},
	{  "WDACB",			WriteDacB,		1,	CLIENT,		"Write seeder DAC B "},
	{  "WDACC",			WriteDacC,		1,	CLIENT,		"Write seeder DAC C "},
	{  "WDACD",			WriteDacD,		1,	CLIENT,		"Write seeder DAC D "},
//---------- FAN  commands---------------------------------------------------
	{  "FAN",			FAN,			1,	CLIENT,		"Set FAN Speed "},
//---------- General  commands---------------------------------------------------
	{  "LOADDEFAULT",	LoadDefaultVal,	1,	CLIENT,		"Load Factory defaults "},
//---------- BOOSTER Temperature  commands---------------------------------------
//	{  "T1BST",			TecTmpr,		1,		CLIENT,		"Get Booster TEC setpoint"},
	{  "TECTBST",		TECTBST,		3,		CLIENT,		"Get Booster TEC temperatures"},		//	adc ticks
	{  "TECTBSTSET",	TecBSTTmpr,		1,		CLIENT,		"Get/Set Booster TEC setpoint"},
	{  "TECTBSTMIN",	TECBST_Min,		1,		CLIENT,		"Booster TEC min working temperature"},
	{  "TECTBSTMAX",	TECBST_Max,		1,		CLIENT,		"Booster TEC max working temperature"},
	{  "TECBSTP",		TecBST_P,		1,		CLIENT,		"Booster TEC P parameter of PID"},
	{  "TECBSTI",		TecBST_I,		1,		CLIENT,		"Booster TEC I parameter of PID"},
//---------------BOOSTER commands-------------------
	{  "S2IPLS",		BoosterCurrent,	1,		CLIENT,		"Get/Set Booster pulse current"},
//---------------stage1 commands-------------------
	{  "S1IPLS",		Stg1Current,	1,		CLIENT,		"Get/Set 1st stage pulse current"},
//----------Temperature  commands---------------------
	{  "BRDT",			BoardTmpr,		3,		CLIENT,		"Get board temperatures"},				// celcius
	{  "CPUT",			CPUTmpr,		3,		CLIENT,		"Get board temperatures"},				// celcius
	{  "HST",			HsTemp,			1,		CLIENT,		"Get heat sink temperature"},			// adc ticks
//-------------Laser-------------------
	{  "ARM",			ArmLaser,		1,		CLIENT,		"Set Get laser Arm state"},
	{  "ARMDLY",		armdly,			1,		CLIENT,		"Set Get laser Arm to Booster delay"},
	{  "FIRE",			ActivateLaser,	1,		CLIENT,		"Activate laser / is laser active"},
	{  "STATE",			GetLaserState,	1,		CLIENT,		"Get laser detail status "},
//	{  "PR",			GetLaserState,	1,		CLIENT,		"Get laser detail status "},
//	{  "PO",			GetLaserState,	1,		CLIENT,		"Get laser detail status "},
	{  "MAXPRR",		MaxPRR,			1,		CLIENT,		"Set Get MaxPRR"},
	{  "INVTIMEMAX",	Maxinvtime,		1,		CLIENT,		"G   Maxinvtime"},
	{  "S2IMAX",		s2imax,			1,		CLIENT,		"Set Get max current"},
	{  "IGAINMAX",		igainmax,		1,		CLIENT,		"Set Get igainmax"},
	{  "IGAIN",			igain,			1,		CLIENT,		"Set Get Booster current as percentage of igainmax"},
	{  "UPDATE",		UpdateLine,		1,		CLIENT,		"U    "},
	{  "OPNUM",			opnum,			1,		CLIENT,		"Get/Set operating point "},
	{  "OPNAME",		opname,			1,		CLIENT,		"Get/Set operating point name"},
	{  "GETOPLINE",		getopline,		1,		CLIENT,		"Get line from OP table"},
	{  "PWR",			pwr,			1,		CLIENT,		"Get/Set POIP "},
	{  "PRRSRC",		prrsrc,			1,		CLIENT,		"Get/Set Int=1 Ext=2 PRR "},
	{  "RS",			rs_ttl,			1,		CLIENT,		"Get/Set Int=1 Ext=2 PRR "},
	{  "TMPRLMT",		tmprlmt,		1,		CLIENT,		"Get/Set Temperature limits "},
//	{  "STATUS",		GetLaserStatus,	1,		CLIENT,		"Get laser detail status "},
//		POIP

//-------------------BRFD---------------
	{  "BREN",			BackReflecEN,		1,	CLIENT,		"Get/Set back reflection functionality on/off "},
	{  "BRTH",			BackReflecTH,		1,	CLIENT,		"Get/Set back reflection th value "},
	{  "BRLEVEL",		BackReflecLevl,		2,	CLIENT,		"Get back reflection ADC level "},
	{  "BRCOUNTER",		BackReflecCount,	1,	CLIENT,		"Get number of back reflection counter "},
//-------------Telemtry-------------------
	{  "GETV",			GetV,				1,	CLIENT,		"Get Voltages"},
	{  "GETC",			GetC,				1,	CLIENT,		"Get Currents"},
//-------------SHG-------------------
	{  "OVTM",			Ovtm,				1,	CLIENT,		"Get SHG Oven TMPR"},
	{  "OVM",			ovm,				1,	CLIENT,		"SHG Oven manual"},
	{  "OVSET",			ovset,				1,	CLIENT,		"Get/Set SHG Oven Setpoint"},
	{  "OVDC",			ovdc,				1,	CLIENT,		"Get/Set SHG Oven manual DC"},
	{  "OVP",			ovp,				1,	CLIENT,		"Get/Set SHG Oven kP"},
	{  "OVI",			ovi,				1,	CLIENT,		"Get/Set SHG Oven kI"},
	{  "OVD",			ovd,				1,	CLIENT,		"Get/Set SHG Oven kD"},
	{  "OVW",			ovw,				1,	CLIENT,		"Get/Set SHG Oven Window"},
	{  "OVTMMIN",		OvtmMin,			1,	CLIENT,		"Get/Set SHG Oven Min Tmpr"},
	{  "OVTMMAX",		OvtmMax,			1,	CLIENT,		"Get/Set SHG Oven Max Tmpr"},
//-------------SHG-------------------
//-------------SHG-------------------
	{  "THGOVTM",		THGOvtm,			1,	CLIENT,		"Get SHG Oven TMPR"},
//	{  "OVM",			ovm,				1,	CLIENT,		"SHG Oven manual"},
	{  "THGOVSET",		THGovset,			1,	CLIENT,		"Get/Set SHG Oven Setpoint"},
//	{  "OVDC",			ovdc,				1,	CLIENT,		"Get/Set SHG Oven manual DC"},
	{  "THGOVP",		THGovp,				1,	CLIENT,		"Get/Set SHG Oven kP"},
	{  "THGOVI",		THGovi,				1,	CLIENT,		"Get/Set SHG Oven kI"},
	{  "THGOVD",		THGovd,				1,	CLIENT,		"Get/Set SHG Oven kD"},
//	{  "OVW",			ovw,				1,	CLIENT,		"Get/Set SHG Oven Window"},
	{  "THGOVTMMIN",	THGOvtmMin,			1,	CLIENT,		"Get/Set SHG Oven Min Tmpr"},
	{  "THGOVTMMAX",	THGOvtmMax,			1,	CLIENT,		"Get/Set SHG Oven Max Tmpr"},
//-------------SHG-------------------




//{  "LS",		StatusGet,		0,	CLIENT,		MAX_STATES,	"Get laser status"},
//{  "STATUS",	GetLaserStatus,		1,	CLIENT,		MAX_STATES,	"Get laser detail status "},
//{  "EVENT",		GetLaserEvent,		1,	CLIENT,		MAX_STATES,	"Get last laser event "},
//{  "RS",		RsTTLmode,		1,	CLIENT,		STDBY,		"Set/Get rs232 mode"},
//{  "LATCH",		LatchInput,		1,	CLIENT,		MAX_STATES,	"Latch the inputs of d25 connector "},
//{  "PWR",		PwrChange,		1,	CLIENT,		MAX_STATES,	"Set/Get lasr power percentage"},
//{  "POIP",		PoipChange,		1,	CLIENT,		MAX_STATES,	"Set/Get lasr power percentage"},
//{  "LG",		LaserGuide,		1,	CLIENT,		ARM,		"Activate laser guide beam"},
//{  "PR",		ArmLaser,		1,	CLIENT,		MAX_STATES,	"Arm laser / is laser armed"},
//{  "PO",		ActivateLaser,		1,	CLIENT,		MAX_STATES,	"Activate laser / is laser active"},

//{  "SDRPWMIN",		PulseWidthMin,		1,	RND,		STDBY,		"Get/Set seeder min pw in nsec"},
//{  "SDRPWMAX",		PulseWidthMax,		1,	RND,		STDBY,		"Get/Set seeder max pw in nsec"},

//	{  "TECD",		TecD,			1,		CLIENT,		"TEC D parameter of PID"},
//	{  "TECTERRMIN",	TEC_MinStop,		1,	TECHNICIAN,	STDBY,		"TEC min error temperature"},
//	{  "TECTERRMAX",	TEC_MaxStop,		1,	TECHNICIAN,	STDBY,		"TEC max error temperature"},
//	{  "TECIWIN",		TecIwin,		1,	RND,		STDBY,		"TEC I limit window parameter of PID"},



//    { "UPDATE",		Updatearams,		1,TECHNICIAN,	"sent parametrs to non-volotile memory"},
//    { "DEBUG",		Debug,			1,CLIENT,	"debug plots: 0-none, 1-events ,2- Error report, 4-PRR, 8-AWG, 16-PID, 32-PWR"},
//    { "SERIAL", 	SerialNumber,		1,TECHNICIAN,	"Get unit Serial Number "},
//    { "LOG",   	        LogData,		1,TECHNICIAN,	"Get all log data "},
//    { "LOGFULL",   	LogFullData,		1,TECHNICIAN,	"Get all log full data "},
//    { "LOGTIME",   	LogTimeDiff,		1,TECHNICIAN,	"Set time between data logs"},
//    { "LOGERASE",   	LogsEraseAll,		1,TECHNICIAN,	"Erase all log data "},
//    { "HFRET",   	HardFaultRetAddress,    1,CLIENT,	"Load hard fault return address from flash "},

    NULL   // __line
     };

*/

