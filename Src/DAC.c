/*
 * ADC.C
 *
 *  Created on: Jul 5, 2020
 *      Author: avinoam.danieli
 */

#include "main.h"
#include "defs.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "CLI.h"
#include "globals.h"
#include "clicmnds.h"
#include "uart.h"
#include "stm32f4xx_ll_adc.h"
#include "queue.h"
#include "LEDs.h"
#include "ADC.h"
#include <math.h>

/*******************************************************************************
*	DACOP
*
*******************************************************************************/

int16_t DACOP(int32_t value)
{

	  /*##-3- Set DAC Channel1 DHR register ######################################*/
	  if(HAL_DAC_SetValue(&hdac, DAC_CHANNEL_1, DAC_ALIGN_12B_R, value) != HAL_OK)
	  {
	    Error_Handler();
	  }

	  /*##-4- Enable DAC Channel1 ################################################*/
	  if(HAL_DAC_Start(&hdac, DAC_CHANNEL_1) != HAL_OK)
	  {
	    Error_Handler();
	  }
	  return TRUE;
}


/*******************************************************************************
*	DACOPx
*
*******************************************************************************/
//uint32_t dacbuff[4096];
//int16_t DACOPx(int32_t value)
//{
//
//	int i;
//	for (i=0;i<4096;i++)
//		dacbuff[i]=0xeeeeeeee;
//
//	  if(HAL_DAC_Start(&hdac, DAC_CHANNEL_1) != HAL_OK)
////	  if(HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1, dacbuff, 40, DAC_ALIGN_12B_R) != HAL_OK)
//	  {
//	    /* Start Error */
//	    Error_Handler();
//	  }
//
//	  /* USER CODE END TIM2_Init 1 */
//	  htim2.Instance = TIM2;
//	  htim2.Init.Prescaler = 47;
//	  htim2.Init.CounterMode = TIM_COUNTERMODE_DOWN;
//	  htim2.Init.Period = 10;
//	  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
//	  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
//	  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
//	  {
//	    Error_Handler();
//	  }
//	  /* Reset the OPM Bit */
//	  (&htim2)->Instance->CR1 &= ~TIM_CR1_OPM;
//
//		__HAL_TIM_SET_COUNTER(&htim2, 0);  // set the counter value
////		__HAL_TIM_CLEAR_FLAG(&htim2, TIM_SR_UIF);
//		HAL_TIM_Base_Start(&htim2);
//
//
//
//	  while(1) vTaskDelay(10);
//	  /*##-3- Set DAC Channel1 DHR register ######################################*/
//	  if(HAL_DAC_SetValue(&hdac, DAC_CHANNEL_1, DAC_ALIGN_12B_R, value) != HAL_OK)
//	  {
//	    /* Setting value Error */
//	    Error_Handler();
//	  }
//
//	  /*##-4- Enable DAC Channel1 ################################################*/
//	  if(HAL_DAC_Start(&hdac, DAC_CHANNEL_1) != HAL_OK)
//	  {
//	    /* Start Error */
//	    Error_Handler();
//	  }
//}

/*******************************************************************************
*	DACSIN
*
*******************************************************************************/
//
//int16_t DACSIN(double value)
//{
//	  /*##-3- Set DAC Channel1 DHR register ######################################*/
//	  if(HAL_DAC_SetValue(&hdac, DAC_CHANNEL_1, DAC_ALIGN_12B_R, 0x0) != HAL_OK)
//	  {
//	    /* Setting value Error */
//	    Error_Handler();
//	  }
//
//	  /*##-4- Enable DAC Channel1 ################################################*/
//	  if(HAL_DAC_Start(&hdac, DAC_CHANNEL_1) != HAL_OK)
//	  {
//	    /* Start Error */
//	    Error_Handler();
//	  }
//	  /* Infinite loop */
//	  uint16_t y;
//	  double x = 0;
//	  double pi = 8 * atan(1.0);
//	  while (1)
//	  {
//		  y = (uint16_t)((sin(x)+1) * 2047.0);
//		  x += value;
//		  if (x >= (pi))
//			  x = 0.0;
//		  /*##-3- Set DAC Channel1 DHR register ######################################*/
//		  if(HAL_DAC_SetValue(&hdac, DAC_CHANNEL_1, DAC_ALIGN_12B_R, y) != HAL_OK)
//		  {
//		    /* Setting value Error */
//		    Error_Handler();
//		  }
//	  }
//}
//
/*******************************************************************************
*	DAC_CLI
*	CLI
*
*******************************************************************************/
//
//int32_t DAC_CLI(CLI_command_T* command)
//{
//	int32_t val;
//	float VSENSE;
//
//	if ( command->ArgType == GETPAR)
//  	{
//		if (AdcChannelConversion(ADC_VBRAKE_CV, ADC_VBRAKE_C, ADC_SAMPLETIME_480CYCLES, 100L, &val) != HAL_OK)
//		{
//		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
//		   return -11;
//		}
//		else
//		{
//			VSENSE = (Vref/4095.0) / RES_RATIO  * val;
//
//			if (gettxbuffer() == FALSE)
//			{
//			   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
//			   return -9;
//			}
//
//			sprintf (txbuff, "%s=%3.3f  \r\n",command->sCommand, VSENSE);
//			txbuff_semaphor = 1;
//			sendtxbuffer();
//			return TRUE;
//		}
//   }
//   return TRUE;
//}
