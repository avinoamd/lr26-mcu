/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "eeprom.h"
#include "CLI.h"
#include <stdio.h>
/** @addtogroup EEPROM_Emulation
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* Virtual address defined by the user: 0xFFFF value is prohibited */
uint16_t VirtAddVarTab[NB_OF_VAR] = {0x5555, 0x6666, 0x7777};
uint16_t VarDataTab[NB_OF_VAR] = {0, 0, 0};
uint16_t VarValue,VarDataTmp = 0;
/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
/* Base address of the Flash sectors */
#define ADDR_FLASH_SECTOR_0     ((uint32_t)0x08000000) /* Base @ of Sector 0, 16 Kbytes */
#define ADDR_FLASH_SECTOR_1     ((uint32_t)0x08004000) /* Base @ of Sector 1, 16 Kbytes */
#define ADDR_FLASH_SECTOR_2     ((uint32_t)0x08008000) /* Base @ of Sector 2, 16 Kbytes */
#define ADDR_FLASH_SECTOR_3     ((uint32_t)0x0800C000) /* Base @ of Sector 3, 16 Kbytes */
#define ADDR_FLASH_SECTOR_4     ((uint32_t)0x08010000) /* Base @ of Sector 4, 64 Kbytes */
#define ADDR_FLASH_SECTOR_5     ((uint32_t)0x08020000) /* Base @ of Sector 5, 128 Kbytes */
#define ADDR_FLASH_SECTOR_6     ((uint32_t)0x08040000) /* Base @ of Sector 6, 128 Kbytes */
#define ADDR_FLASH_SECTOR_7     ((uint32_t)0x08060000) /* Base @ of Sector 7, 128 Kbytes */
#define ADDR_FLASH_SECTOR_8     ((uint32_t)0x08080000) /* Base @ of Sector 8, 128 Kbytes */
#define ADDR_FLASH_SECTOR_9     ((uint32_t)0x080A0000) /* Base @ of Sector 9, 128 Kbytes */
#define ADDR_FLASH_SECTOR_10    ((uint32_t)0x080C0000) /* Base @ of Sector 10, 128 Kbytes */
#define ADDR_FLASH_SECTOR_11    ((uint32_t)0x080E0000) /* Base @ of Sector 11, 128 Kbytes */

#define FLASH_USER_START_ADDR   ADDR_FLASH_SECTOR_2   /* Start @ of user Flash area */
#define FLASH_USER_END_ADDR     ADDR_FLASH_SECTOR_3 - 1

#define DATA_32                 ((uint32_t)0x12345678)

int eeprom_test(void)
{
   HAL_StatusTypeDef EE_Format(void);
	HAL_FLASH_Unlock();
	/* EEPROM Init */
//	EE_Format();
	if( EE_Init() != EE_OK)
	{
		  errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	}
  
  /* --- Store successively many values of the three variables in the EEPROM ---*/
  /* Store 0x1000 values of Variable1 in EEPROM */
	for (VarValue = 1; VarValue <= 0x1000; VarValue++)
	{
    /* Sequence 1 */
       if((EE_WriteVariable(VirtAddVarTab[0],  VarValue)) != HAL_OK)
       {
      	 errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
       }
    if((EE_ReadVariable(VirtAddVarTab[0],  &VarDataTab[0])) != HAL_OK)
    {
   	 errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    }
    if (VarValue != VarDataTab[0])
    {
   	 errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    }
    
    /* Sequence 2 */
    if(EE_WriteVariable(VirtAddVarTab[1], ~VarValue) != HAL_OK)
    {
   	 errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    }  
    if(EE_ReadVariable(VirtAddVarTab[1], &VarDataTab[1]) != HAL_OK)
    {
   	 errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    } 
    if(((uint16_t)~VarValue) != VarDataTab[1])
    {
   	 errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    }

    /* Sequence 3 */
    if(EE_WriteVariable(VirtAddVarTab[2],  VarValue << 1) != HAL_OK)
    {
   	 errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    }
    if(EE_ReadVariable(VirtAddVarTab[2],  &VarDataTab[2]) != HAL_OK)
    {
   	 errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    } 
    if ((VarValue << 1) != VarDataTab[2])
    {
   	 errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    }
  }

  /* Store 0x2000 values of Variable2 in EEPROM */
  for (VarValue = 1; VarValue <= 0x2000; VarValue++)
  {
    if(EE_WriteVariable(VirtAddVarTab[1], VarValue) != HAL_OK)
    {
   	 errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    }      
    if(EE_ReadVariable(VirtAddVarTab[1], &VarDataTab[1]) != HAL_OK)
    {
   	 errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    }    
    if(VarValue != VarDataTab[1])
    {
   	 errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    }
  }

  /* read the last stored variables data*/
  if(EE_ReadVariable(VirtAddVarTab[0], &VarDataTmp) != HAL_OK)
  {
	  errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
  }   
  if (VarDataTmp != VarDataTab[0])
  {
	  errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
  }

  if(EE_ReadVariable(VirtAddVarTab[1], &VarDataTmp) != HAL_OK)
  {
	  errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
  }    
  if (VarDataTmp != VarDataTab[1])
  {
	  errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
  }

  if(EE_ReadVariable(VirtAddVarTab[2], &VarDataTmp) != HAL_OK)
  {
	  errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
  }    
  if (VarDataTmp != VarDataTab[2])
  {
	  errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
  }
  
  /* Store 0x3000 values of Variable3 in EEPROM */
  for (VarValue = 1; VarValue <= 0x3000; VarValue++)
  {
    if(EE_WriteVariable(VirtAddVarTab[2], VarValue) != HAL_OK)
    {
   	 errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    }      
    if(EE_ReadVariable(VirtAddVarTab[2], &VarDataTab[2]) != HAL_OK)
    {
   	 errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    }    
    if(VarValue != VarDataTab[2])
    {
   	 errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    }
  }

  /* read the last stored variables data*/
  if(EE_ReadVariable(VirtAddVarTab[0], &VarDataTmp) != HAL_OK)
  {
	  errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
  }    
  if (VarDataTmp != VarDataTab[0])
  {
	  errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
  }

  if(EE_ReadVariable(VirtAddVarTab[1], &VarDataTmp) != HAL_OK)
  {
	  errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
  }
  if (VarDataTmp != VarDataTab[1])
  {
	  errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
  }

  if(EE_ReadVariable(VirtAddVarTab[2], &VarDataTmp) != HAL_OK)
  {
	  errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
  }  
  if (VarDataTmp != VarDataTab[2])
  {
	  errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
  }

  while (1)
  {
	  printf("EEPROM Verified\n");
	  HAL_Delay(100);
    /* Turn LED1 On */
//    BSP_LED_On(LED1);
  }
}
