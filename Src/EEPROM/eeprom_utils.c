/*
 * eeprom_utils.c
 *
 *  Created on: 8 Oct 2020
 *      Author: avinoam.danieli
 */

/*
MEMORY
{
  CCMRAM    (xrw)    : ORIGIN = 0x10000000,   LENGTH = 64K
  RAM    	(xrw)    : ORIGIN = 0x20000000,   LENGTH = 128K
  ISR    	(rx)     : ORIGIN = 0x08000000,   LENGTH = 16K			// must be here
  FLASH     (rx)     : ORIGIN = 0x08020000,   LENGTH = 512K			// program
  EEPROM1    (rx)    : ORIGIN = 0x08004000,   LENGTH = 16K
  EEPROM2    (rx)    : ORIGIN = 0x08008000,   LENGTH = 16K
  SECTOR1    (rx)    : ORIGIN = 0x0800c000,   LENGTH = 16K
  SECTOR2    (rx)    : ORIGIN = 0x08010000,   LENGTH = 16K
}
*/
/*
 * cli to store a parameter
 * cli to read a parameter
 * after boot load parameters to variables
 * printing to swo parameter
 * printing to uart3 parameter
 *
 * hard error handler
 * soft error handler
 * print to uart3
 * print to swo
 * save on flash
 * disable interrupts
 * PVD - power - brounout
 * BOOT from
 */
#include "main.h"
#include <stdio.h>
#include <string.h>
#include "print.h"
#include "defs.h"
#include "eeprom.h"
#include "print.h"
#include "CLI.h"
#include "CLIcmnds.h"
#include "uart.h"
#include "PLC.h"

char eevcode[9];
const char eevstr[9] = "AVINOAMD";

/*******************************************************************************
*	ee_write_param
*	description:
*		write value to eeprom
*	parameters :
*		addr - address of data
*		data - data to write
*	return value :
*		none
*
*******************************************************************************/

void ee_write_param(uint16_t addr, uint16_t data)
{
	HAL_FLASH_Unlock();
	if ( (EE_WriteVariable(addr, data)) != HAL_OK )
	{
		HAL_FLASH_Lock();
	   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	}
	HAL_FLASH_Lock();
}

/*******************************************************************************
*	check_valid_params
*	description:
*		look for "AVINOAMD" in EEPROM to validate EEPROM
*	parameters :
*		none
*	return value :
*		valid - TRUE, not valid - FALSE
*
*******************************************************************************/

int32_t check_valid_params(void)
{
	uint16_t i;
	uint16_t res;


	for (i = 0;i < 4;i ++)
	{
		res = EE_ReadVariable(i,  (uint16_t *)(eevcode + i + i) );
		if(res == NO_VALID_PAGE)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		}
		else if (res == 1)									// the variable was not found
			return FALSE;
	}
	return !(strncmp(eevcode, eevstr, 7));
}

/*******************************************************************************
*	set_valid_eeprom
*	description:
*		write "AVINOAMD" in EEPROM to validate EEPROM in first 4 variables
*	parameters :
*		none
*	return value :
*		none
*
*******************************************************************************/

void set_valid_eeprom(void)
{
	uint16_t i;

	HAL_FLASH_Unlock();
	for (i = 0;i < 4;i ++)
	{
		if((EE_WriteVariable(i, *(uint16_t *)(eevstr + i + i) )) != HAL_OK)
		{
			HAL_FLASH_Lock();
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		}
	}
	HAL_FLASH_Lock();
}

/*******************************************************************************
*	eewriteall
*	description:
*		write all parameters to EEPROM
*	parameters :
*		update : 0 - write all params, 1 - write updated params
*	return value :
*		none
*
*******************************************************************************/

void eewriteall(int32_t update)
{
	int i;

	if (update == 0)
	{
		HAL_FLASH_Unlock();
		for (i = 0;i < EEPROM_LEN;i ++)
		{
			if((EE_WriteVariable(10 + i,  EEPROM_Array[i])) != HAL_OK)
			{
				errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			}
		}
		HAL_FLASH_Lock();
	}
	if (update == 1)
	{
		uint16_t var;

		HAL_FLASH_Unlock();
		for (i = 0;i < EEPROM_LEN;i ++)
		{
			EE_ReadVariable(10 + i,  &var);
			if (var != EEPROM_Array[i])
			{
				if((EE_WriteVariable(10 + i, EEPROM_Array[i])) != HAL_OK)
				{
					errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
				}
			}
		}
		HAL_FLASH_Lock();
	}
}

/*******************************************************************************
*	load_params
*
*******************************************************************************/

void load_params(int32_t update)
{
	int i;
	uint16_t ret;

	HAL_FLASH_Unlock();
	if( EE_Init() != EE_OK)
	{
	   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	}
	HAL_FLASH_Lock();

	if ( check_valid_params() )
	{											// update variables from EEPROM
		if (update)
		{
		   eewriteall(0);
		}
		else
		{
			for (i = 0;i < EEPROM_LEN;i ++)
			{
				if(( ret = EE_ReadVariable(10 + i,  &EEPROM_Array[i])) != HAL_OK )
				{
					if( ret == NO_VALID_PAGE )
					{
						errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
					}
					else
					{
						HAL_FLASH_Unlock();
						if((EE_WriteVariable(10 + i,  EEPROM_Array[i])) != HAL_OK)
						{
							HAL_FLASH_Lock();
							errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
						}
						HAL_FLASH_Lock();
					}
				}
			}
		}
	}
	else
	{											// init eeprom and store defaults
		set_valid_eeprom();
	   eewriteall(0);
	}
}

/*******************************************************************************
*	params_update
*	CLI
*
*******************************************************************************/

int32_t params_update(CLI_command_T* command)
{

   if ( (command->ArgType == SETPAR) || (command->ArgType == GETPAR) )
   {
   	eewriteall(1);

		if (gettxbuffer() == FALSE)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		   return -3;
		}
    	snprintf (txbuff, TXMSG_MAX_LEN, "%s\r\n", command->sCommand);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   }
   return 1;
}


