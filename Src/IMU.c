/*
 * IMU.c
 *
 *  Created on: Sep 7, 2020
 *      Author: avinoam.danieli
 */

#include "main.h"
#include "cmsis_os.h"
#include <stdio.h>
#include <string.h>
#include <stdint.h>

uint8_t i2c_scan(I2C_HandleTypeDef *hi2c)
{
	HAL_StatusTypeDef ret;
	uint8_t data[10] = {0,0};
	uint8_t j;

	for (j = 20;j < 240;j +=2)
	{
		ret = HAL_I2C_Mem_Read(hi2c, j, 0x0f, 1, data, 1, 200);
		if (ret == HAL_OK)
		{
			printf("%x  RET = %d %x \n", data[0], ret, j);
			return j;
		}
	}
	return -1;
}

