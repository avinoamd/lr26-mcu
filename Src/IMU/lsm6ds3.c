/******************************************************************************
 * @file    read_data_simple.c
 * @author  Sensors Software Solution Team
 * @brief   This file show the simplest way to get data from sensor.
 *
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */

/* This example was developed using the following STMicroelectronics
 * evaluation boards:
 *
 * - STEVAL_MKI109V3 + STEVAL-MKI160V1
 * - NUCLEO_F411RE + STEVAL-MKI160V1
 *
 * and STM32CubeMX tool with STM32CubeF4 MCU Package
 *
 * Used interfaces:
 *
 * STEVAL_MKI109V3    - Host side:   USB (Virtual COM)
 *                    - Sensor side: SPI(Default) / I2C(supported)
 *
 * NUCLEO_STM32F411RE - Host side: UART(COM) to USB bridge
 *                    - I2C(Default) / SPI(supported)
 *
 * If you need to run this example on a different hardware platform a
 * modification of the functions: `platform_write`, `platform_read`,
 * `tx_com` and 'platform_init' is required.
 *
 */

/* STMicroelectronics evaluation boards definition
 *
 * Please uncomment ONLY the evaluation boards in use.
 * If a different hardware is used please comment all
 * following target board and redefine yours.
 */
#include "main.h"

/* Includes ------------------------------------------------------------------*/
#include <string.h>
#include <stdio.h>
//#include "stm32f4xx_hal.h"
#include "defs.h"
#include "lsm6ds3_reg.h"
#include "BMS.h"

typedef union{
  int16_t i16bit[3];
  uint8_t u8bit[6];
} axis3bit16_t;

typedef union{
  int16_t i16bit;
  uint8_t u8bit[2];
} axis1bit16_t;

/* Private macro -------------------------------------------------------------*/
#define SENSOR_BUS hi2c2

/* Private variables ---------------------------------------------------------*/
static uint8_t whoamI, rst;

/* Extern variables ----------------------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/*   WARNING:
 *   Functions declare in this section are defined at the end of this file
 *   and are strictly related to the hardware platform used.
 *
 */
int32_t platform_write(void *handle, uint8_t reg, uint8_t *bufp,
                              uint16_t len);
int32_t platform_read(void *handle, uint8_t reg, uint8_t *bufp,
                             uint16_t len);

/* Main Example --------------------------------------------------------------*/
int32_t IMU_init(void)
{
	TickType_t currentTick;

  /* Initialize mems driver interface */
  stmdev_ctx_t dev_ctx;
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &SENSOR_BUS;

  /* Check device ID */
  lsm6ds3_device_id_get(&dev_ctx, &whoamI);
  if (whoamI != LSM6DS3_ID)
  {
     /* manage here device not found */
	  return (FALSE);
  }

  /* Restore default configuration */
  currentTick = xTaskGetTickCount();
  lsm6ds3_reset_set(&dev_ctx, PROPERTY_ENABLE);
  do {
	 lsm6ds3_reset_get(&dev_ctx, &rst);
  } while ( rst && ( ( xTaskGetTickCount() - currentTick ) < 2000 ));
  if ( ( xTaskGetTickCount() - currentTick ) > 2000 )
  {
	  return(FALSE);
  }

  /*  Enable Block Data Update */
  lsm6ds3_block_data_update_set(&dev_ctx, PROPERTY_ENABLE);

  /* Set full scale */
  lsm6ds3_xl_full_scale_set(&dev_ctx, LSM6DS3_2g);
  lsm6ds3_gy_full_scale_set(&dev_ctx, LSM6DS3_2000dps);

  /* Set Output Data Rate for Acc and Gyro */
  lsm6ds3_xl_data_rate_set(&dev_ctx, LSM6DS3_XL_ODR_12Hz5);
  lsm6ds3_gy_data_rate_set(&dev_ctx, LSM6DS3_GY_ODR_12Hz5);

  return TRUE;
}

uint8_t IMU_Data[16];			// 14
int32_t IMU_Getdata()
{
  //	int32_t status = TRUE;
  /* Initialize mems driver interface */
	  stmdev_ctx_t dev_ctx;
	  dev_ctx.write_reg = platform_write;
	  dev_ctx.read_reg = platform_read;
	  dev_ctx.handle = &SENSOR_BUS;

	  memset(IMU_Data, 0, 14);
  /* Read samples in polling mode (no int) */
    uint8_t reg;

    /* Read output only if new value is available */
    lsm6ds3_xl_flag_data_ready_get(&dev_ctx, &reg);
    if (reg)
    {
      /* Read acceleration field data */
      lsm6ds3_acceleration_raw_get(&dev_ctx, IMU_Data);
    }

    lsm6ds3_gy_flag_data_ready_get(&dev_ctx, &reg);
    if (reg)
    {
      /* Read angular rate field data */
      lsm6ds3_angular_rate_raw_get(&dev_ctx, IMU_Data + 6);
    }

    lsm6ds3_temp_flag_data_ready_get(&dev_ctx, &reg);
    if (reg)
    {
      /* Read temperature data */
      lsm6ds3_temperature_raw_get(&dev_ctx, IMU_Data + 12);
    }
    return TRUE;
}

/* @brief  Write generic device register (platform dependent)
 *
 * @param  handle    customizable argument. In this examples is used in
 *                   order to select the correct sensor bus handler.
 * @param  reg       register to write
 * @param  bufp      pointer to data to write in register reg
 * @param  len       number of consecutive register to write
 *
 */
int32_t platform_write(void *handle, uint8_t reg, uint8_t *bufp,
                              uint16_t len)
{
	HAL_StatusTypeDef ret;

  if (handle == &hi2c2)
  {
	  ret = HAL_I2C_Mem_Write(handle, LSM6DS3_I2C_ADD_L, reg,
                      I2C_MEMADD_SIZE_8BIT, bufp, len, 1000);
	  if (ret != HAL_OK)
	  {
		  i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		  return (-1);
	  }
  }
#ifdef PRINTDEBUG
  printf("IMU Write ret = %d\n", ret);
#endif
  return 0;
}

/* @brief  Read generic device register (platform dependent)
 *
 * @param  handle    customizable argument. In this examples is used in
 *                   order to select the correct sensor bus handler.
 * @param  reg       register to read
 * @param  bufp      pointer to buffer that store the data read
 * @param  len       number of consecutive register to read
 *
 */
int32_t platform_read(void *handle, uint8_t reg, uint8_t *bufp,
                             uint16_t len)
{
	HAL_StatusTypeDef ret;

  if (handle == &hi2c2)
  {
	  ret = HAL_I2C_Mem_Read(handle, LSM6DS3_I2C_ADD_L, reg,
                     I2C_MEMADD_SIZE_8BIT, bufp, len, 1000);
	  if (ret != HAL_OK)
	  {
		  i2cerrorhandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		  return (-1);
	  }
  }
#ifdef PRINTDEBUG
  printf("IMU Read ret = %d\n", ret);
#endif
  return 0;
}

/* @brief  Write generic device register (platform dependent)
 *
 * @param  tx_buffer     buffer to trasmit
 * @param  len           number of byte to send
 *
 */
