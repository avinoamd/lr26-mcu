/******************************************************************************
 * @file    lsm6ds3_CLI.c
 *
 ******************************************************************************
 */

#define SENSOR_BUS hi2c2

/* Includes ------------------------------------------------------------------*/
#include <string.h>
#include <stdio.h>
//#include "stm32f4xx_hal.h"
#include "lsm6ds3_reg.h"
#include "main.h"
#include "CLI.h"
#include "CLIcmnds.h"
#include "defs.h"
#include "uart.h"

typedef union{
  int16_t i16bit[3];
  uint8_t u8bit[6];
} axis3bit16_t;

typedef union{
  int16_t i16bit;
  uint8_t u8bit[2];
} axis1bit16_t;

/* Private macro -------------------------------------------------------------*/
#define    BOOT_TIME   20 //ms

/* Private variables ---------------------------------------------------------*/
static axis3bit16_t data_raw_acceleration;
static axis3bit16_t data_raw_angular_rate;
static axis1bit16_t data_raw_temperature;
static float acceleration_mg[3];
static float angular_rate_mdps[3];
static float temperature_degC;

/* Extern variables ----------------------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/*   WARNING:
 *   Functions declare in this section are defined at the end of this file
 *   and are strictly related to the hardware platform used.
 *
 */
int32_t platform_write(void *handle, uint8_t reg, uint8_t *bufp,
                              uint16_t len);
int32_t platform_read(void *handle, uint8_t reg, uint8_t *bufp,
                             uint16_t len);
int32_t IMU_init(void);

/*******************************************************************************
*	imu
*	CLI
*
*******************************************************************************/

int32_t IMU_CLI_getdata(CLI_command_T* command)
{
  /* Initialize mems driver interface */
  stmdev_ctx_t dev_ctx;
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &SENSOR_BUS;

  uint8_t reg;


	if ( command->ArgType == GETPAR )
 	{
		//if ( IMU_init() == TRUE )
		{
			if (gettxbuffer() == FALSE)
			{
			   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			   return -9;
			}
			snprintf (txbuff, TXMSG_MAX_LEN, "%s    \r\n",command->sCommand);
			txbuff_semaphor = 1;
			sendtxbuffer();

    /* Read output only if new value is available */
			lsm6ds3_xl_flag_data_ready_get(&dev_ctx, &reg);
			if (reg)
			{
				/* Read acceleration field data */
				lsm6ds3_acceleration_raw_get(&dev_ctx, data_raw_acceleration.u8bit);
				acceleration_mg[0] =
				  lsm6ds3_from_fs2g_to_mg(data_raw_acceleration.i16bit[0]);
				acceleration_mg[1] =
				  lsm6ds3_from_fs2g_to_mg(data_raw_acceleration.i16bit[1]);
				acceleration_mg[2] =
				  lsm6ds3_from_fs2g_to_mg(data_raw_acceleration.i16bit[2]);

				if (gettxbuffer() == FALSE)
				{
					errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
					return -9;
				}
				snprintf((char*)txbuff, TXMSG_MAX_LEN, "Acceleration [mg]:%4.2f\t%4.2f\t%4.2f\r\n",
						  acceleration_mg[0], acceleration_mg[1], acceleration_mg[2]);
				txbuff_semaphor = 1;
				sendtxbuffer();
			}
			vTaskDelay(10);
			lsm6ds3_gy_flag_data_ready_get(&dev_ctx, &reg);
			vTaskDelay(10);
			if (reg)
			{
				/* Read angular rate field data */
				lsm6ds3_angular_rate_raw_get(&dev_ctx, data_raw_angular_rate.u8bit);
				angular_rate_mdps[0] =
				  lsm6ds3_from_fs2000dps_to_mdps(data_raw_angular_rate.i16bit[0]);
				angular_rate_mdps[1] =
				  lsm6ds3_from_fs2000dps_to_mdps(data_raw_angular_rate.i16bit[1]);
				angular_rate_mdps[2] =
				  lsm6ds3_from_fs2000dps_to_mdps(data_raw_angular_rate.i16bit[2]);

				if (gettxbuffer() == FALSE)
				{
					errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
					return -9;
				}
				snprintf((char*)txbuff, TXMSG_MAX_LEN, "Angular rate [mdps]:%4.2f\t%4.2f\t%4.2f\r\n",
						  angular_rate_mdps[0],
						  angular_rate_mdps[1],
						  angular_rate_mdps[2]);
				txbuff_semaphor = 1;
				sendtxbuffer();
			 }

			 lsm6ds3_temp_flag_data_ready_get(&dev_ctx, &reg);
			 if (reg)
			 {
				/* Read temperature data */
				lsm6ds3_temperature_raw_get(&dev_ctx, data_raw_temperature.u8bit);

				temperature_degC =
				  lsm6ds3_from_lsb_to_celsius(data_raw_temperature.i16bit);

				if (gettxbuffer() == FALSE)
				{
					errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
					return -9;
				}
				snprintf((char*)txbuff, TXMSG_MAX_LEN,
						  "Temperature  [degC]:%6.2f\r\n",
						  temperature_degC);
				txbuff_semaphor = 1;
				sendtxbuffer();
			 }
		}
//		else
	//		return FALSE;
 	}
	return TRUE;
}
