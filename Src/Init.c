/*
 * Init.c
 *
 *  Created on: Sep 8, 2020
 *      Author: avinoam.danieli
 */

#include "main.h"
#include "defs.h"
#include "cmsis_os.h"
#include <stdio.h>
#include <string.h>
#include "CLI.h"
#include "lsm6ds3.h"
#include "safety.h"
#include "BCR.h"
#include "flash.h"

/*
 * timer2  uSec Delay for PLC
 */
void timers_init()
{
	HAL_StatusTypeDef retval;

	retval = HAL_TIM_PWM_Start(&htim10, TIM_CHANNEL_1);		// led blue
	osDelay (10);
	retval |= HAL_TIM_PWM_Start(&htim11, TIM_CHANNEL_1);		// led red
	osDelay (10);
	retval |= HAL_TIM_PWM_Start(&htim13, TIM_CHANNEL_1);		// led green
	osDelay (10);
	retval |= HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);		// led G2
	osDelay (10);
	retval |= HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_2);		// led button
	osDelay (10);
	HAL_TIM_Base_Start(&htim14);										// used for 16 bit void uSecDelay(uint16_t us)
	osDelay (10);
	//__disable_irq();

	printf("timers_init = %d\n", retval);

}

void device_init()
{
	int32_t val;

	timers_init();
//	flash_init(0);
	safety_init();
	IMU_init();
	BCR_en();
	val = BCR_init();
	if (val == TRUE)
		printf("BCR_Init OK\n");
	else
		printf("BCR_Init Fail\n");
	BCR_dis();
}
