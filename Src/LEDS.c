/*
 * LEDS.c
 *
 *  Created on: May 22, 2020
 *      Author: avinoam.danieli
 */

#include "main.h"
#include <math.h>
#include "defs.h"
#include "cli.h"
#include "globals.h"
#include "queue.h"
#include "LEDs.h"
#include <stdio.h>
#include "clicmnds.h"
#include "uart.h"

typeLEDs LEDsArr[6] = {{255, 0, 0, 5, 0}, {255, 0, 0, 5, 0}, {255, 0, 0, 5, 0}, {255, 99, 15, 0, 0}, {255, 0, 0, 5, 0}, {0, 0, 0, 0, 0}};

int32_t A[3] = {1,2,3};

/*******************************************************************************
*	setPower
*
*******************************************************************************/

void setPower(typeLEDs LED)
{

   sgConfigOC.Pulse = LED.power;
   switch (LED.num)
   {
   case 1:									// red
      if (HAL_TIM_PWM_ConfigChannel(&htim11, &sgConfigOC, TIM_CHANNEL_1) != HAL_OK)
      {
         Error_Handler();
      }
      break;
   case 2:									// green
      if (HAL_TIM_PWM_ConfigChannel(&htim13, &sgConfigOC, TIM_CHANNEL_1) != HAL_OK)
      {
         Error_Handler();
      }
      break;
   case 3:									// blue
      if (HAL_TIM_PWM_ConfigChannel(&htim10, &sgConfigOC, TIM_CHANNEL_1) != HAL_OK)
      {
         Error_Handler();
      }
      break;
   case 4:									// pushbutton
      if (HAL_TIM_PWM_ConfigChannel(&htim4, &sgConfigOC, TIM_CHANNEL_2) != HAL_OK)
      {
         Error_Handler();
      }
      break;
   case 6:									// g2
      if (HAL_TIM_PWM_ConfigChannel(&htim1, &sgConfigOC, TIM_CHANNEL_1) != HAL_OK)
      {
         Error_Handler();
      }
      break;
   default:
   	  break;
	}
}
const uint8_t TAB[] = {10, 20, 30, 40, 50, 60, 70, 80, 90, 99, 90, 80, 70, 60, 50, 40, 30, 20, 10};

void ledwave(typeLEDs LED)
{
	static uint8_t val = 0;

	LED.num = 4;
	LED.power = TAB[val];
	val = (val + 1) % 19;
	setPower(LED);
	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_2);
}

/*******************************************************************************
*	LEDs
*	Task
*	tim11	CH1		Red
*	tim13	CH1		Green
*	tim10	CH1		Blue
*	tim4	CH2		Pushbutton
*******************************************************************************/

void LEDs(void *argument)
{

	//HAL_StatusTypeDef HAL_TIM_PWM_Init(TIM_HandleTypeDef *htim);
	typeLEDs LED1 = {0, 0, 0, 99, 44};

	osDelay (1000);
   for(;;)
   {
      if (pdPASS == xQueueReceive( LEDqHandle, &LED1, 100/*portMAX_DELAY*/ ))
      {
      	//printf("NUMBER IS : %d\n", uxQueueMessagesWaiting(LEDqHandle));
#ifdef PRINT
         printf("\n%ld  %ld  %ld  %ld  %ld \n", LED1.num, LED1.power, LED1.on, LED1.off, LED1.cnt);
#endif
         if ( (LED1.num > 0) && (LED1.num < 7) )
         {
         	LEDsArr[LED1.num - 1] = LED1;
         	setPower(LED1);
            syncleds();
         }
      }

		if (LEDsArr[0].cnt < LEDsArr[0].on)											// red
		{
			  HAL_TIM_PWM_Start(&htim11, TIM_CHANNEL_1);
		}
		else
		{
			  HAL_TIM_PWM_Stop(&htim11, TIM_CHANNEL_1);
		}
		if ( (LEDsArr[0].on + LEDsArr[0].off) > 0 )
			LEDsArr[0].cnt = (LEDsArr[0].cnt + 1) % (LEDsArr[0].on + LEDsArr[0].off);

		if (LEDsArr[1].cnt < LEDsArr[1].on)											// green
		{
			  HAL_TIM_PWM_Start(&htim13, TIM_CHANNEL_1);
		}
		else
		{
			  HAL_TIM_PWM_Stop(&htim13, TIM_CHANNEL_1);
		}
		if ( (LEDsArr[1].on + LEDsArr[1].off) > 0 )
			LEDsArr[1].cnt = (LEDsArr[1].cnt + 1) % (LEDsArr[1].on + LEDsArr[1].off);

		if (LEDsArr[2].cnt < LEDsArr[2].on)											// blue
		{
			  HAL_TIM_PWM_Start(&htim10, TIM_CHANNEL_1);
		}
		else
		{
			  HAL_TIM_PWM_Stop(&htim10, TIM_CHANNEL_1);
		}
		if ( (LEDsArr[2].on + LEDsArr[2].off) > 0 )
			LEDsArr[2].cnt = (LEDsArr[2].cnt + 1) % (LEDsArr[2].on + LEDsArr[2].off);

		if (LEDsArr[5].cnt < LEDsArr[5].on)											// g2
		{
			  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
		}
		else
		{
			  HAL_TIM_PWM_Stop(&htim1, TIM_CHANNEL_1);
		}
		if ( (LEDsArr[5].on + LEDsArr[5].off) > 0 )
			LEDsArr[5].cnt = (LEDsArr[5].cnt + 1) % (LEDsArr[5].on + LEDsArr[5].off);

		if (LEDsArr[4].power == 0)
		{
			if (LEDsArr[3].cnt < LEDsArr[3].on)											// button
			{
				  HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_2);
			}
			else
			{
				  HAL_TIM_PWM_Stop(&htim4, TIM_CHANNEL_2);
			}
			if ( (LEDsArr[3].on + LEDsArr[3].off) > 0 )
			LEDsArr[3].cnt = (LEDsArr[3].cnt + 1) % (LEDsArr[3].on + LEDsArr[3].off);
		}
		else
			ledwave(LEDsArr[4]);
//		HAL_GPIO_TogglePin(LED4_GPIO_Port, LED4_Pin);
//		vTaskDelay(100);
	}
}

void syncleds(void)
{
	int32_t i;

	for (i = 0;i < 4;i ++)
		LEDsArr[i].cnt = 0;
}

void LEDblink0(void *argument)
{
	static double d = 0.0;

	for(;;)
	{
		HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);
		HAL_GPIO_TogglePin(LED0_GPIO_Port, LED0_Pin);
		d= log(tan(d));
		d += 0.001;
		vTaskDelay(1000);
	}
}
void LEDblink1(void *argument)
{
	for(;;)
	{
		HAL_GPIO_TogglePin(LED2_GPIO_Port, LED2_Pin);
//		HAL_GPIO_TogglePin(LED4_GPIO_Port, LED4_Pin);
		vTaskDelay(500);
	}
}

/*******************************************************************************
*	LEDcmd
*	LED number
*	LED Power
*	LED on
*	LED off
*******************************************************************************/
int32_t LEDcmd(CLI_command_T* command)
{
	typeLEDs LED1 = {64, 15, 5, 0};

	int32_t res;

  	if (command->ArgType == GETPAR)
  	{
		if (gettxbuffer() == FALSE)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		   return -3;
		}
   		sprintf (txbuff, "%s\r\n",command->sCommand);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
  	}
  	else if ( command->ArgType == SETPAR)
  	{
		res = sscanf(command->sParameter, "%ld %ld %ld %ld ", &LED1.num, &LED1.power, &LED1.on, &LED1.off);
		if (res >= 4)
		{
			xQueueSend( LEDqHandle, &LED1, 2000 );
			if (gettxbuffer() == FALSE)
			{
			   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			   return -3;
			}
			sprintf (txbuff, "%s=%ld %ld %ld %ld \r\n", command->sCommand, LED1.num, LED1.power, LED1.on, LED1.off);
		}
		else
		{
			if (gettxbuffer() == FALSE)
			{
			   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			   return -3;
			}
			sprintf (txbuff, "%s \r\n", command->sCommand);
		}
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   	}

    return 1;
}

