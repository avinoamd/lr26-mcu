/*
 * PLC.c
 *
 *  Created on: 19 May 2020
 *      Author: avinoam.danieli
 */
#include "main.h"
#include "cmsis_os.h"
#include <stdio.h>
#include <string.h>
#include "defs.h"

#include "port.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "mbcrc.h"
#include "uart.h"
#include "CLI.h"
#include "CLIcmnds.h"
#include "PLC.h"
#include "print.h"
#include "timers.h"
#include "boardinit.h"

extern TIM_HandleTypeDef htim2;
uint8_t PLCinbuff[PLCINLEN];						// for PLC
uint8_t PLCoutbuff[PLCOUTLEN];					// for PLC
int32_t rxflg;

/*******************************************************************************
*	uS_Delay
*
*******************************************************************************/

void uS_Delay(uint32_t us)
{

	if (us == 0)
		return;
	__HAL_TIM_SET_COUNTER(&htim2, us);  // set the counter value
	__HAL_TIM_CLEAR_FLAG(&htim2, TIM_SR_UIF);
	HAL_TIM_Base_Start(&htim2);
	while (! __HAL_TIM_GET_FLAG(&htim2, TIM_SR_UIF));

	//while (__HAL_TIM_GET_COUNTER(&htim2) != 0xffffffff);
	HAL_TIM_Base_Stop(&htim2);
}

/*******************************************************************************
*	PLC
*	Task
*
*******************************************************************************/
void UART_EndRxTransfer(UART_HandleTypeDef *huart);
void PLCcmnds(uint8_t *buff);
int PLCcnt, time1, time2, time3, time4;

void PLC(void)
{

//
   //HAL_UART_StateTypeDef state;
   int len = 0xffff;
   uint16_t llen, laddr;
   //BaseType_t ret;
   static int lasttime = 0;

   xSemaphoreTake( BSem01Handle, 10 );
   vTaskDelay(1000);
   HAL_UART_AbortReceive_IT(&huart4);
   HAL_UART_Receive(&huart4, (uint8_t *)PLCinbuff, PLCINLEN, PLCINLEN);
   for(;;)
   {

	   //HAL_UART_Receive(&huart4, (uint8_t *)PLCinbuff, 8, 30000);
	   //state = HAL_UART_GetState(&huart4);
	   //while ( (state & HAL_UART_STATE_BUSY_TX) ==  HAL_UART_STATE_BUSY_TX)
		//   state = HAL_UART_GetState(&huart4);
   	len = 0xffff;
	   HAL_UART_AbortReceive_IT(&huart4);
	   //printf("Passed\n");
	   //HAL_UART_Receive(&huart4, (uint8_t *)PLCinbuff, 70, 70);
	   PLCinbuff[0] = 0;
	   rxflg = 1;
	   if(HAL_UART_Receive_IT(&huart4, (uint8_t *)PLCinbuff, 70) != HAL_OK)
	   {
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	   }
	   xSemaphoreTake( BSem01Handle, portMAX_DELAY );
      if ( (huart4.ErrorCode & HAL_UART_ERROR_ORE) != RESET )
      {
			HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);
      	continue;
      }
      else
      {
			time1 = DWT->CYCCNT;
			while (len > huart4.RxXferCount)
			{
				len = huart4.RxXferCount;
				uS_Delay(500);
			}
			time2 = DWT->CYCCNT;
			if (PLCTimeout > 0)
	   	/*BaseType_t*/ xTimerChangePeriod( PLC_TIMERHandle, PLCTimeout, 0 );

			laddr = PLCinbuff[3] + (PLCinbuff[2] << 8);
			llen = PLCinbuff[5] + (PLCinbuff[4] << 8);
			HAL_GPIO_WritePin(LED0_GPIO_Port, LED0_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET);
			HAL_GPIO_TogglePin(LED2_GPIO_Port, LED2_Pin);

			PLCcnt++;

			snprintf(Msguart3, MSGUART3LEN, "%d  %d\t%d   \t%d\t%d   %ld\n",PLCcnt, laddr, llen, 70-len, (time2-time1)/96, xTaskGetTickCount() - lasttime);
			if (PLCPrintToUart3 == TRUE)
				HAL_UART_Transmit_DMA(&huart3, (uint8_t *)Msguart3, strlen(Msguart3));
			if (PLCPrintToSWO == TRUE)
				printf(Msguart3);
			lasttime = xTaskGetTickCount();
			//sprintf(txbuff,  "commbuf = %d %d %d %d %d \r\n", PLCinbuff[1], PLCinbuff[2], PLCinbuff[3], PLCinbuff[4], PLCinbuff[5]);

			//HAL_UART_Transmit(&huart3, (uint8_t *)txbuff, strlen(txbuff), 100);
			//HAL_UART_Transmit_DMA(&huart1, (uint8_t*)PLCinbuff, 8);
			PLCcmnds(PLCinbuff);
      }
   }
}
int uartcnt;

int flg = 0;
void PLC_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	flg = 1;
}

/*******************************************************************************
*	plc_timeout
*	CLI
*
*******************************************************************************/

int32_t plc_timeout(CLI_command_T* command)
{
   int32_t num, stam;

   if (command->ArgType == GETPAR)
   {
	  if (gettxbuffer() == FALSE)
      {
	     errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
         return -3;
      }
	  snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n",command->sCommand, PLCTimeout);
	  txbuff_semaphor = 1;
	  sendtxbuffer();
	  return TRUE;
   }
   else if ( command->ArgType == SETPAR)
   {
	   num = sscanf(command->sParameter, "%ld", (int32_t *)&stam);
	   if (num == 1)
	   {
	   	if (stam > 65535)
	   		stam = 65535;
	   	PLCTimeout = stam;
			if (PLCTimeout == 0)
			{
				xTimerStop( PLC_TIMERHandle, 0 );
			}
	   }

		if (gettxbuffer() == FALSE)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		   return -3;
		}
    	snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n", command->sCommand, PLCTimeout);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   	}
    return 1;
}

#include "timers.h"
void PLC_Timer(TimerHandle_t xTimer)
{
	if (PLCTimeout > 0)
		BrakeOff();
	printf("PLC_Timer   %ld\n", xTaskGetTickCount());
}
