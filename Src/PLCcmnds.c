/*
 * PLCcmnds.c
 *
 *  Created on: Aug 16, 2020
 *      Author: avinoam.danieli
 */

#include "main.h"
#include "defs.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <tgmath.h>

#include "CLI.h"
#include "LEDs.h"
#include "queue.h"
#include "PLC.h"
#include "mbcrc.h"
#include "BoardInit.h"
#include "Safety.h"
#include "CLI.h"
#include "uart.h"
#include "ADC.h"
#include "BCR.h"
#include "BMS.h"
#include "shift.h"
#include "lsm6ds3.h"
#include "PushButton.h"
#include "Battery.h"

int PLCcntOut;

/*******************************************************************************
*	write_single_reg
*	6
*******************************************************************************/

void write_single_reg(uint8_t *buff)
{
   uint16_t addr;
   typeLEDs LED;
   USHORT val;
   int32_t i;

   val = usMBCRC16( PLCinbuff, 8 );
   if (val == 0)
   {
	  for (i = 0;i < 8;i ++)						// prepare reply
		  PLCoutbuff[i] = buff[i];
	  LED.num = 0xff;
	  addr =  (buff[3] & 0x00ff) + (buff[2] << 8) ;
	  switch (addr)
	  {
		case 256:									// Red
		   LED.num = 1;
		   break;
		case 257:
		   LED.num = 2;
		   break;
		case 258:
		   LED.num = 3;
		   break;
		case 259:									//
		   LED.num = 11;
		   break;
		case 260:									// On Off
		   LED.num = 4;
		   break;
	  }
      if (LED.num < 5)
	  {
         if (buff[5] < 100)
            LED.power = buff[5];					// power
		 else
			LED.power = 99;
         LED.on = buff[4] & 0x0f;					// on
		 LED.off = buff[4] >> 4;					// off
		 xQueueSend( LEDqHandle, &LED, 2000 );
		 //		 if(HAL_UART_Transmit_IT(&huart4, (uint8_t*)PLCoutbuff, 8)!= HAL_OK)			// reply
		 PLCcntOut ++;
		 if(HAL_UART_Transmit_DMA(&huart4, (uint8_t*)PLCoutbuff, 8)!= HAL_OK)			// reply
		 {
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		 }
	   }
      else
      {
    	  PLCoutbuff[4] = 0xff;
    	  PLCoutbuff[5] = 0xff;
    	  val = usMBCRC16( PLCoutbuff, 6 );
		  PLCoutbuff[6] = val;
		  PLCoutbuff[7] = val >> 8;
		  //		 if(HAL_UART_Transmit_IT(&huart4, (uint8_t*)PLCoutbuff, 8)!= HAL_OK)			// reply
		  PLCcntOut ++;
		  if(HAL_UART_Transmit_DMA(&huart4, (uint8_t*)PLCoutbuff, 8)!= HAL_OK)			// reply
		  {
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		  }
      }
   }
}

/*******************************************************************************
*	write_discrete_output
*	5
*******************************************************************************/

void write_discrete_output(uint8_t *buff)
{
   uint16_t addr, data;
   USHORT val;
   int32_t i;
   int32_t stat = FALSE;

   val = usMBCRC16( buff, 8 );

   if (val == 0)
   {
	  for (i = 0;i < 8;i ++)					// prepare reply
		  PLCoutbuff[i] = buff[i];
	  addr =  (buff[3] & 0x00ff) + (buff[2] << 8) ;
	  data =  (buff[5] & 0x00ff) + (buff[4] << 8) ;

	  switch (addr)
	  {
		case 0:										// Brake
			   if (data == 0xFF00)
				   stat = BrakeOn();
			   if (data == 0x0000)
			   {
				   stat = TRUE;
				   BrakeOff();
			   }
		   break;
		case 1:										// Brake
			   if (data == 0xFF00)
			   	HAL_GPIO_WritePin(SOM_nMR_GPIO_Port, SOM_nMR_Pin, GPIO_PIN_RESET);
			   if (data == 0x0000)
			   {
				   stat = TRUE;
			   	HAL_GPIO_WritePin(SOM_nMR_GPIO_Port, SOM_nMR_Pin, GPIO_PIN_SET);
			   }
		   break;
		case 2:										// SAFETY SET
			   if (data == 0xFF00)
				   stat = Safety_SET();
			   break;
		case 3:										// safety rst
			   if (data == 0xFF00)
				   stat = Safety_RST();
			   break;
		case 17:										// BCR
		   if (data == 0xFF00)
		   {
		   	BCR_Clear();
			   stat = BCR_en();
		   }
		   if (data == 0x0000)
		   {
			   stat = BCR_dis();
		   }
		   break;
		case 18:										//
		   if (data == 0xFF00)
			   stat = ChargeOn();
		   if (data == 0x0000)
		   {
			   stat = TRUE;
			   ChargeOff();
		   }
		   break;
		case 19:										//
		   break;
		case 20:										// LPM
		   if (data == 0xFF00)
			   	xTaskNotify( xTaskToNotifyPBTask, LPM, eSetValueWithOverwrite );
		   if (data == 0x0000)
		   {
			   xTaskNotify( xTaskToNotifyPBTask, ON, eSetValueWithOverwrite );
		   }
		   break;
		case 21:										// Shutdown
		   if (data == 0xFF00)
			   xTaskNotify( xTaskToNotifyPBTask, OFF, eSetValueWithOverwrite );
		   break;
		case 38:										//
		   if (data == 0xFF00)
			   stat = DriverOn();
		   if (data == 0x0000)
		   {
			   stat = TRUE;
			   DriverOff();
		   }
		   break;
		case 39:										//
		   if (data == 0xFF00)
			   stat = PullerOn();
		   if (data == 0x0000)
		   {
			   stat = TRUE;
			   PullerOff();
		   }
		   break;
		case 40:										//
		   if (data == 0xFF00)
			   stat = LockerOn();
		   if (data == 0x0000)
		   {
			   stat = TRUE;
			   LockerOff();
		   }
		   break;
		default:
		   break;
	  }
	  if (stat == TRUE)
	  {
		  //		  if(HAL_UART_Transmit_IT(&huart4, (uint8_t*)PLCoutbuff, 8)!= HAL_OK)			// reply
		  PLCcntOut ++;
		  if(HAL_UART_Transmit_DMA(&huart4, (uint8_t*)PLCoutbuff, 8)!= HAL_OK)			// reply
		  {
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		  }
	  }
     else
     {
    	  PLCoutbuff[4] = 0xff;
    	  PLCoutbuff[5] = 0xff;
    	  val = usMBCRC16( PLCoutbuff, 6 );
		  PLCoutbuff[6] = val;
		  PLCoutbuff[7] = val >> 8;
		  //		 if(HAL_UART_Transmit_IT(&huart4, (uint8_t*)PLCoutbuff, 8)!= HAL_OK)			// reply
		  PLCcntOut ++;
		  if(HAL_UART_Transmit_DMA(&huart4, (uint8_t*)PLCoutbuff, 8)!= HAL_OK)			// reply
		  {
			  errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		  }
     }
   }
}

/*******************************************************************************
*	read_discrete_output
*	1
*
*******************************************************************************/

void read_discrete_output(uint8_t *buff)
{
   uint16_t addr, num;
   USHORT val;
   int32_t i;

   uint8_t len;

   val = usMBCRC16( buff, 8 );
   if (val == 0)
   {
	   addr = PLCinbuff[3] + (PLCinbuff[2] << 8);
	   num = PLCinbuff[5] + (PLCinbuff[4] << 8);
	   len = ((num - 1) >> 3) + 1;
   	PLCoutbuff[0] = buff[0];												// device address
		PLCoutbuff[1] = buff[1];												// functional code
	   PLCoutbuff[2] = (uint8_t)len;

	   getcoils();
	   shftreg(addr, num);
	   for (i = 0;i < fmin(len, 8);i ++)
	   {
	   	PLCoutbuff[3 + i] = lo.uc[i];
	   }
	   for (i = 8;i < fmin(len, 16);i ++)
	   {
	   	PLCoutbuff[3 + i] = hi.uc[i - 8];
	   }

	   len += 3;
	   val = usMBCRC16( PLCoutbuff, len );
	   PLCoutbuff[len] = val;
	   PLCoutbuff[len+1] = val >> 8;
	   //HAL_UART_StateTypeDef state;
	   //state = HAL_UART_GetState(&huart4);
	   //	   if(HAL_UART_Transmit_IT(&huart4, (uint8_t*)PLCoutbuff, len+2)!= HAL_OK)			// reply
	   PLCcntOut ++;
	   if(HAL_UART_Transmit_DMA(&huart4, (uint8_t*)PLCoutbuff, len+2)!= HAL_OK)			// reply
	   {
		   Error_Handler();
	   }
//	   vTaskDelay(50);
//	   vTaskDelay(10);
//	   state = HAL_UART_GetState(&huart4);
//	   vTaskDelay(2);
//	   state = HAL_UART_GetState(&huart4);
//	   state = HAL_UART_GetState(&huart4);
//		sprintf(txbuff,  "%d %d %d %d %d \r\n", PLCoutbuff[3], PLCoutbuff[4], PLCoutbuff[5], PLCoutbuff[6], PLCoutbuff[7]);

//		HAL_UART_Transmit(&huart3, (uint8_t *)txbuff, strlen(txbuff),100);
		//vTaskDelay(1);
		//HAL_UART_Transmit_DMA(&huart3, "\r\n", 2);

   }
}

/*******************************************************************************
*	read_analog_intput
*	4
*
*******************************************************************************/

void read_analog_intput(uint8_t *buff)
{
   uint16_t addr, num, curraddr, retval;
   USHORT val;
   int32_t i, len;
   int32_t first = 0;

   val = usMBCRC16( buff, 8 );
   if (val == 0)
   {
	   addr = buff[3] + (buff[2] << 8);
	   num = buff[5] + (buff[4] << 8);
	   num = fmin(num, 32);
	   PLCoutbuff[0] = buff[0];												// device address
	   PLCoutbuff[1] = buff[1];												// Functional code

	   PLCoutbuff[2] = (uint8_t)(num << 1);
	   memset(PLCoutbuff + 3, 0, num);										// clear
	   curraddr = addr;
		for (i = 0;i < num; i++)
		{
			switch (curraddr)
			{
			case 261:																// barcode reader
				BCR_getdata(PLCoutbuff + 3, num);
				i += num;
				break;
			case 281:
				retval = BMS_Data(0x08, &PLCoutbuff[i+i+3], &PLCoutbuff[i+i+4]);			// Temperature
				break;
			case 282:
				retval = BMS_Data(0x09, &PLCoutbuff[i+i+3], &PLCoutbuff[i+i+4]);			// BMS - Voltage
				break;
			case 283:
				retval = BMS_Data(0x0A, &PLCoutbuff[i+i+3], &PLCoutbuff[i+i+4]);			// BMS - Current
				break;
			case 284:
				retval = BMS_Data(0x0B, &PLCoutbuff[i+i+3], &PLCoutbuff[i+i+4]);			// BMS - Average Current
				break;
				break;
			case 285:
				retval = BMS_Data(0x1C, &PLCoutbuff[i+i+3], &PLCoutbuff[i+i+4]);			// BMS - Battery ID - seria
				break;
			case 286:
				retval = BMS_Data(0x0D, &PLCoutbuff[i+i+3], &PLCoutbuff[i+i+4]);			// BMS - Relative State of Charge
				break;
			case 287:
				retval = BMS_Data(0X0E, &PLCoutbuff[i+i+3], &PLCoutbuff[i+i+4]);			// BMS - Absolute State of Charge
				break;
			case 288:
				retval = 10 * BMS_Data(0X0F, &PLCoutbuff[i+i+3], &PLCoutbuff[i+i+4]);	// BMS - Remaning Capacity
				break;
			case 289:
				retval = 10 * BMS_Data(0X10, &PLCoutbuff[i+i+3], &PLCoutbuff[i+i+4]);	// BMS - Full Charge Capacity
				break;
			case 290:
				retval = BMS_Data(0X16, &PLCoutbuff[i+i+3], &PLCoutbuff[i+i+4]);			// BMS - Battery Status
				break;
			case 291:
				retval = 10 * BMS_Data(0X18, &PLCoutbuff[i+i+3], &PLCoutbuff[i+i+4]);	// BMS - Design Capacity
				break;
			case 292:
				retval = BMS_Data(0X17, &PLCoutbuff[i+i+3], &PLCoutbuff[i+i+4]);			// BMS - Cycle Count
				break;
			case 293:
				retval = BMS_Data(0X4F, &PLCoutbuff[i+i+3], &PLCoutbuff[i+i+4]);			// BMS - State of Health
				break;
			case 294:
				retval = BMS_DAStatus2(first++, 0, &PLCoutbuff[i+i+3], &PLCoutbuff[i+i+4]);	//
				break;
			case 295:
				retval = BMS_DAStatus2(first++, 2, &PLCoutbuff[i+i+3], &PLCoutbuff[i+i+4]);	//
				break;
			case 296:
				retval = BMS_DAStatus2(first++, 3, &PLCoutbuff[i+i+3], &PLCoutbuff[i+i+4]);	//
				break;
			case 297:
				retval = BMS_DAStatus2(first++, 4, &PLCoutbuff[i+i+3], &PLCoutbuff[i+i+4]);	//
				break;
			case 298:
				retval = BMS_DAStatus2(first++, 5, &PLCoutbuff[i+i+3], &PLCoutbuff[i+i+4]);	//
				break;
			case 299:
				retval = BMS_DAStatus2(first++, 6, &PLCoutbuff[i+i+3], &PLCoutbuff[i+i+4]);	//
				break;
			case 300:
				retval = BMS_DAStatus2(first++, 7, &PLCoutbuff[i+i+3], &PLCoutbuff[i+i+4]);	//
				break;
			case 305:
				retval = plcvbatt();												// ADC 0
				PLCoutbuff[i+i+3] = retval >> 8;								// hi
				PLCoutbuff[i+i+4] = retval & 0xff;							// low
				break;
			case 306:
				retval = plcvsw();
				PLCoutbuff[i+i+3] = retval >> 8;								// hi
				PLCoutbuff[i+i+4] = retval & 0xff;							// low
				break;
			case 307:
				retval = plcvswcurr();
				PLCoutbuff[i+i+3] = retval >> 8;								// hi
				PLCoutbuff[i+i+4] = retval & 0xff;							// low
				break;
			case 312:
				retval = plcvcharge();
				PLCoutbuff[i+i+3] = retval >> 8;								// hi
				PLCoutbuff[i+i+4] = retval & 0xff;							// low
				break;
			case 313:
				retval = plcp24v();
				PLCoutbuff[i+i+3] = retval >> 8;								// hi
				PLCoutbuff[i+i+4] = retval & 0xff;							// low
				break;
			case 314:
				retval = plcp5v();
				PLCoutbuff[i+i+3] = retval >> 8;								// hi
				PLCoutbuff[i+i+4] = retval & 0xff;							// low
				break;
			case 322:
				retval = plcp3v3();
				PLCoutbuff[i+i+3] = retval >> 8;								// hi
				PLCoutbuff[i+i+4] = retval & 0xff;							// low
				break;
			case 321:
				retval = plcTMPR();
				PLCoutbuff[i+i+3] = retval >> 8;								// hi
				PLCoutbuff[i+i+4] = retval & 0xff;							// low
				break;
			case 325:
				retval = IMU_Getdata();
				PLCoutbuff[i+i+3] = IMU_Data[1];								// hi
				PLCoutbuff[i+i+4] = IMU_Data[0];								// low
				break;
			case 326:
				PLCoutbuff[i+i+3] = IMU_Data[3];								// hi
				PLCoutbuff[i+i+4] = IMU_Data[2];								// low
				break;
			case 327:
				PLCoutbuff[i+i+3] = IMU_Data[5];								// hi
				PLCoutbuff[i+i+4] = IMU_Data[4];								// low
				break;
			case 328:
				PLCoutbuff[i+i+3] = IMU_Data[7];								// hi
				PLCoutbuff[i+i+4] = IMU_Data[6];								// low
				break;
			case 329:
				PLCoutbuff[i+i+3] = IMU_Data[9];								// hi
				PLCoutbuff[i+i+4] = IMU_Data[8];								// low
				break;
			case 330:
				PLCoutbuff[i+i+3] = IMU_Data[11];								// hi
				PLCoutbuff[i+i+4] = IMU_Data[10];								// low
				break;
			case 331:
				PLCoutbuff[i+i+3] = IMU_Data[13];								// hi
				PLCoutbuff[i+i+4] = IMU_Data[12];								// low
				break;
			case 336:
				PLCoutbuff[i+i+3] = (PowerState & 0x0000ffff) >> 8;		// hi
				PLCoutbuff[i+i+4] = PowerState & 0x000000ff;					// low
				break;
			case 337:
			case 338:
			case 339:
			case 340:
				PLCoutbuff[i+i+3] = (BattLevel & 0x0000ffff) >> 8;		// hi
				PLCoutbuff[i+i+4] = BattLevel & 0x000000ff;				// low
				break;
			default:
				PLCoutbuff[i+i+3] = 0x00;
				PLCoutbuff[i+i+4] = 0x00;
				break;
			}
			curraddr ++;
		}
		len = num + num + 3;
		val = usMBCRC16( PLCoutbuff, len );
		PLCoutbuff[len] = val;
		PLCoutbuff[len+1] = val >> 8;
   }
//   vTaskDelay(2);
//   if(HAL_UART_Transmit_IT(&huart4, (uint8_t*)PLCoutbuff, len+2)!= HAL_OK)			// reply
   PLCcntOut ++;
   if(HAL_UART_Transmit_DMA(&huart4, (uint8_t*)PLCoutbuff, len+2)!= HAL_OK)			// reply
   {
		errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
   }
//   retrycnt = 0;
//   int gr;
//   gr = HAL_OK;
////   while(HAL_UART_Transmit_DMA(&huart4, (uint8_t*)PLCoutbuff, len+2)!= HAL_OK)			// reply
//   while(gr!= HAL_OK)			// reply
//   {
//   	gr = HAL_UART_Transmit_DMA(&huart4, (uint8_t*)PLCoutbuff, len+2);
//      retrycnt ++;
//   		//errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
//   }
//   if (retrycnt > 1)
//      	printf("RetryCnt = %d\n", retrycnt);
}

/*******************************************************************************
*	read analog output
*	3
*
*******************************************************************************/

uint8_t SOMoutbuff[9];

void read_analog_output(uint8_t *buff)
{
   //uint16_t addr, data;
   USHORT val;

   val = usMBCRC16( buff, 8 );												// check CRC
   if (val == 0)
   {
//	   addr = buff[3] + (buff[2] << 8);
//	   data =  (buff[8]) ;

	   SOMoutbuff[0] = buff[0];												// device address
	   SOMoutbuff[1] = buff[1];												// Functional code
	   SOMoutbuff[2] = buff[2];												// Address of the first register Hi bytes
	   SOMoutbuff[3] = buff[3];												// Address of the first register Lo bytes
	   SOMoutbuff[4] = 0;														// Value Hi bytes
	   SOMoutbuff[5] = 1;														// Value Lo bytes
//	   addr = buff[3] + (buff[2] << 8);
		val = usMBCRC16( SOMoutbuff, 6 );
		SOMoutbuff[6] = val;														// Checksum CRC
		SOMoutbuff[7] = val >> 8;												// Checksum CRC

 	   if(HAL_UART_Transmit_DMA(&huart1, (uint8_t*)SOMoutbuff, 8)!= HAL_OK)			// reply
 	   {
 	   	errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
 	   }
   }
}

/*******************************************************************************
*	record_multiple_analog_outputs
*	16
*
*******************************************************************************/

void record_multiple_analog_outputs(uint8_t *buff)
{
   uint16_t addr, data;
   USHORT val;

   val = usMBCRC16( buff, 11 );												// check CRC
   if (val == 0)
   {
	   addr = buff[3] + (buff[2] << 8);
	   data =  (buff[8]) ;

	   SOMoutbuff[0] = buff[0];												// device address
	   SOMoutbuff[1] = buff[1];												// Functional code
	   SOMoutbuff[2] = buff[2];												// Address of the first register Hi bytes
	   SOMoutbuff[3] = buff[3];												// Address of the first register Lo bytes
	   SOMoutbuff[4] = buff[4];												// Number of registers Hi bytes
	   SOMoutbuff[5] = buff[5];												// Number of registers Lo bytes
	   addr = buff[3] + (buff[2] << 8);
		switch (addr)
		{
		case 20:										// LPM
		   if (data == 0x00)
			   	xTaskNotify( xTaskToNotifyPBTask, LPM, eSetValueWithOverwrite );
		   if (data == 0x01)
		   {
			   xTaskNotify( xTaskToNotifyPBTask, ON, eSetValueWithOverwrite );
		   }
		   break;
		case 21:										// Shutdown
		   if (data == 0x01)
			   xTaskNotify( xTaskToNotifyPBTask, OFF, eSetValueWithOverwrite );
		   break;
		}
		val = usMBCRC16( SOMoutbuff, 6 );
		SOMoutbuff[6] = val;														// Checksum CRC
		SOMoutbuff[7] = val >> 8;												// Checksum CRC

 	   if(HAL_UART_Transmit_DMA(&huart1, (uint8_t*)SOMoutbuff, 8)!= HAL_OK)			// reply
 	   {
 	   	errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
 	   }
   }
}

/*******************************************************************************
*	PLCcmnds
*
*******************************************************************************/

void PLCcmnds(uint8_t *buff)
{

   switch (buff[1])
   {
   case 1:									// read discrete output
   	read_discrete_output(buff);
      break;
   case 4:									// read analog input
   	read_analog_intput(buff);
      break;
   case 6:									// write discrete output
   	write_single_reg(buff);
      break;
   case 5:									// write discrete output
	   write_discrete_output(buff);
      break;
   case 16:									// record multiple analog outputs output
   	record_multiple_analog_outputs(buff);
      break;
   case 3:									// record multiple analog outputs output
   	read_analog_output(buff);
      break;
	default:
	   break;
   }
}

int64_t coilslo, coilshi;
void load_data()
{
	coilslo = 0;
	coilshi = 0;
	coilshi |= ( 1/*HAL_GPIO_ReadPin(STO_GPIO_Port, STO_Pin)*/ << 0 );    	// 64
	coilshi |= ( 1/*HAL_GPIO_ReadPin(SAFETY_ALERT_GPIO_Port, SAFETY_ALERT_Pin)*/ << 1 );    	// 65
}





void test_02()
{

	uint8_t hi, lo;
	uint8_t hio = 0, loo = 0;

	hi = 0xa5;
	lo = 0xc3;
	// shift down 3
	loo = hi;
	loo = loo << 5;
	hio = hi >> 3;
	loo = loo | (lo >> 3);
	printf("%d %d %d %d\n", hi, lo, hio, loo);




}

void test_01()
{
uint16_t len, val1;
int i;

for (i = 0;i < 100;i ++)
	PLCoutbuff[i] = 61;

PLCinbuff[0] = 7;
PLCinbuff[1] = 1;
PLCinbuff[2] = 0;
PLCinbuff[3] = 64;
PLCinbuff[4] = 0;
PLCinbuff[5] = 8;
len = 6;
val1 = usMBCRC16( PLCinbuff, len );
PLCinbuff[len] = val1;
PLCinbuff[len+1] = val1 >> 8;

read_discrete_output(PLCinbuff);

PLCinbuff[3] = 63;
val1 = usMBCRC16( PLCinbuff, len );
PLCinbuff[len] = val1;
PLCinbuff[len+1] = val1 >> 8;
read_discrete_output(PLCinbuff);

PLCinbuff[3] = 58;
val1 = usMBCRC16( PLCinbuff, len );
PLCinbuff[len] = val1;
PLCinbuff[len+1] = val1 >> 8;
read_discrete_output(PLCinbuff);

PLCinbuff[3] = 57;
val1 = usMBCRC16( PLCinbuff, len );
PLCinbuff[len] = val1;
PLCinbuff[len+1] = val1 >> 8;
read_discrete_output(PLCinbuff);

PLCinbuff[3] = 57;
val1 = usMBCRC16( PLCinbuff, len );
PLCinbuff[len] = val1;
PLCinbuff[len+1] = val1 >> 8;
read_discrete_output(PLCinbuff);

}


