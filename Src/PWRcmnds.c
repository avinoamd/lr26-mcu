#include "main.h"
#include "defs.h"
#include "defs.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "CLI.h"
#include "clicmnds.h"
#include "uart.h"
#include "globals.h"

/*******************************************************************************
*	P5V_EN
*
*******************************************************************************/
int32_t P5V_EN(CLI_command_T* command)
{

	uint16_t stam;
	int32_t var;

  	if (command->ArgType == GETPAR)
  	{

		if (gettxbuffer() == FALSE)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		   return -3;
		}
		var = HAL_GPIO_ReadPin(P5V_EN_GPIO_Port, P5V_EN_Pin);
    	sprintf (txbuff, "%s=%ld\r\n",command->sCommand, var );
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
  	}
  	else if ( command->ArgType == SETPAR)
  	{
		sscanf(command->sParameter, "%hd", &stam);
		if (stam == 0)
		{
			HAL_GPIO_WritePin(P5V_EN_GPIO_Port, P5V_EN_Pin ,GPIO_PIN_RESET);
		}
		else
		{
			HAL_GPIO_WritePin(P5V_EN_GPIO_Port, P5V_EN_Pin ,GPIO_PIN_SET);
		}
		if (gettxbuffer() == FALSE)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		   return -3;
		}
    	sprintf (txbuff, "%s=%d\r\n", command->sCommand, stam);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   	}

    return 1;
}

/*******************************************************************************
*	P24V_EN
*
*******************************************************************************/
int32_t P24V_EN(CLI_command_T* command)
{

	uint16_t stam;
	int32_t var;

  	if (command->ArgType == GETPAR)
  	{

		if (gettxbuffer() == FALSE)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		   return -3;
		}
		var = HAL_GPIO_ReadPin(P24V_EN_GPIO_Port, P24V_EN_Pin);
    	sprintf (txbuff, "%s=%ld\r\n",command->sCommand, var );
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
  	}
  	else if ( command->ArgType == SETPAR)
  	{
		sscanf(command->sParameter, "%hd", &stam);
		if (stam == 0)
		{
			HAL_GPIO_WritePin(P24V_EN_GPIO_Port, P24V_EN_Pin ,GPIO_PIN_RESET);
		}
		else
		{
			HAL_GPIO_WritePin(P24V_EN_GPIO_Port, P24V_EN_Pin ,GPIO_PIN_SET);
		}
		if (gettxbuffer() == FALSE)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		   return -3;
		}
    	sprintf (txbuff, "%s=%d\r\n", command->sCommand, stam);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   	}

    return 1;
}

/*******************************************************************************
*	MCU_HOLD
*
*******************************************************************************/
int32_t MCU_HOLD(CLI_command_T* command)
{

	uint16_t stam;
	int32_t var;

  	if (command->ArgType == GETPAR)
  	{

		if (gettxbuffer() == FALSE)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		   return -3;
		}
		var = HAL_GPIO_ReadPin(MCU_HOLD_GPIO_Port, MCU_HOLD_Pin);
    	sprintf (txbuff, "%s=%ld\r\n",command->sCommand, var );
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
  	}
  	else if ( command->ArgType == SETPAR)
  	{
		sscanf(command->sParameter, "%hd", &stam);
		if (stam == 0)
		{
			HAL_GPIO_WritePin(MCU_HOLD_GPIO_Port, MCU_HOLD_Pin ,GPIO_PIN_RESET);
		}
		else
		{
			HAL_GPIO_WritePin(MCU_HOLD_GPIO_Port, MCU_HOLD_Pin ,GPIO_PIN_SET);
		}
		if (gettxbuffer() == FALSE)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		   return -3;
		}
    	sprintf (txbuff, "%s=%d\r\n", command->sCommand, stam);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   	}

    return 1;
}

/*******************************************************************************
*	PRECHRG_EN
*
*******************************************************************************/
int32_t PRECHRG_EN(CLI_command_T* command)
{

	uint16_t stam;
	int32_t var;

  	if (command->ArgType == GETPAR)
  	{

		if (gettxbuffer() == FALSE)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		   return -3;
		}
		var = HAL_GPIO_ReadPin(PRECHRG_EN_GPIO_Port, PRECHRG_EN_Pin);
    	sprintf (txbuff, "%s=%ld\r\n",command->sCommand, var );
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
  	}
  	else if ( command->ArgType == SETPAR)
  	{
		sscanf(command->sParameter, "%hd", &stam);
		if (stam == 0)
		{
			HAL_GPIO_WritePin(PRECHRG_EN_GPIO_Port, PRECHRG_EN_Pin ,GPIO_PIN_RESET);
		}
		else
		{
			HAL_GPIO_WritePin(PRECHRG_EN_GPIO_Port, PRECHRG_EN_Pin ,GPIO_PIN_SET);
		}
		if (gettxbuffer() == FALSE)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		   return -3;
		}
    	sprintf (txbuff, "%s=%d\r\n", command->sCommand, stam);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   	}

    return 1;
}

/*******************************************************************************
*	VSW_EN
*
*******************************************************************************/
int32_t VSW_EN(CLI_command_T* command)
{

	uint16_t stam;
	int32_t var;

  	if (command->ArgType == GETPAR)
  	{

		if (gettxbuffer() == FALSE)
		{
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			return -3;
		}
		var = HAL_GPIO_ReadPin(VSW_EN_GPIO_Port, VSW_EN_Pin);
    	sprintf (txbuff, "%s=%ld\r\n",command->sCommand, var );
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
  	}
  	else if ( command->ArgType == SETPAR)
  	{
		sscanf(command->sParameter, "%hd", &stam);
		if (stam == 0)
		{
			HAL_GPIO_WritePin(VSW_EN_GPIO_Port, VSW_EN_Pin ,GPIO_PIN_RESET);
		}
		else
		{
			HAL_GPIO_WritePin(VSW_EN_GPIO_Port, VSW_EN_Pin ,GPIO_PIN_SET);
		}
		if (gettxbuffer() == FALSE)
		{
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			return -3;
		}
    	sprintf (txbuff, "%s=%d\r\n", command->sCommand, stam);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   	}

    return 1;
}

/*******************************************************************************
*	CHARGE_DIS
*
*******************************************************************************/
int32_t CHARGE_DIS(CLI_command_T* command)
{

	uint16_t stam;
	int32_t var;

  	if (command->ArgType == GETPAR)
  	{

		if (gettxbuffer() == FALSE)
		{
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			return -3;
		}
		var = HAL_GPIO_ReadPin(CHARGE_DIS_GPIO_Port, CHARGE_DIS_Pin);
    	sprintf (txbuff, "%s=%ld\r\n",command->sCommand, var );
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
  	}
  	else if ( command->ArgType == SETPAR)
  	{
		sscanf(command->sParameter, "%hd", &stam);
		if (stam == 0)
		{
			HAL_GPIO_WritePin(CHARGE_DIS_GPIO_Port, CHARGE_DIS_Pin ,GPIO_PIN_RESET);
		}
		else
		{
			HAL_GPIO_WritePin(CHARGE_DIS_GPIO_Port, CHARGE_DIS_Pin ,GPIO_PIN_SET);
		}
		if (gettxbuffer() == FALSE)
		{
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			return -3;
		}
    	sprintf (txbuff, "%s=%d\r\n", command->sCommand, stam);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   	}

    return 1;
}

/*******************************************************************************
*	DCDC24V_EN
*
*******************************************************************************/
int32_t DCDC24V_EN(CLI_command_T* command)
{

	uint16_t stam;
	int32_t var;

  	if (command->ArgType == GETPAR)
  	{

		if (gettxbuffer() == FALSE)
		{
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			return -3;
		}
		var = HAL_GPIO_ReadPin(DCDC24V_EN_GPIO_Port, DCDC24V_EN_Pin);
    	sprintf (txbuff, "%s=%ld\r\n",command->sCommand, var );
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
  	}
  	else if ( command->ArgType == SETPAR)
  	{
		sscanf(command->sParameter, "%hd", &stam);
		if (stam == 0)
		{
			HAL_GPIO_WritePin(DCDC24V_EN_GPIO_Port, DCDC24V_EN_Pin ,GPIO_PIN_RESET);
		}
		else
		{
			HAL_GPIO_WritePin(DCDC24V_EN_GPIO_Port, DCDC24V_EN_Pin ,GPIO_PIN_SET);
		}
		if (gettxbuffer() == FALSE)
		{
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			return -3;
		}
    	sprintf (txbuff, "%s=%d\r\n", command->sCommand, stam);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   	}

    return 1;
}
//
///*******************************************************************************
//*	DRV_EN
//*
//*******************************************************************************/
//int32_t DRV_EN(CLI_command_T* command)
//{
//
//	uint16_t stam;
//	int32_t var;
//
//  	if (command->ArgType == GETPAR)
//  	{
//
//		if (gettxbuffer() == FALSE)
//		{
//			errorHandler(-3, __LINE__, __PRETTY_FUNCTION__);
//			return -3;
//		}
//		var = HAL_GPIO_ReadPin(DRV_EN_GPIO_Port, User_Button_Pin);
//    	sprintf (txbuff, "%s=%ld\r\n",command->sCommand, var );
//		txbuff_semaphor = 1;
//		sendtxbuffer();
//		return TRUE;
//  	}
//  	else if ( command->ArgType == SETPAR)
//  	{
//		sscanf(command->sParameter, "%hd", &stam);
//		if (stam == 0)
//		{
//			HAL_GPIO_WritePin(DRV_EN_GPIO_Port, DRV_EN_Pin ,GPIO_PIN_RESET);
//		}
//		else
//		{
//			HAL_GPIO_WritePin(DRV_EN_GPIO_Port, DRV_EN_Pin ,GPIO_PIN_SET);
//		}
//		if (gettxbuffer() == FALSE)
//		{
//			errorHandler(-3, __LINE__, __PRETTY_FUNCTION__);
//			return -3;
//		}
//    	sprintf (txbuff, "%s=%d\r\n", command->sCommand, stam);
//		txbuff_semaphor = 1;
//		sendtxbuffer();
//		return TRUE;
//   	}
//
//    return 1;
//}
//
///*******************************************************************************
//*	SAFTY_SET
//*
//*******************************************************************************/
//int32_t SAFTY_SET(CLI_command_T* command)
//{
//
//	uint16_t stam;
//	int32_t var;
//
//  	if (command->ArgType == GETPAR)
//  	{
//
//		if (gettxbuffer() == FALSE)
//		{
//			errorHandler(-3, __LINE__, __PRETTY_FUNCTION__);
//			return -3;
//		}
//		var = HAL_GPIO_ReadPin(User_Button_GPIO_Port, User_Button_Pin);
//    	sprintf (txbuff, "%s=%ld\r\n",command->sCommand, var );
//		txbuff_semaphor = 1;
//		sendtxbuffer();
//		return TRUE;
//  	}
//  	else if ( command->ArgType == SETPAR)
//  	{
//		sscanf(command->sParameter, "%hd", &stam);
//		if (stam == 0)
//		{
//			HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin ,GPIO_PIN_RESET);
//		}
//		else
//		{
//			HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin ,GPIO_PIN_SET);
//		}
//		if (gettxbuffer() == FALSE)
//		{
//			errorHandler(-3, __LINE__, __PRETTY_FUNCTION__);
//			return -3;
//		}
//    	sprintf (txbuff, "%s=%d\r\n", command->sCommand, stam);
//		txbuff_semaphor = 1;
//		sendtxbuffer();
//		return TRUE;
//   	}
//
//    return 1;
//}
//
///*******************************************************************************
//*	SAFETY_RST
//*
//*******************************************************************************/
//int32_t SAFETY_RST(CLI_command_T* command)
//{
//
//	uint16_t stam;
//	int32_t var;
//
//  	if (command->ArgType == GETPAR)
//  	{
//
//		if (gettxbuffer() == FALSE)
//		{
//			errorHandler(-3, __LINE__, __PRETTY_FUNCTION__);
//			return -3;
//		}
//		var = HAL_GPIO_ReadPin(User_Button_GPIO_Port, User_Button_Pin);
//    	sprintf (txbuff, "%s=%ld\r\n",command->sCommand, var );
//		txbuff_semaphor = 1;
//		sendtxbuffer();
//		return TRUE;
//  	}
//  	else if ( command->ArgType == SETPAR)
//  	{
//		sscanf(command->sParameter, "%hd", &stam);
//		if (stam == 0)
//		{
//			HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin ,GPIO_PIN_RESET);
//		}
//		else
//		{
//			HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin ,GPIO_PIN_SET);
//		}
//		if (gettxbuffer() == FALSE)
//		{
//			errorHandler(-3, __LINE__, __PRETTY_FUNCTION__);
//			return -3;
//		}
//    	sprintf (txbuff, "%s=%d\r\n", command->sCommand, stam);
//		txbuff_semaphor = 1;
//		sendtxbuffer();
//		return TRUE;
//   	}
//
//    return 1;
//}
//
///*******************************************************************************
//*	LED1
//*
//*******************************************************************************/
//int32_t LED1(CLI_command_T* command)
//{
//
//	uint16_t stam;
//	int32_t var;
//
//  	if (command->ArgType == GETPAR)
//  	{
//
//		if (gettxbuffer() == FALSE)
//		{
//			errorHandler(-3, __LINE__, __PRETTY_FUNCTION__);
//			return -3;
//		}
//		var = HAL_GPIO_ReadPin(User_Button_GPIO_Port, User_Button_Pin);
//    	sprintf (txbuff, "%s=%ld\r\n",command->sCommand, var );
//		txbuff_semaphor = 1;
//		sendtxbuffer();
//		return TRUE;
//  	}
//  	else if ( command->ArgType == SETPAR)
//  	{
//		sscanf(command->sParameter, "%hd", &stam);
//		if (stam == 0)
//		{
//			HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin ,GPIO_PIN_RESET);
//		}
//		else
//		{
//			HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin ,GPIO_PIN_SET);
//		}
//		if (gettxbuffer() == FALSE)
//		{
//			errorHandler(-3, __LINE__, __PRETTY_FUNCTION__);
//			return -3;
//		}
//    	sprintf (txbuff, "%s=%d\r\n", command->sCommand, stam);
//		txbuff_semaphor = 1;
//		sendtxbuffer();
//		return TRUE;
//   	}
//
//    return 1;
//}
//
//
//
//
