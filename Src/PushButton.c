/*
 * PushButton.c
 *
 *  Created on: Sep 5, 2020
 *      Author: avinoam.danieli
 */

#include <stdio.h>
#include <string.h>
#include "main.h"
#include "cmsis_os.h"
#include "cli.h"
#include "LEDs.h"
#include "queue.h"
#include "Boardinit.h"
#include "PushButton.h"
#include "uart.h"

typedef enum
{
  OPEN = 0,
  S1,
  SHORT,
  LONG,
} PB_State;;

#define REP 30

TaskHandle_t xTaskToNotifyPBtoMCU;
TaskHandle_t xTaskToNotifyPBTask;
TaskHandle_t xTaskToNotifyKEYtoMCU;

/*******************************************************************************
*	StartPB
*	Task
*
*******************************************************************************/

void StartPB(void)
{
	static PB_State state = OPEN;
	uint32_t retval;
	int32_t cnt;
	//GPIO_PinState pinstate;
	TickType_t xTimeNow;
	typeLEDs LEDwave = {5, 255, 0, 0, 0};
//	typeLEDs LEDnorm = {5, 0, 0, 0, 0};
//	typeLEDs LEDon= {4, 99, 15, 0, 0};
//	typeLEDs LEDoff= {4, 99, 0, 15, 0};

	xTaskToNotifyPBtoMCU = xTaskGetCurrentTaskHandle();										// for ISR
	vTaskDelay(2500);
	for (;;)
	{
		//res = xTaskNotifyWait( 0, 0, &retval, portMAX_DELAY);
		//printf("%ld RES = %d \n", retval, res);
		//vTaskDelay(5000);
		//GPIO_Pin = ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
		//val = HAL_GPIO_ReadPin(PBtoMCU_GPIO_Port, PBtoMCU_Pin);

		switch (state)
		{
		case OPEN :
			xTimeNow = xTaskGetTickCount();
			snprintf(txbuff, TXMSG_MAX_LEN, "OPEN\t%ld\n", xTimeNow);								// led on
			HAL_UART_Transmit_DMA(&huart3, (uint8_t *)txbuff, strlen(txbuff));
			xTaskNotifyWait( 0, 0, &retval, portMAX_DELAY);												// wait for int
			if ( retval == GPIO_PIN_SET )																		// PB pressed
			{
				vTaskDelay(100);
				xTaskNotifyWait( 0, 0, &retval, 0);															// clear
				if ( HAL_GPIO_ReadPin(PBtoMCU_GPIO_Port, PBtoMCU_Pin) == GPIO_PIN_SET )
				{
					state = S1;
				}
			}
			break;
		case S1 :
			xTimeNow = xTaskGetTickCount();
			snprintf(txbuff, TXMSG_MAX_LEN, "S1\t%ld\n", xTimeNow);									// led on
			HAL_UART_Transmit_DMA(&huart3, (uint8_t *)txbuff, strlen(txbuff));
			//printf("S1\t %ld\n", xTimeNow);																// led on
			cnt = 0;
			while ( (cnt < 2) )
			{
				if ( (pdFALSE == xTaskNotifyWait(0, 0, &retval, 100)) )
				{
					cnt ++;
				}
				else
				{
					if (retval == GPIO_PIN_RESET)
					{
						vTaskDelay(100);
						xTaskNotifyWait( 0, 0, &retval, 0);													// clear
						if ( HAL_GPIO_ReadPin(PBtoMCU_GPIO_Port, PBtoMCU_Pin) == GPIO_PIN_RESET )
						{
							break;
						}
					}
					else
						continue;
				}
			}
			if (cnt < 2)																						// t < 2
			{
				state = OPEN;
			}
			else																									// t >= 2
			{
				state = SHORT;
			}
			break;
		case SHORT :
			xTimeNow = xTaskGetTickCount();
			snprintf(txbuff, TXMSG_MAX_LEN, "SHORT\t%ld\n", xTimeNow);							// led blink
			HAL_UART_Transmit_DMA(&huart3, (uint8_t *)txbuff, strlen(txbuff));
			xQueueSend( LEDqHandle, &LEDwave, 2000 );
			cnt = 0;
			while ( (cnt < REP) )
			{
				if ( (pdFALSE == xTaskNotifyWait(0, 0, &retval, 100)) )
				{
					cnt ++;
				}
				else
				{
					if (retval == GPIO_PIN_RESET)
					{
						vTaskDelay(100);
						xTaskNotifyWait( 0, 0, &retval, 0);														// clear
						if ( HAL_GPIO_ReadPin(PBtoMCU_GPIO_Port, PBtoMCU_Pin) == GPIO_PIN_RESET )
						{
							break;
						}
					}
					else
						continue;
				}
			}
			if (cnt < REP)																								// t < 2
			{
				xTimeNow = xTaskGetTickCount();
				snprintf(txbuff, TXMSG_MAX_LEN, "SHORT cnt smaller than REP\t %ld\n", xTimeNow);				// led blink
				HAL_UART_Transmit_DMA(&huart3, (uint8_t *)txbuff, strlen(txbuff));
				vTaskDelay(10);
		   	xTaskNotify( xTaskToNotifyPBTask, SHORTPRESS, eSetValueWithOverwrite );
				state = OPEN;
			}
			else																											// t >= 2
			{
				state = LONG;
			}
			break;
		case LONG :
			xTimeNow = xTaskGetTickCount();
			snprintf(txbuff, TXMSG_MAX_LEN, "LONG\t %ld\n", xTimeNow);									// led blink
			HAL_UART_Transmit_DMA(&huart3, (uint8_t *)txbuff, strlen(txbuff));
			xTaskNotifyWait( 0, 0, &retval, portMAX_DELAY);													// wait for int
	   	xTaskNotify( xTaskToNotifyPBTask, LONGPRESS, eSetValueWithOverwrite );
			xTimeNow = xTaskGetTickCount();
			printf("LONG2\t %ld\n", xTimeNow);															// led blink
			break;
		default:
			state = OPEN;
			break;
		}		// case
	}			// for
}

/*******************************************************************************
*	HAL_GPIO_EXTI_Callback
*	ISR
*
* GPIO_PIN_15		PBtoMCU_Pin
* GPIO_PIN_14		KEY_MCU_Pin
* GPIO_PIN_12		SAFETYALERT_Pin
*
*******************************************************************************/
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{

	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	if (GPIO_Pin == PBtoMCU_Pin)
	{
		if( xTaskToNotifyPBtoMCU != NULL )
		{
	//		vTaskNotifyGiveFromISR( xTaskToNotifyPBtoMCU, &xHigherPriorityTaskWoken );
			xTaskNotifyFromISR( xTaskToNotifyPBtoMCU, HAL_GPIO_ReadPin(PBtoMCU_GPIO_Port, PBtoMCU_Pin), eSetValueWithOverwrite, &xHigherPriorityTaskWoken );
			portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
		}
	}
	if (GPIO_Pin == KEY_MCU_Pin)
	{
		if( xTaskToNotifyKEYtoMCU != NULL )
		{
	//		vTaskNotifyGiveFromISR( xTaskToNotifyPBtoMCU, &xHigherPriorityTaskWoken );
			xTaskNotifyFromISR( xTaskToNotifyKEYtoMCU, HAL_GPIO_ReadPin(KEY_MCU_GPIO_Port, KEY_MCU_Pin), eSetValueWithOverwrite, &xHigherPriorityTaskWoken );
			portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
		}
	}
  /* Prevent unused argument(s) compilation warning */
  //UNUSED(GPIO_Pin);
  //printf("PB Event %d PIN = %x CNT = %d \n", s++, GPIO_Pin, cnt - 1);
}

/*******************************************************************************
*	KEYTask
*	Task
*	this task wait for Key close, turns power and brake on
*
*******************************************************************************/

void KEYTask(void)
{
	uint32_t retval;

	xTaskToNotifyKEYtoMCU = xTaskGetCurrentTaskHandle();										// for ISR

	for (;;)
	{
		xTaskNotifyWait( 0, 0, &retval, portMAX_DELAY);											// from ISR
		if (retval == GPIO_PIN_SET)
		{
			OnMode();
			BrakeOn();
		}
	}
}

/*******************************************************************************
*	PMTask
*	Task
*	this task wait for  form pushbutton or CLI or ModBus to perform power mode change
*
*******************************************************************************/

void PMTask(void)
{
	uint32_t pstate;
	static uint32_t Cpstate = ON;
	typeLEDs LEDwave = {5, 255, 0, 0, 0};
	typeLEDs LEDnorm = {5, 0, 0, 0, 0};
	typeLEDs LEDon= {4, 99, 15, 0, 0};
	typeLEDs LEDoff= {4, 99, 0, 15, 0};

	xTaskToNotifyPBTask = xTaskGetCurrentTaskHandle();										// for other tasks

	for (;;)
	{
		xTaskNotifyWait( 0, 0, &pstate, portMAX_DELAY );
		printf("%ld\n", pstate);
		switch (pstate)
		{
		case ON :
			if (Cpstate != ON)
			{
		   	OnMode();
				xQueueSend( LEDqHandle, &LEDnorm, 2000 );
				xQueueSend( LEDqHandle, &LEDon, 2000 );
				Cpstate = ON;
			}
			break;
		case LPM :
			if (Cpstate != LPM)
			{
				xQueueSend( LEDqHandle, &LEDwave, 2000 );
				HAL_Delay(1000);
				LowPowerMode();
				Cpstate = LPM;
			}
			break;
		case OFF :
			xQueueSend( LEDqHandle, &LEDnorm, 2000 );
			xQueueSend( LEDqHandle, &LEDoff, 2000 );
			ShutDown();
			Cpstate = OFF;
			break;
		case SHORTPRESS :
			if (Cpstate == LPM)
			{
		   	OnMode();
				xQueueSend( LEDqHandle, &LEDnorm, 2000 );
				xQueueSend( LEDqHandle, &LEDon, 2000 );
				Cpstate = ON;
			}
			else if (Cpstate == ON)
			{
				xQueueSend( LEDqHandle, &LEDwave, 2000 );
				HAL_Delay(4000);
				LowPowerMode();
				Cpstate = LPM;
			}
			break;
		case LONGPRESS :
			xQueueSend( LEDqHandle, &LEDnorm, 2000 );
			xQueueSend( LEDqHandle, &LEDoff, 2000 );
			ShutDown();
			Cpstate = OFF;
			break;
		}

	}
}

//tskTaskControlBlock
//#include "task.h"
TaskHandle_t xTaskToNotifySOF;
int32_t SOFcnt;

void vApplicationStackOverflowHook( TaskHandle_t *pxTask, signed char *pcTaskName )
{

	SOFcnt =  uxTaskGetStackHighWaterMark( NULL );
	xTaskToNotifySOF = xTaskGetCurrentTaskHandle();
	printf("%s\t", pcTaskGetName(xTaskToNotifySOF));
	printf("SOV %ld\n", SOFcnt);
}

/*
 *
 * 	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	if (huart == &huart5)
	{
		if( xTaskToNotifyU5Rx != NULL )
		{
			vTaskNotifyGiveFromISR( xTaskToNotifyU5Rx, &xHigherPriorityTaskWoken );
			portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
		}
	}
 *
 */

/*******************************************************************************
*	StartPB
*	Task
*
*******************************************************************************/
/*
void StartPB(void)
{
	static PB_State state = ON;
	uint32_t retval;
	int32_t cnt;
	//GPIO_PinState pinstate;
	TickType_t xTimeNow;
	typeLEDs LEDwave = {5, 255, 0, 0, 0};
	typeLEDs LEDnorm = {5, 0, 0, 0, 0};
	typeLEDs LEDon= {4, 99, 15, 0, 0};
	typeLEDs LEDoff= {4, 99, 0, 15, 0};

	xTaskToNotifyPBtoMCU = xTaskGetCurrentTaskHandle();										// for ISR

	for (;;)
	{
		//res = xTaskNotifyWait( 0, 0, &retval, portMAX_DELAY);
		//printf("%ld RES = %d \n", retval, res);
		//vTaskDelay(5000);
		//GPIO_Pin = ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
		//val = HAL_GPIO_ReadPin(PBtoMCU_GPIO_Port, PBtoMCU_Pin);

		xTimeNow = xTaskGetTickCount();
		switch (state)
		{
		case ON1 :
			OnMode();
			state = ON;
			break;
		case ON :
			xQueueSend( LEDqHandle, &LEDnorm, 2000 );
			xQueueSend( LEDqHandle, &LEDon, 2000 );
			xTaskNotifyWait( 0, 0, &retval, portMAX_DELAY);											// wait for int
			if ( retval == GPIO_PIN_SET )																	// PB pressed
			{
				vTaskDelay(100);
				xTaskNotifyWait( 0, 0, &retval, 0);														// clear
				if ( HAL_GPIO_ReadPin(PBtoMCU_GPIO_Port, PBtoMCU_Pin) == GPIO_PIN_SET )
				{
					state = W1;
				}
			}
			break;
		case W1 :
			printf("W1\t %ld\n", xTimeNow);																// led on
			cnt = 0;
			while ( (cnt < REP) )
			{
				if ( (pdFALSE == xTaskNotifyWait(0, 0, &retval, 100)) )
				{
					cnt ++;
				}
				else
				{
					if (retval == GPIO_PIN_RESET)
					{
						vTaskDelay(100);
						xTaskNotifyWait( 0, 0, &retval, 0);												// clear
						if ( HAL_GPIO_ReadPin(PBtoMCU_GPIO_Port, PBtoMCU_Pin) == GPIO_PIN_RESET )
						{
							break;
						}
					}
					else
						continue;
				}
			}
			if (cnt < REP)																						// t < 2
			{
				state = ON;
			}
			else																									// t >= 2
			{
				state = W2;
			}
			break;
		case W2 :
			printf("W2\t %ld\n", xTimeNow);															// led blink
			xQueueSend( LEDqHandle, &LEDwave, 2000 );
			cnt = 0;
			while ( (cnt < REP) )
			{
				if ( (pdFALSE == xTaskNotifyWait(0, 0, &retval, 100)) )
				{
					cnt ++;
				}
				else
				{
					if (retval == GPIO_PIN_RESET)
					{
						vTaskDelay(100);
						xTaskNotifyWait( 0, 0, &retval, 0);														// clear
						if ( HAL_GPIO_ReadPin(PBtoMCU_GPIO_Port, PBtoMCU_Pin) == GPIO_PIN_RESET )
						{
							break;
						}
					}
					else
						continue;
				}
			}
			if (cnt < REP)																					// t < 2
			{
				state = LPM;
			}
			else																								// t >= 2
			{
				state = OFF;
			}
			break;
		case LPM1 :
			LowPowerMode();
			state = LPM;
			break;

		case LPM :
			printf("LPM\t %ld  \n", xTimeNow);
			xTaskNotifyWait( 0, 0, &retval, portMAX_DELAY);										// wait for int from PB
			if ( retval == GPIO_PIN_SET )																// PB pressed
			{
				vTaskDelay(100);
				xTaskNotifyWait( 0, 0, &retval, 0);														// clear
				if ( HAL_GPIO_ReadPin(PBtoMCU_GPIO_Port, PBtoMCU_Pin) == GPIO_PIN_SET )
				{
					state = W3;
				}
			}
			break;
		case W3 :
			cnt = 0;
			printf("W3\t %ld\n", xTimeNow);														// led blink
			cnt = 0;
			while ( (cnt < REP) )
			{
				if ( (pdFALSE == xTaskNotifyWait(0, 0, &retval, 100)) )
				{
					cnt ++;
				}
				else
				{
					if (retval == GPIO_PIN_RESET)
					{
						vTaskDelay(100);
						xTaskNotifyWait( 0, 0, &retval, 0);														// clear
						if ( HAL_GPIO_ReadPin(PBtoMCU_GPIO_Port, PBtoMCU_Pin) == GPIO_PIN_RESET )
						{
							break;
						}
					}
					else
						continue;
				}
			}
			if (cnt < REP)																					// t < 2
			{
				state = LPM;
			}
			else																								// t >= 2
			{
				state = W4;
			}
			break;
		case W4 :
			cnt = 0;
			printf("W4\t %ld\n", xTimeNow);															// led on
			xQueueSend( LEDqHandle, &LEDnorm, 2000 );
			xQueueSend( LEDqHandle, &LEDon, 2000 );
			while ( (cnt < REP) )
			{
				if ( (pdFALSE == xTaskNotifyWait(0, 0, &retval, 100)) )
				{
					cnt ++;
				}
				else
				{
					if (retval == GPIO_PIN_RESET)
					{
						vTaskDelay(100);
						xTaskNotifyWait( 0, 0, &retval, 0);														// clear
						if ( HAL_GPIO_ReadPin(PBtoMCU_GPIO_Port, PBtoMCU_Pin) == GPIO_PIN_RESET )
						{
							break;
						}
					}
					else
						continue;
				}
			}
			if (cnt < REP)																					// t < 2
			{
				state = ON;
			}
			else																								// t >= 2
			{
				state = OFF;
			}
			break;
		case OFF :
			xQueueSend( LEDqHandle, &LEDnorm, 2000 );
			xQueueSend( LEDqHandle, &LEDoff, 2000 );
			printf("OFF\t %ld \n", xTimeNow);														// led on
			vTaskDelay(1000);
			ShutDown();
			xTaskNotifyWait( 0, 0, &retval, 0);														// clear
			break;
		default:
			state = ON;
			break;
		}		// case
	}			// for
}

*/
