/*
 * SOM_Comm.c
 *
 *  Created on: Aug 11, 2020
 *      Author: avinoam.danieli
 */

#include "main.h"
#include "FreeRTOS.h"
#include "timers.h"
#include "globals.h"
#include "CLI.h"
#include "ADC.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "PLC.h"
#include "BCR.h"
#include "SOM_MODBUS.h"

//uint8_t *start = BcrRxBuff;
//uint8_t *end = BcrRxBuff;

#define SWVER "\eZ1\r"			// software version
#define ASCIIP "\eZA\r"			// ascii characters
#define START "\eZ\r"
#define STOP "\eY\r"

char SOMbuff[100];
int32_t adcbuff[10];
int16_t stat[16];

void SOM_Comm1(void)
{
	printf("h");
}
void SOM_Comm(void)
//__RAM_FUNC void SOM_Comm(void)
//void SOM_Comm(void)
{
//	int32_t i = 0, val, min= 0xffff, max = 0, mean = 0;
	int32_t i = 0;
//	HAL_UART_Transmit(&huart3, __PRETTY_FUNCTION__, strlen(__PRETTY_FUNCTION__), 1000);
	osDelay (2000);
//	printf(Sector1);
//	printf(Sector2);
//	HAL_UART_Transmit(&huart3, __PRETTY_FUNCTION__, strlen(__PRETTY_FUNCTION__), 1000);
//	SOM_Comm1();
//	printf("%d",Sector1[0]);
//	printf("%d",Sector2[0]);
//	printf("%d",Sector3[0]);
//	printf("%d",Sector4[0]);
/*
	uint8_t buff[] = {"Booting... \n\r"};

	  for (;;)
	  {
	  	  retval = HAL_UART_Transmit(&huart4, buff, 13, 1000);
	  	  HAL_Delay(1000);
	  }
*/
#define PBtoMCU_Pin GPIO_PIN_15
#define PBtoMCU_GPIO_Port GPIOG
#define MCU_HOLD_Pin GPIO_PIN_13
#define MCU_HOLD_GPIO_Port GPIOG

	uint8_t msb, lsb;
	int32_t len;
	float f;

	uint16_t BMS_Data(uint8_t addr, uint8_t *MSB, uint8_t *LSB);
	BMS_Data(8, &msb, &lsb);
	len = (msb << 8) + lsb;
	f = len;
	printf("%g\n", f/10.0 -273.1);
	HAL_Delay(100);

	BMS_Data(9, &msb, &lsb);
	len = (msb << 8) + lsb;
	f = len;
	printf("%g\n", f/1000.0);
	HAL_Delay(100);

	//BMS();
/*	BCR_en();
	int x;
	while (1)
	{
		x = BCR_getdata(tmpbuff);
		vTaskDelay(2000);
		printf("%s     %d\n", tmpbuff, x);
	}
	//BCR_dis();
*/
	SOM_MODBUS();													// for (;;)

/* test procedure for uSecDelay */
/*
	unsigned int val1, val2, dwt1, dwt2;
	HAL_Delay(1000);
	for (;;)
	{
		val1 = HAL_GetTick();
		dwt1 = DWT->CYCCNT;
		uSecDelay((uint16_t)59000);
		val2 = HAL_GetTick();
		dwt2 = DWT->CYCCNT;
		printf("%d   %d\n", (dwt2 - dwt1)/96, val2-val1);
	}
*/

	/* test procedure for SOM communication */

//	HAL_UART_Transmit(&huart3, __PRETTY_FUNCTION__, strlen(__PRETTY_FUNCTION__), 1000);
	for (;;)
	{
		HAL_StatusTypeDef retval;
		for (i = 0;i < 100;i ++)
			SOMbuff[i] = 0;
	   while ( (retval = HAL_UART_Receive(&huart1, (uint8_t *)SOMbuff, 1, 2)) );
	   retval = HAL_UART_Receive(&huart1, (uint8_t *)SOMbuff+1, 10, 10);
	   {
//		   HAL_UART_Transmit(&huart1, (uint8_t *)SOMbuff, 11, 20);
		   HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);
			vTaskDelay(40);
			HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET);
	   }
/*	   if (retval == HAL_OK)
	   {
		   retval = HAL_UART_Transmit(&huart1, (uint8_t *)SOMbuff, 1, 10);
		   HAL_GPIO_WritePin(LED0_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);
			vTaskDelay(40);
			HAL_GPIO_WritePin(LED0_GPIO_Port, LED1_Pin, GPIO_PIN_SET);
	   }
*/		vTaskDelay(8);
#include <math.h>
		double d;
		int i;

		for (i = 0;i < 1;i ++)
		{
			d = i & 0xff;
			d = sin(d);
		}

		/* Generate a text table from the run-time stats. This must fit into the
		cStringBuffer array. */
		//vTaskGetRunTimeStats( cStringBuffer );
		/* Print out column headings for the run-time stats table. */
		//printf( "\nTask\t\t\tAbs\t\t\t%%\n" );
		//printf( "-------------------------------------------------------------\n" );
		/* Print out the run-time stats themselves. The table of data contains
		multiple lines, so the vPrintMultipleLines() function is called instead of
		calling printf() directly. vPrintMultipleLines() simply calls printf() on
		each line individually, to ensure the line buffering works as expected. */
//		vPrintMultipleLines( cStringBuffer );
		//printf( cStringBuffer );
		//vTaskList( cStringBuffer );
		//printf( "-------------------------------------------------------------\n" );
		//vTaskDelay(1000);
		//printf( cStringBuffer );
	}
}

/*
osStatus_t osDelayUntil (uint32_t ticks) {
*/
