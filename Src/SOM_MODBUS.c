/*
 * SOM_MODBUS.c
 *
 *  Created on: 19 May 2020
 *      Author: avinoam.danieli
 */
/*
 * 	SOM Modbus communication via usart1 115200,N,8,1
 * 	ISR returns semaphor to detect first charactr in message
 *
 */
#include "main.h"
#include "cmsis_os.h"
#include <stdio.h>
#include <string.h>
#include "defs.h"

#include "port.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "mbcrc.h"
#include "uart.h"
#include "PLC.h"
#include "SOM_MODBUS.h"
#include "print.h"

extern UART_HandleTypeDef huart3;
extern TIM_HandleTypeDef htim2;
uint8_t SOMinbuff[SOMINLEN];						// for SOM
//uint8_t PLCoutbuff[PLCOUTLEN];					// for PLC
int32_t SOMrxflg;

/*******************************************************************************
*	uSec_Delay
*	Must call HAL_TIM_Base_Start(&htim14);
*	TIM_CR1_CEN               TIM_CR1_CEN_Msk                 !<Counter enable
*	before using uSec_Delay
*	Resolution 1 uSec
*	Range 0 - 65635 uSec
*******************************************************************************/

void uSecDelay(uint16_t us)
{
	uint16_t target;

	if (us == 0)
		return;
	target = us + __HAL_TIM_GET_COUNTER(&htim14);  	// set the counter value
	if (target < us)											// overflow
	{
	   __HAL_TIM_CLEAR_FLAG(&htim14, TIM_FLAG_UPDATE);
		while ( !__HAL_TIM_GET_FLAG(&htim14, TIM_FLAG_UPDATE) );		// wait to overflow
	}
	while ( __HAL_TIM_GET_COUNTER(&htim14) < target );					// wait
}

/*******************************************************************************
*	SOM_MODBUS
*	Called by SOM_Comm Task
*
*******************************************************************************/

void PLCcmnds(uint8_t *buff);
int SOMcnt;
static int time1, time2;

void SOM_MODBUS(void)
{

//
   int len = 0xffff;
   uint16_t llen, laddr;
   static int lasttime = 0;

   xSemaphoreTake( BSemUART1_RX_ISRHandle, 10 );
   vTaskDelay(100);
   HAL_UART_AbortReceive_IT(&huart1);						// clear buffer
   HAL_UART_Receive(&huart1, (uint8_t *)SOMinbuff, SOMINLEN, SOMINLEN);
//   xSemaphoreTake( BSemUART1_RX_ISRHandle, 10 );
   for(;;)
   {

   	len = 0xffff;
	   HAL_UART_AbortReceive_IT(&huart1);													// clear
	   SOMinbuff[0] = 0;																			// clear
	   SOMrxflg = 1;																				// first byte flag, reset by ISR
	   if(HAL_UART_Receive_IT(&huart1, (uint8_t *)SOMinbuff, 70) != HAL_OK)
	   {
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	   }
	   xSemaphoreTake( BSemUART1_RX_ISRHandle, portMAX_DELAY );

	   time1 = DWT->CYCCNT;
	   while (len > huart1.RxXferCount)
	   {
		   len = huart1.RxXferCount;
		   uSecDelay(500);
	   }
	   HAL_Delay(6);
	   time2 = DWT->CYCCNT;
	   laddr = SOMinbuff[3] + (SOMinbuff[2] << 8);
	   llen = SOMinbuff[5] + (SOMinbuff[4] << 8);
	   SOMcnt++;

   	snprintf(Msguart3, MSGUART3LEN, "%d  %d\t%d   \t%d\tXfercnt = %d  %d  %ld\n", SOMcnt, laddr, llen, 70-len, huart1.RxXferCount, (time2-time1)/96, xTaskGetTickCount() - lasttime);
   	if (SOMPrintToUart3 == TRUE)
   		HAL_UART_Transmit_DMA(&huart3, (uint8_t *)Msguart3, strlen(Msguart3));
   	if (SOMPrintToSWO == TRUE)
   		printf(Msguart3);
   	lasttime = xTaskGetTickCount();
		//sprintf(txbuff,  "commbuf = %d %d %d %d %d \r\n", PLCinbuff[1], PLCinbuff[2], PLCinbuff[3], PLCinbuff[4], PLCinbuff[5]);

		//HAL_UART_Transmit(&huart3, (uint8_t *)txbuff, strlen(txbuff), 100);
   	PLCcmnds(SOMinbuff);
   }
}
int uartcnt;

/*
int flg = 0;
void PLC_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	flg = 1;
}
*/
