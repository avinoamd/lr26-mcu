/*
 * Safety.c
 *
 *  Created on: 13 Aug 2020
 *      Author: avinoam.danieli
 */

#include "main.h"
#include "FreeRTOS.h"
#include "timers.h"
#include "globals.h"
#include "defs.h"
#include "CLI.h"
#include "ADC.h"
#include <stdio.h>
#include <string.h>
#include "CLIcmnds.h"
#include "BoardInit.h"
#include "uart.h"

void safety_init()
{
//	/*BaseType_t*/ xTimerStart( SAFETY_SETHandle, 100 );
//	/*BaseType_t*/ xTimerStart( SAFETY_RSTHandle, 100 );
}

/*******************************************************************************
*	SafetySET
*	Timer
*
*******************************************************************************/

void SafetySET(void *argument)
{
	HAL_GPIO_WritePin(SAFETY_SET_GPIO_Port, SAFETY_SET_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET);
}

/*******************************************************************************
*	SafetyRST
*	Timer
*
*******************************************************************************/

void SafetyRST(void *argument)
{
	HAL_GPIO_WritePin(SAFETY_RST_GPIO_Port, SAFETY_RST_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LED0_GPIO_Port, LED0_Pin, GPIO_PIN_SET);
}

/*******************************************************************************
*	Safety_SET
*
*
*******************************************************************************/

int32_t Safety_SET(void)
{
	if (PowerState | P24V)
	{
		HAL_GPIO_WritePin(SAFETY_SET_GPIO_Port, SAFETY_SET_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(LED0_GPIO_Port, LED0_Pin, GPIO_PIN_SET);		// led off
		xTimerChangePeriod( SAFETY_SETHandle, 100, 100 );
		//xTimerStart( SAFETY_SETHandle, 100 );
		return (TRUE);
	}
	return (FALSE);
}

/*******************************************************************************
*	Safety_RST
*	Timer
*
*******************************************************************************/

int32_t Safety_RST()
{
	if (PowerState | P24V)
	{
		HAL_GPIO_WritePin(SAFETY_RST_GPIO_Port, SAFETY_RST_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(LED0_GPIO_Port, LED0_Pin, GPIO_PIN_RESET);		// led on
		xTimerChangePeriod( SAFETY_RSTHandle, 100, 100 );
		//xTimerStart( SAFETY_RSTHandle, 100 );
		return (TRUE);
	}
	return (FALSE);
}

/*******************************************************************************
*	SafetySETCLI
*
*******************************************************************************/

int32_t SafetySETCLI(CLI_command_T* command)
{
	int32_t var;
   int32_t res;

   if (command->ArgType == GETPAR)
   {
	  if (gettxbuffer() == FALSE)
      {
		 errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
         return -3;
      }
		res = Safety_SET();
		if (res == TRUE)
		{
			sprintf (txbuff, "%s=%d\r\n", command->sCommand, 1);
		}
		else
		{
			sprintf (txbuff, "%s=%d\r\n", command->sCommand, -1);
		}
	  txbuff_semaphor = 1;
	  sendtxbuffer();
	  return TRUE;
   }
   else
   {
   	if ( command->ArgType == SETPAR)
		{
			sscanf(command->sParameter, "%ld", (int32_t *)&var);
			if (var == 1)
			{
				res = Safety_SET();
				if (res == TRUE)
				{
					sprintf (txbuff, "%s=%ld\r\n", command->sCommand, var);
				}
				else
				{
					sprintf (txbuff, "%s=%d\r\n", command->sCommand, -1);
				}
			}
			else
			{
				sprintf (txbuff, "%s=%ld\r\n", command->sCommand, var);
			}

			if (gettxbuffer() == FALSE)
			{
				errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
				return -3;
			}
			txbuff_semaphor = 1;
			sendtxbuffer();
			return TRUE;
		}
  	}
	return TRUE;
}

/*******************************************************************************
*	SafetyRSTCLI
*
*******************************************************************************/

int32_t SafetyRSTCLI(CLI_command_T* command)
{
	int32_t var;
   int32_t res;

   if (command->ArgType == GETPAR)
   {
   	if (gettxbuffer() == FALSE)
      {
   		errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
         return -3;
      }
   	res = Safety_RST();
   	if (res == TRUE)
   	{
       	sprintf (txbuff, "%s=%d\r\n", command->sCommand, 1);
   	}
		else
		{
			sprintf (txbuff, "%s=%d\r\n", command->sCommand, -1);
		}
   	txbuff_semaphor = 1;
   	sendtxbuffer();
   	return TRUE;
   }
   else { if ( command->ArgType == SETPAR)
   {
	   sscanf(command->sParameter, "%ld", (int32_t *)&var);
	   if (var == 1)
	   {
	   	res = Safety_RST();
	   	if (res == TRUE)
	   	{
	       	sprintf (txbuff, "%s=%ld\r\n", command->sCommand, var);
	   	}
			else
			{
				sprintf (txbuff, "%s=%d\r\n", command->sCommand, -1);
			}
	   }
	   else
	   {
       	sprintf (txbuff, "%s=%ld\r\n", command->sCommand, var);
	   }

		if (gettxbuffer() == FALSE)
		{
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			return -3;
		}
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
  	}
   }
	return TRUE;
}

/*******************************************************************************
*	SafetyAlertCLI
*	CLI
*
*******************************************************************************/

int32_t SafetyAlertCLI(CLI_command_T* command)
{
   int32_t val;

   if (command->ArgType == GETPAR)
   {
   	val = HAL_GPIO_ReadPin(SAFETY_ALERT_GPIO_Port, SAFETY_ALERT_Pin);
   	if (gettxbuffer() == FALSE)
      {
		 errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
         return -3;
      }
    	sprintf (txbuff, "%s=%ld\r\n", command->sCommand, val);
      txbuff_semaphor = 1;
      sendtxbuffer();
      return TRUE;
   }
   else
   {
   	if ( command->ArgType == SETPAR)
		{
			sscanf(command->sParameter, "%ld", (int32_t *)&val);
			sprintf (txbuff, "%s=%ld\r\n", command->sCommand, val);

			if (gettxbuffer() == FALSE)
			{
				errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
				return -3;
			}
			txbuff_semaphor = 1;
			sendtxbuffer();
			return TRUE;
		}
   }
	return TRUE;
}



/*
void safetytest(void)
{
	HAL_StatusTypeDef val;
	int i, mean1;

while(1){
	val = HAL_GPIO_ReadPin(SAFETY_ALERT_GPIO_Port, SAFETY_ALERT_Pin);			// 0
	val = HAL_GPIO_ReadPin(SAFETY_ALERT_GPIO_Port, SAFETY_ALERT_Pin);			// 0
	HAL_GPIO_WritePin(SAFETY_RST_GPIO_Port, SAFETY_RST_Pin, GPIO_PIN_SET);
	vTaskDelay(500);
	HAL_GPIO_WritePin(SAFETY_RST_GPIO_Port, SAFETY_RST_Pin, GPIO_PIN_RESET);

	vTaskDelay(2000);
	val = HAL_GPIO_ReadPin(SAFETY_ALERT_GPIO_Port, SAFETY_ALERT_Pin);
	HAL_GPIO_WritePin(SAFETY_SET_GPIO_Port, SAFETY_SET_Pin, GPIO_PIN_SET);
	TickType_t currentTick = xTaskGetTickCount();
	while(val != 0)
	{
		val = HAL_GPIO_ReadPin(SAFETY_ALERT_GPIO_Port, SAFETY_ALERT_Pin);
		i++;
	}
	mean1 = xTaskGetTickCount() - currentTick;
	HAL_GPIO_WritePin(SAFETY_SET_GPIO_Port, SAFETY_SET_Pin, GPIO_PIN_RESET);
	vTaskDelay(500);
}
}
*/

