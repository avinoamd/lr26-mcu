/*
 * Flash.c
 *
 *  Created on: 10 Oct 2020
 *      Author: avinoam.danieli
 */


/******************************************************************************
 * Init -
 * 	check or write "AVINOAM\n\r at sector1 start
 * 	check \n\r at end of sector if full
 * 	set sector variable
 *
 * Write -
 * 	check room in sector
 * 		look for \n\r from end to start
 *		 	if yes
 * 			write
 * 		else
 * 			write \n\r at end
 * 			erase other sector
 * 			set sector variable
 * 			write
 *
 * Read n
 * 	print last n messages
 *
 * SECT0			ISR
 * SECT1			EEPROM
 * SECT2			EEPROM
 * SECT3			FLASH
 * SECT4			FLASH
 *
 * Sector structure :
 * 	First bytes "AVINOAM"
 * 	messeges
 * 	when less than 100 bytes left
 * 		last bytes "DANIELI"
 *
 * 	*/

#include "main.h"
#include "cmsis_os.h"
#include <stdio.h>
#include <string.h>
#include "defs.h"
#include "uart.h"

#define F_SECT1_ADDR					0x0800C000
#define F_SECT2_ADDR					0x08010000
#define SECT_SIZE 0x4000							/*	16k	*/

const char Flash_String[] = "Avinoam\n\r";

#ifdef ERASE_FLASH
//__attribute__((section(".sector1"))) char Sector1[0x4000];
//__attribute__((section(".sector2"))) char Sector2[0x4000];
#endif
char * psect1 = (char *)F_SECT1_ADDR;
char * psect2 = (char *)F_SECT2_ADDR;

/********************************************************************
* FLerrorHandler
*
* Write to SWO
* Write to CLI UART3
*
********************************************************************/

void FLerrorHandler(int32_t num, char const * FILE, int LINE, char const * PRETTY_FUNCTION)
{
	sprintf(txebuff,  "File = %s  ", FILE);
	sprintf(txebuff+strlen(txebuff),  "Error line = %d  ", LINE);
	sprintf(txebuff+strlen(txebuff), "%s  ",  PRETTY_FUNCTION);
	sprintf(txebuff+strlen(txebuff), "UPTIME = %lu \n\r", xTaskGetTickCount() / 1000);
	printf("%s", txebuff);
	HAL_UART_Transmit(&huart3, (uint8_t *)txebuff, strlen(txebuff), 1000L);
}

/*******************************************************************************
*	flash_valid
*
*
*******************************************************************************/

int32_t flash_valid(void)
{
	if ( ( strncmp(Flash_String, psect1, strlen(Flash_String)) != 0 ) ||
		  ( strncmp(Flash_String, psect2, strlen(Flash_String)) != 0 ) )
		return FALSE;
	else
		return TRUE;
}

/*******************************************************************************
*	flash_format
*
*
*******************************************************************************/

int32_t flash_format(void)
{
	HAL_StatusTypeDef FlashStatus;
	uint32_t SectorError = 0;
	FLASH_EraseInitTypeDef pEraseInit;
	int32_t i;

	pEraseInit.TypeErase = TYPEERASE_SECTORS;
	pEraseInit.Sector = FLASH_SECTOR_3;
	pEraseInit.NbSectors = 2;
	pEraseInit.VoltageRange = VOLTAGE_RANGE_3;

	if (HAL_FLASH_Unlock() != HAL_OK)
	{
		 FLerrorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		 HAL_FLASH_Lock();
		 return FALSE;
	}

	FlashStatus = HAL_FLASHEx_Erase(&pEraseInit, &SectorError);					// Erase
	/* If erase operation was failed, a Flash error code is returned */
	if (FlashStatus != HAL_OK)
	{
		FLerrorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		HAL_FLASH_Lock();
		return FALSE;
	}

	for (i = 0;i < 20;i ++)
		printf("%x %x ", psect1[i], psect2[i]);
	printf("\n");

	for (i = 0;i < strlen(Flash_String);i ++)
	{
		/* Set variable data */
		FlashStatus = HAL_FLASH_Program(TYPEPROGRAM_BYTE, F_SECT1_ADDR + i, Flash_String[i]);
		/* If program operation was failed, a Flash error code is returned */
		if (FlashStatus != HAL_OK)
		{
			FLerrorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			HAL_FLASH_Lock();
			return FALSE;
		}
		FlashStatus = HAL_FLASH_Program(TYPEPROGRAM_BYTE, F_SECT2_ADDR + i, Flash_String[i]);
		/* If program operation was failed, a Flash error code is returned */
		if (FlashStatus != HAL_OK)
		{
			FLerrorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			HAL_FLASH_Lock();
			return FALSE;
		}
	}
	HAL_FLASH_Lock();
	return TRUE;
}

/*******************************************************************************
*	flash_init
*
*
*******************************************************************************/

int32_t flash_init(int32_t force)
{

	if (force == 1)												// force init of flash sectors
	{
		if ( flash_format() != TRUE )
		{
			 FLerrorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			 return FALSE;
		}
		else
			return TRUE;
	}
	else
	{
		if ( !flash_valid() )
		{
			if ( flash_format() != TRUE )
			{
				 FLerrorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
				 return FALSE;
			}
		}
	}
	return TRUE;
}

/*******************************************************************************
*	flash_last
*	search backwards on 0xFF until "\n"
*
*******************************************************************************/
#define u 71
uint8_t a[0x8000];
char b[] = "KK\n";
const char es[] = "DANIELI";
int32_t flash_last(void)
{
	int32_t i;
	uint8_t *cp;

	for ( i = 0;i < u;i++ )
	{
		a[i] = 'A';
	}
	for ( i = u+1;i < 0x8000;i++ )
	{
		a[i] = 0xff;
	}
	a[u] = 0;
	a[7] = 0;
	a[17] = 0;
	a[0x3000] = 0;

	uint32_t clk_cycle_start = DWT->CYCCNT;
	cp = memchr((uint8_t *)0x8020000, 0xcd, 0x8000);
	uint32_t clk_cycle_end = DWT->CYCCNT;

	printf("%ld  %ld  %ld\n", (int)0x8020000, (int)cp, (int)cp - (int)0x8020000 );
	printf("DWT = %ld\n", clk_cycle_end - clk_cycle_start);
	printf("%d  %d\n", strlen("!@#$%\n"), b[1]);
	return 1;
}


/*******************************************************************************
*	flash_write
*
*
*******************************************************************************/
int32_t flash_write(char *msg)
{
	int32_t i;
	int32_t left, msglen;
	uint8_t *cp;

	msglen = strlen(msg);
	// is first sector full
	if ( strncmp( (char *)(F_SECT1_ADDR + SECT_SIZE - 8), es, 7 ) == 0 )
	{
		// check room in second sector
		cp = memchr((char *)F_SECT1_ADDR, 0xff, SECT_SIZE);
		left = F_SECT2_ADDR + SECT_SIZE - (int)cp;
		if ( (left - msglen) < 20 )
		{
			// no room, write full mark
			FlashStatus = HAL_FLASH_Program(TYPEPROGRAM_HALFWORD, PAGE1_BASE_ADDRESS, VALID_PAGE);
		}
	}

	cp = memchr((char *)F_SECT1_ADDR, 0xff, SECT_SIZE);
	if ( memcmp(cp - 11, es, 10) == 0)									// sector full
	{
		cp = memchr((char *)F_SECT2_ADDR, 0xff, SECT_SIZE);
		left = F_SECT2_ADDR + SECT_SIZE - (int)cp;
	}
	else
	{
		left = F_SECT1_ADDR + SECT_SIZE - (int)cp;
	}

}

/*******************************************************************************
*	flash_read
*
*
*******************************************************************************/

int32_t flash_read(int32_t force)
{

}


