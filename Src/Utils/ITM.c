/*
 * ITM.c
 *
 *  Created on: Jul 1, 2020
 *      Author: avinoam.danieli
 */
#include "main.h"
#include "cmsis_os.h"
int GWM = 0xffff;
int _write(int file, char *ptr, int len)
{
  /* Implement your write code here, this is used by puts and printf for example */
  int i;
  for(i=0 ; i<len ; i++)
    ITM_SendChar((*ptr++));
  return len;
}

__STATIC_INLINE uint32_t ITM_Out (uint32_t cnl, uint32_t ch)
{
  if (((ITM->TCR & ITM_TCR_ITMENA_Msk) != 0UL) &&      /* ITM enabled */
      ((ITM->TER & 1UL               ) != 0UL)   )     /* ITM Port #0 enabled */
  {
    while (ITM->PORT[cnl].u32 == 0UL)
    {
      __NOP();
    }
    ITM->PORT[cnl].u8 = (uint8_t)ch;
  }
  return (ch);
}

void swvPrint(int port, char* ptr, int len)
{
  int i = 0;
  for (i = 0; i < len; i++)
    ITM_Out(port, (uint32_t) *ptr++);
}

