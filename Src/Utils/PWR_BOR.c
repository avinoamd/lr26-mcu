/*
 * PWR_BOR.c
 *
 *  Created on: Oct 8, 2020
 *      Author: avinoam.danieli
 */
#include "main.h"
#include <stdio.h>
#include <string.h>
#include "print.h"
//#include "stm32f4xx_hal_flash_ex.h"
// #define BOR_LEVEL OB_BOR_OFF /*!< BOR Reset threshold levels for 1.64V - 2.10V VDD power supply  */
//	#define BOR_LEVEL OB_BOR_LEVEL1     /*!< BOR Reset threshold levels for 2.10V - 2.40V VDD power supply */
// #define BOR_LEVEL OB_BOR_LEVEL2   /*!< BOR Reset threshold levels for 2.40V - 2.70V VDD power supply */
 #define BOR_LEVEL OB_BOR_LEVEL3   /*!< BOR Reset threshold levels for 2.70V - 3.00V VDD power supply */

FLASH_OBProgramInitTypeDef FLASH_OBInitStruct;

void PWR_BOR(void)
{
    HAL_FLASHEx_OBGetConfig(&FLASH_OBInitStruct);
    /* Get BOR Option Bytes */
    if((FLASH_OBInitStruct.BORLevel & 0x0C) != BOR_LEVEL)
    {
      /* Unlock the option bytes block access */
      HAL_FLASH_OB_Unlock();

      /* Select the desired V(BOR) Level */
      FLASH_OBInitStruct.OptionType = OPTIONBYTE_BOR;
      FLASH_OBInitStruct.BORLevel = BOR_LEVEL;
      HAL_FLASHEx_OBProgram(&FLASH_OBInitStruct);

      /* Launch the option byte loading */
      HAL_FLASH_OB_Launch();

      /* Locks the option bytes block access */
      HAL_FLASH_OB_Lock();
    }
}
/*
RCC_CSR_BORRSTF_Msk
*/
void reset_source(void)
{
	if (RCC->CSR & RCC_CSR_BORRSTF_Msk)
	{
		snprintf(Msguart3, MSGUART3LEN, "BOR reset\n");
		HAL_UART_Transmit(&huart3, (uint8_t *)Msguart3, strlen(Msguart3), 100);
	}
	if (RCC->CSR & RCC_CSR_PINRSTF_Msk)
	{
		snprintf(Msguart3, MSGUART3LEN, "PIN reset\n");
		HAL_UART_Transmit(&huart3, (uint8_t *)Msguart3, strlen(Msguart3), 100);
	}
	if (RCC->CSR & RCC_CSR_PORRSTF_Msk)
	{
		snprintf(Msguart3, MSGUART3LEN, "POR/PDR reset\n");
		HAL_UART_Transmit(&huart3, (uint8_t *)Msguart3, strlen(Msguart3), 100);
	}
	if (RCC->CSR & RCC_CSR_SFTRSTF_Msk)
	{
		snprintf(Msguart3, MSGUART3LEN, "Software reset\n");
		HAL_UART_Transmit(&huart3, (uint8_t *)Msguart3, strlen(Msguart3), 100);
	}
	if (RCC->CSR & RCC_CSR_IWDGRSTF_Msk)
	{
		snprintf(Msguart3, MSGUART3LEN, "Independent watchdog reset\n");
		HAL_UART_Transmit(&huart3, (uint8_t *)Msguart3, strlen(Msguart3), 100);
	}
	if (RCC->CSR & RCC_CSR_WWDGRSTF_Msk)
	{
		snprintf(Msguart3, MSGUART3LEN, "Window watchdog reset\n");
		HAL_UART_Transmit(&huart3, (uint8_t *)Msguart3, strlen(Msguart3), 100);
	}
	if (RCC->CSR & RCC_CSR_LPWRRSTF_Msk)
	{
		snprintf(Msguart3, MSGUART3LEN, "Low-power reset\n");
		HAL_UART_Transmit(&huart3, (uint8_t *)Msguart3, strlen(Msguart3), 100);
	}

	RCC->CSR |= RCC_CSR_RMVF;
}
