/*
 * Ticks.c
 *
 *  Created on: Aug 11, 2020
 *      Author: avinoam.danieli
 */

#include "cmsis_os.h"
#include "stm32f4xx_hal.h"
void HAL_IncTick(void)
{
  uwTick += uwTickFreq;
}

/**
  * @brief Provides a tick value in millisecond.
  * @note This function is declared as __weak to be overwritten in case of other
  *       implementations in user file.
  * @retval tick value
  */
uint32_t HAL_GetTick(void)
{
  return uwTick;
}
/* blocking delay using freertos */
void my_delay(TickType_t ticks)
{
	TickType_t currentTick = xTaskGetTickCount();
	while( ( xTaskGetTickCount() - currentTick) < ticks )
	{
    //…
	}
}

