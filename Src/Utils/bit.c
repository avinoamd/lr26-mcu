/*
 * bit.c
 *
 *  Created on: 11 Nov 2020
 *      Author: avinoam.danieli
 */

/*
 * uptime
 * rst / set safety and print sto, ack time
 * plc
 * power status
 * goto LPM and print voltages
 * stop PLC communication
 * turn ps on and print voltages
 * precharge time
 * leds
 * safety
 * key
 * set brake and print voltage
 * bms
 * imu
 * charger
 * start PLC communication
 * goto LPM
 */

#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "FreeRTOS.h"
#include "timers.h"
#include <stdio.h>
#include <string.h>
#include "defs.h"
#include "ADC.h"
#include "CLIcmnds.h"
#include "uart.h"
#include "stm32f4xx_ll_adc.h"
#include "Boardinit.h"
#include "PushButton.h"
#include "LEDs.h"
#include "queue.h"
#include "Safety.h"
#include "lsm6ds3.h"
#include "BCR.h"
#include "BMS.h"

int32_t PrechargeTime;

int32_t test_intr_tmpr(void)
{
	float /*inttmpr, VSENSE, */Temp;
	float TS_CAL1, TS_CAL2;
	int32_t val;

	val = ADC_DMA_Buffer[INTR_TMPR];

	//VSENSE = (Vref/4095.0) * val;
	//inttmpr = ((VSENSE - V25) / Avg_Slope) + 25.0;

	if (gettxbuffer() == TRUE)
	{
		TS_CAL1 = (float)(*TEMPSENSOR_CAL1_ADDR * VREFINT_CAL_VREF) / ( (float)Vref * 1000.0 );

		TS_CAL2 = (float)(*TEMPSENSOR_CAL2_ADDR * VREFINT_CAL_VREF) / ( (float)Vref * 1000.0 );
		Temp = ( (float)( (TEMPSENSOR_CAL2_TEMP - TEMPSENSOR_CAL1_TEMP)*(val - TS_CAL1) ) / (float)(TS_CAL2 - TS_CAL1) ) + TEMPSENSOR_CAL1_TEMP;

		snprintf (txbuff, TXMSG_MAX_LEN, "%s = %3.1f Degrees", "MCU Internal Temperature", Temp);
		if (Temp < 70.0)
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t ........ pass    \n\r");
		else
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t ........ fail    \n\r");
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
	}
	return FALSE;
}

int32_t TestInternalVref(void)
{
	int32_t val;
	float VSENSE;

	val = ADC_DMA_Buffer[INTR_VREF];

	VSENSE = (Vref/4095.0) * val;

	if (gettxbuffer() == TRUE)
	{
		sprintf (txbuff, "MCU Internal Vref = %3.3f", VSENSE);
		if ( (VSENSE < 1.215) && (VSENSE > 1.195) )
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t ........ pass    \n\r");
		else
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t ........ fail    \n\r");
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   }
   return TRUE;
}

/*******************************************************************************
*	TestBrake
*	turn brake pwm on
*	rst safety
*	measure current
*	compare to target
*
*******************************************************************************/

void TestBrake(void)
{
	if ( BrakeOn() )
	{
		Safety_RST();
		vTaskDelay(500);
	}
}

/*******************************************************************************
*	safety_bit
*
*******************************************************************************/

void safetybit(void)
{
	int q;

	while (xTaskGetTickCount() < 15000)							// wait for safety controller to power up
		vTaskDelay(500);
	Safety_SET();														// known state
	vTaskDelay(500);													// wait for completion
	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf (txbuff, TXMSG_MAX_LEN, "Safety_RST : STO = %d", q = HAL_GPIO_ReadPin(STO_GPIO_Port, STO_Pin));
		if (q == GPIO_PIN_RESET)
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ pass    \n\r");
		else
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ fail    \n\r");
		sendtxbuffer();
	}
	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf (txbuff, TXMSG_MAX_LEN, "Safety_RST : ALERT = %d", q = HAL_GPIO_ReadPin(SAFETY_ALERT_GPIO_Port, SAFETY_ALERT_Pin));
		if (q == GPIO_PIN_RESET)
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ pass    \n\r");
		else
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ fail    \n\r");
		sendtxbuffer();
	}
	Safety_RST();
	vTaskDelay(500);
	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf (txbuff, TXMSG_MAX_LEN, "Safety_SET : STO = %d", q = HAL_GPIO_ReadPin(STO_GPIO_Port, STO_Pin));
		if (q == GPIO_PIN_SET)
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ pass    \n\r");
		else
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ fail    \n\r");
		sendtxbuffer();
	}
	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf (txbuff, TXMSG_MAX_LEN, "Safety_SET : ALERT = %d", q = HAL_GPIO_ReadPin(SAFETY_ALERT_GPIO_Port, SAFETY_ALERT_Pin));
		if (q == GPIO_PIN_SET)
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ pass    \n\r");
		else
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ fail    \n\r");
		sendtxbuffer();
	}
}

/*******************************************************************************
*	bit
*
*******************************************************************************/

void bit(void)
{
	float VSENSE, VSENSE2;
	uint16_t q;
	int32_t i;
	int32_t start;
	int32_t status;

	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf(txbuff, TXMSG_MAX_LEN, "BIT Starting \n\r");
		sendtxbuffer();
	}

	safetybit();

	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf(txbuff, TXMSG_MAX_LEN, "UPTIME=%lu    \n\r", HAL_GetTick() / 1000);
		sendtxbuffer();
	}
//	Low Power Mode -------------------------------------------------------------
	xTaskNotify( xTaskToNotifyPBTask, LPM, eSetValueWithOverwrite );			// LPM
  	vTaskDelay(2000);																			// wait more time
	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf (txbuff, TXMSG_MAX_LEN, "Going into Low Power Mode \r\n");
		sendtxbuffer();
	}
	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf (txbuff, TXMSG_MAX_LEN, "%s = %c%c%c%c%c%c%c%c", "PowerState", BYTE_TO_BINARY(PowerState));
		if (PowerState & P3V3)
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ pass    \n\r");
		else
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ fail    \n\r");
		sendtxbuffer();
	}
	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		q = ADC_DMA_Buffer[INTR_VBATT];
		snprintf (txbuff, TXMSG_MAX_LEN, "P3V3 Voltage = %1.3f", (float)q / 1000.0);
		if ( (q >= P3V3_LOW_LIMIT) && (q <= P3V3_HIGH_LIMIT) )
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ pass    \n\r");
		else
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ fail    \n\r");
		sendtxbuffer();
	}
	getvbatt(&VSENSE);
	q = CheckBatt(0);
	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf (txbuff, TXMSG_MAX_LEN, "Battery Voltage = %3.3f", VSENSE);
		if ( (q == TRUE) )
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t ........ pass    \n\r");
		else
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t ........ fail    \n\r");
		sendtxbuffer();
	}
	vTaskDelay(100);
	getvsw(&VSENSE);
	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf (txbuff, TXMSG_MAX_LEN, "Switched Voltage = %3.3f", VSENSE);
		if ( (VSENSE < 46.0) )
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t ........ pass    \n\r");
		else
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t ........ fail    \n\r");
		sendtxbuffer();
	}
	i = HAL_GPIO_ReadPin(VSW_FAULT_GPIO_Port, VSW_FAULT_Pin);
	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf (txbuff, TXMSG_MAX_LEN, "Power Switch Status = %ld", i);
		if ( (i == 1) )
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ pass    \n\r");
		else
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ fail    \n\r");
		sendtxbuffer();
	}
	getp5v(&VSENSE);
	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf (txbuff, TXMSG_MAX_LEN, "P5V Voltage = %3.3f", VSENSE);
		if ( (VSENSE < 2.0) )
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ pass    \n\r");
		else
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ fail    \n\r");
		sendtxbuffer();
	}
	getp24v(&VSENSE);
	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf (txbuff, TXMSG_MAX_LEN, "P24V Voltage = %3.3f", VSENSE);
		if ( (VSENSE < 3.0) )
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ pass    \n\r");
		else
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ fail    \n\r");
		sendtxbuffer();
	}
	getvswcurr(&VSENSE);
	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf (txbuff, TXMSG_MAX_LEN, "Switch Current = %3.3f  \r\n", VSENSE);				// !!!
		sendtxbuffer();
	}
	getvcharge(&VSENSE);
	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf (txbuff, TXMSG_MAX_LEN, "Charger Pads Voltage = %3.3f", VSENSE);
		if ( (VSENSE < 3.0) )
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t ........ pass    \n\r");
		else
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t ........ fail    \n\r");
		sendtxbuffer();
	}
	test_intr_tmpr();
	TestInternalVref();
//	On Mode --------------------------------------------------------------------
	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf (txbuff, TXMSG_MAX_LEN, "Powering On \r\n\n");
		sendtxbuffer();
	}
	xTaskNotify( xTaskToNotifyPBTask, ON, eSetValueWithOverwrite );			// On
  	vTaskDelay(2000);																			// wait more time
	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf(txbuff, TXMSG_MAX_LEN, "PrechargeTime = %lu", PrechargeTime);
		if (PrechargeTime < 2500)
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ pass    \n\r");
		else
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ fail    \n\r");
		sendtxbuffer();
	}
	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf (txbuff, TXMSG_MAX_LEN, "%s = %c%c%c%c%c%c%c%c", "PowerState", BYTE_TO_BINARY(PowerState));
		if (PowerState == (P3V3 | P24V | P5V | VSWON | DCDC24V))
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ pass    \n\r");
		else
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ fail    \n\r");
		sendtxbuffer();
	}
	vTaskDelay(100);
	getvbatt(&VSENSE2);
	getvsw(&VSENSE);
	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf (txbuff, TXMSG_MAX_LEN, "Switch Voltage = %3.3f", VSENSE2 - VSENSE);
		if ( (VSENSE2 - VSENSE < 1.5) )
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ pass    \n\r");
		else
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ fail    \n\r");
		sendtxbuffer();
	}
	i = HAL_GPIO_ReadPin(VSW_FAULT_GPIO_Port, VSW_FAULT_Pin);
	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf (txbuff, TXMSG_MAX_LEN, "Power Switch Status = %ld", i);
		if ( (i == 1) )
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ pass    \n\r");
		else
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ fail    \n\r");
		sendtxbuffer();
	}
	getp5v(&VSENSE);
	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf (txbuff, TXMSG_MAX_LEN, "P5V Voltage = %3.3f", VSENSE);
		if ( (VSENSE >= P5V_LOW_LIMIT_F) && (VSENSE <= P5V_HIGH_LIMIT_F) )
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ pass    \n\r");
		else
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ fail    \n\r");
		sendtxbuffer();
	}
	getp24v(&VSENSE);
	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf (txbuff, TXMSG_MAX_LEN, "P24V Voltage = %3.3f", VSENSE);
		if ( (VSENSE >= DCDC24V_LOW_LIMIT_F) && (VSENSE <= DCDC24V_HIGH_LIMIT_F) )
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ pass    \n\r");
		else
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t ........ fail    \n\r");
		sendtxbuffer();
	}
	getvswcurr(&VSENSE);
	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf (txbuff, TXMSG_MAX_LEN, "Switch Current = %3.3f  \r\n", VSENSE);				// !!!
		sendtxbuffer();
	}
	getvcharge(&VSENSE);
	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf (txbuff, TXMSG_MAX_LEN, "Charger Pads Voltage = %3.3f", VSENSE);
		if ( (VSENSE < 3.0) )
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t ........ pass    \n\r");
		else
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t ........ fail    \n\r");
		sendtxbuffer();
	}
	typeLEDs LED;
   LED.num = 1;
	LED.power = 99;
	LED.on = 5;
	LED.off = 5;
	xQueueSend( LEDqHandle, &LED, 2000 );
	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf (txbuff, TXMSG_MAX_LEN, "RED LED Blink \n\r");
		sendtxbuffer();
	}
	vTaskDelay(3000);
   LED.num = 2;
	xQueueSend( LEDqHandle, &LED, 2000 );
   LED.num = 1;
	LED.power = 0;
	xQueueSend( LEDqHandle, &LED, 2000 );
	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf (txbuff, TXMSG_MAX_LEN, "GREEN LED Blink \n\r");
		sendtxbuffer();
	}
	vTaskDelay(3000);
   LED.num = 3;
	LED.power = 99;
	xQueueSend( LEDqHandle, &LED, 2000 );
   LED.num = 2;
	LED.power = 0;
	xQueueSend( LEDqHandle, &LED, 2000 );
	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf (txbuff, TXMSG_MAX_LEN, "BLUE LED Blink \n\r");
		sendtxbuffer();
	}
	vTaskDelay(3000);
   LED.num = 1;
	LED.power = 29;
	xQueueSend( LEDqHandle, &LED, 2000 );
   LED.num = 2;
	LED.power = 59;
	xQueueSend( LEDqHandle, &LED, 2000 );
	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf (txbuff, TXMSG_MAX_LEN, "WHITE LED Blink \n\r");
		sendtxbuffer();
	}

	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf (txbuff, TXMSG_MAX_LEN, "Turn KEY Off \n\r");
		sendtxbuffer();
	}
	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf (txbuff, TXMSG_MAX_LEN, "KEY = %d", q = HAL_GPIO_ReadPin(KEY_MCU_GPIO_Port, KEY_MCU_Pin));
		if ( q == GPIO_PIN_RESET )
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t\t\t ........ pass    \n\r");
		else
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t\t\t ........ fail    \n\r");
		sendtxbuffer();
	}

	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf (txbuff, TXMSG_MAX_LEN, "Turn KEY On \n\r");
		sendtxbuffer();
	}
	start = xTaskGetTickCount();
	while ( (HAL_GPIO_ReadPin(KEY_MCU_GPIO_Port, KEY_MCU_Pin) != GPIO_PIN_SET) &&
			  ((start + 6000) > xTaskGetTickCount()) );
	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf (txbuff, TXMSG_MAX_LEN, "KEY = %d", q = HAL_GPIO_ReadPin(KEY_MCU_GPIO_Port, KEY_MCU_Pin));
		if ( q == GPIO_PIN_SET )
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t\t\t ........ pass    \n\r");
		else
			snprintf(txbuff+strlen(txbuff), TXMSG_MAX_LEN, "\t\t\t\t\t ........ fail    \n\r");
		sendtxbuffer();
	}

	CLI_command_T command;
	command.ArgType = GETPAR;
	strcpy(command.sCommand, "IMU Test Start\n\r");
	IMU_CLI_getdata(&command);

	strcpy(command.sCommand, "BMS Test Start\n\r");
	BMS_CLI(&command);

	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		status = BCR_en();
		if (status != TRUE)
		{
			snprintf(txbuff, TXMSG_MAX_LEN, "\nBCR Test\t\t\t\t ........ fail    \n\r");
			sendtxbuffer();
		}
		else
		{
			status = BCR_dis();
			if (status != TRUE)
			{
				snprintf(txbuff, TXMSG_MAX_LEN, "BCR Test\t\t\t\t ........ fail    \n\r");
				sendtxbuffer();
			}
			else
			{
				snprintf(txbuff, TXMSG_MAX_LEN, "BCR Test\t\t\t\t ........ pass    \n\r");
				sendtxbuffer();
			}
		}
	}

	if (gettxbuffer() == TRUE)
	{
		txbuff_semaphor = 1;
		snprintf(txbuff, TXMSG_MAX_LEN, "\nBIT Complete \n\r");
		sendtxbuffer();
	}

	vTaskDelay(3000);
}

/*******************************************************************************
*	BIT_CLI
*	CLI
*
*******************************************************************************/

int32_t BIT_CLI(CLI_command_T* command)
{

	{
	   if ( (command->ArgType == GETPAR) || ( command->ArgType == SETPAR) )
	   {
	   	bit();
	   }
   }
	return TRUE;
}


//	printf("%d  safety_alert = %d STO = %d\n", i, HAL_GPIO_ReadPin(SAFETY_ALERT_GPIO_Port, SAFETY_ALERT_Pin), HAL_GPIO_ReadPin(STO_GPIO_Port, STO_Pin));
