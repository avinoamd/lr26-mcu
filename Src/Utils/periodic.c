/*
 * periodic.c
 *
 *  Created on: Dec 13, 2020
 *      Author: avinoam.danieli
 */

/* Includes ------------------------------------------------------------------*/
/*
 * check power status
 * vbatt
 * vsw diff
 * P24V
 * P5V
 * P3V3
 */
#include "main.h"
#include "cmsis_os.h"
#include "FreeRTOS.h"
#include "defs.h"
#include <stdio.h>
#include <string.h>
#include "CLI.h"
#include "CLIcmnds.h"
#include "uart.h"
#include "print.h"
#include "ADC.h"
#include "BoardInit.h"
#include "PushButton.h"
#include "Battery.h"

#define BcurrentzeroLow 3038
#define BcurrentzeroHigh 3639
#define BcurrentzeroDef 3342
int32_t CheckP3V3(void);
int32_t CheckP5V(void);
int32_t CheckP24V(void);
int32_t CheckVswDiff(void);

uint16_t Bcurrentzero = 0;

void measure_brake_zero(void)
{
	int i;

	for (i = 0;i < 8;i ++)
	{
		Bcurrentzero += ADC_DMA_Buffer[DMA_BRKCURR];
		HAL_Delay(4);
	}
	Bcurrentzero = Bcurrentzero >> 3;
	if ( ( (Bcurrentzero <= BcurrentzeroLow) || (Bcurrentzero >= BcurrentzeroHigh) ) )			// outside limit
	{
		Bcurrentzero = BcurrentzeroDef;
	}
}

/*******************************************************************************
*	bcurrent
*	set brake current to required value
*	PID controller, only P implemented
*	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_1)
*	clk = 48MHz, prescaler = 47, period = 99
*	10KHz, 1/100 duty cycle step
*	current = 5m * 20
*
*******************************************************************************/

int32_t bcurrent(void)
{
	uint16_t value;
	int32_t set;
	static int32_t current = 100;
	static int c = 0, loop = 0;

	value = ADC_DMA_Buffer[DMA_BRKCURR];
	if ( (value - Bcurrentzero) > 20 )											// check there is current
	{
		current = (current + value - Bcurrentzero) >> 1;	// IIR
		if (PowerState & BRAKE)								// check brake operated
		{															// check relay
			if (  HAL_GPIO_ReadPin(STO_GPIO_Port, STO_Pin) == GPIO_PIN_SET )
			{
				if ( (BrakeCurrent  > 0) && (BrakeCurrent<=3000) )
				{
					set = BrakeCurrent / 5;
					if ( (current - set) > 3 )								// decrease
					{
						if (htim4.Instance->CCR1 > 32){
							htim4.Instance->CCR1 --;
						printf("%ld\t\t\t%d\t%d\n", htim4.Instance->CCR1, ++c, loop);}
					}
					if ( (set - current) > 3 )								// decrease
					{
						if (htim4.Instance->CCR1 < 60){
							htim4.Instance->CCR1 ++;
							printf("%ld\t\t\t%d\t%d\n", htim4.Instance->CCR1, ++c, loop);}
					}
				}
			}
		}
	}
	loop ++;
	return TRUE;
}

/*******************************************************************************
*	periodic
*	Task
*	this task perform all periodic tasks such as BIT.
*	Set DRV_GPIO
*
*******************************************************************************/

void fff1();
void periodic(void)
{
	static int32_t cnt = 0;

	vTaskDelay(2000);
	BMS_Detect();

	for (;;)
	{
		vTaskDelay(10);
		bcurrent();											// check On State
		if ( (PowerState & (VSWON | DCDC24V | P5V | P3V3)) == (VSWON | DCDC24V | P5V | P3V3) )
		{
			if ( !CheckBatt(0) )
			{
				gettxbuffer();
		    	snprintf (txbuff, TXMSG_MAX_LEN, "CheckBatt Failed....LPM\r\n");
				txbuff_semaphor = 1;
				sendtxbuffer();
				xTaskNotify( xTaskToNotifyPBTask, LPM, eSetValueWithOverwrite );
			}
		}
		if ( (PowerState & (VSWON | DCDC24V | P5V | P3V3)) == (VSWON | DCDC24V | P5V | P3V3) )
		{
			if ( !CheckVswDiff() )
			{
				gettxbuffer();
		    	snprintf (txbuff, TXMSG_MAX_LEN, "CheckVswDiff Failed....LPM\r\n");
				txbuff_semaphor = 1;
				sendtxbuffer();
		   	xTaskNotify( xTaskToNotifyPBTask, LPM, eSetValueWithOverwrite );
			}
		}
		if ( (PowerState & (VSWON | DCDC24V | P5V | P3V3)) == (VSWON | DCDC24V | P5V | P3V3) )
		{
			if ( !CheckP24V() )
			{
				gettxbuffer();
		    	snprintf (txbuff, TXMSG_MAX_LEN, "CheckP24V Failed....LPM\r\n");
				txbuff_semaphor = 1;
				sendtxbuffer();
		   	xTaskNotify( xTaskToNotifyPBTask, LPM, eSetValueWithOverwrite );
			}
		}
		//taskENTER_CRITICAL();
		if ( (PowerState & (VSWON | DCDC24V | P5V | P3V3)) == (VSWON | DCDC24V | P5V | P3V3) )
		{
			if ( !CheckP5V() )
		   {
				//taskEXIT_CRITICAL();
				gettxbuffer();
		    	snprintf (txbuff, TXMSG_MAX_LEN, "CheckP5V Failed....LPM\r\n");
				txbuff_semaphor = 1;
				sendtxbuffer();
				xTaskNotify( xTaskToNotifyPBTask, LPM, eSetValueWithOverwrite );
		   }
			else
			{
				//taskEXIT_CRITICAL();
			}
		}
		if ( (PowerState & (VSWON | DCDC24V | P5V | P3V3)) == (VSWON | DCDC24V | P5V | P3V3) )
		{
			if ( !CheckP3V3() )
		   {
				gettxbuffer();
		    	snprintf (txbuff, TXMSG_MAX_LEN, "CheckP3V3 Failed....LPM\r\n");
				txbuff_semaphor = 1;
				sendtxbuffer();
		   	xTaskNotify( xTaskToNotifyPBTask, LPM, eSetValueWithOverwrite );
		   }
		}
		if (BMSPresent)
		{
			if (cnt++ >= 100)
			{
				cnt = 1;
				Battery_Status();
			}
		}
	} // for
}

/*******************************************************************************
*	CheckP3V3
*
*******************************************************************************/

int32_t CheckP3V3(void)
{
   int32_t val;

   val = ADC_DMA_Buffer[INTR_VBATT];
	if ( (val > P3V3_LOW_LIMIT) && (val < P3V3_HIGH_LIMIT))
	{
		if ( HAL_GPIO_ReadPin(P3V3_PG_GPIO_Port, P3V3_PG_Pin) == GPIO_PIN_SET )
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	else
		return FALSE;
}

/*******************************************************************************
*	CheckP5V
*
*******************************************************************************/

int32_t CheckP5V(void)
{
   int32_t val;

   val = ADC_DMA_Buffer[DMA_P5V];
	 if ( (val > P5V_LOW_LIMIT) && (val < P5V_HIGH_LIMIT))
			return TRUE;
		else
		{
			return FALSE;
		}
}

/*******************************************************************************
*	CheckP24V
*
*******************************************************************************/

int32_t CheckP24V(void)
{
   int32_t val;

	val = ADC_DMA_Buffer[DMA_P24V];
   if ( (val > DCDC24V_LOW_LIMIT) && (val < DCDC24V_HIGH_LIMIT) )
   {
   	if ( HAL_GPIO_ReadPin(P24V_PG_GPIO_Port, P24V_PG_Pin) == GPIO_PIN_SET )
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	else
		return FALSE;
}

/*******************************************************************************
*	CheckVswDiff
*
*******************************************************************************/

int32_t CheckVswDiff(void)
{
   int32_t vbatt = 0x7fffffff, vsw = 0;

   vsw = ADC_DMA_Buffer[DMA_VSW];
	vbatt = ADC_DMA_Buffer[DMA_VBATT];
	if ( ( vbatt - vsw ) < ( PRECHARGE_LIMIT << 2 ) )  									// compare voltages
		return TRUE;
	else
		return FALSE;
}

/*******************************************************************************
*	Brake_Current
*	CLI
*
*******************************************************************************/

int32_t Brake_Current(CLI_command_T* command)
{
   int32_t stam;

   if (command->ArgType == GETPAR)
   {
	  if (gettxbuffer() == FALSE)
      {
	     errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
         return -3;
      }
	  snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n",command->sCommand, BrakeCurrent);
	  txbuff_semaphor = 1;
	  sendtxbuffer();
	  return TRUE;
   }
   else if ( command->ArgType == SETPAR)
   {
	   sscanf(command->sParameter, "%ld", (int32_t *)&stam);
	   if ( (stam >= 0) && (stam <= 3000) )
	   {
	   	BrakeCurrent = stam;
	   }

		if (gettxbuffer() == FALSE)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		   return -3;
		}
    	snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n", command->sCommand, BrakeCurrent);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   }
   return 1;
}

