/*
 * print.c
 *
 *  Created on: Sep 14, 2020
 *      Author: avinoam.danieli
 */

#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include "defs.h"
#include "CLI.h"
#include "CLIcmnds.h"
#include "uart.h"
#include "print.h"
#include "PLC.h"
#include "SOM_MODBUS.h"
//#include "eeprom_utils.h"

char Msguart3[MSGUART3LEN];
char Msguart3[MSGUART3LEN];

uint16_t EEPROM_Array[EEPROM_LEN] = {FALSE, FALSE, FALSE, TRUE, FALSE, FALSE, TRUE};

/*******************************************************************************
*	psom_swo
*	CLI
*
*******************************************************************************/

int32_t psom_swo(CLI_command_T* command)
{
   int32_t stam;

   if (command->ArgType == GETPAR)
   {
	  if (gettxbuffer() == FALSE)
      {
	     errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
         return -3;
      }
	  snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n",command->sCommand, SOMPrintToSWO);
	  txbuff_semaphor = 1;
	  sendtxbuffer();
	  return TRUE;
   }
   else if ( command->ArgType == SETPAR)
   {
	   sscanf(command->sParameter, "%ld", (int32_t *)&stam);
	   if (stam == 1)
	   {
	   	SOMPrintToSWO = TRUE;
	   }
	   else
	   {
		   SOMPrintToSWO = FALSE;
	   }

		if (gettxbuffer() == FALSE)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		   return -3;
		}
    	snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n", command->sCommand, SOMPrintToSWO);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   	}
    return 1;
}

/*******************************************************************************
*	psom_uart3
*	CLI
*
*******************************************************************************/

int32_t psom_uart3(CLI_command_T* command)
{
   int32_t stam;

   if (command->ArgType == GETPAR)
   {
	  if (gettxbuffer() == FALSE)
      {
	     errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
         return -3;
      }
	  snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n",command->sCommand, SOMPrintToUart3);
	  txbuff_semaphor = 1;
	  sendtxbuffer();
	  return TRUE;
   }
   else if ( command->ArgType == SETPAR)
   {
	   sscanf(command->sParameter, "%ld", (int32_t *)&stam);
	   if (stam == 1)
	   {
	   	SOMPrintToUart3 = TRUE;
	   }
	   else
	   {
	   	SOMPrintToUart3 = FALSE;
	   }

		if (gettxbuffer() == FALSE)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		   return -3;
		}
    	snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n", command->sCommand, PLCPrintToSWO);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   	}
    return 1;
}

/*******************************************************************************
*	pplc_swo
*	CLI
*
*******************************************************************************/

int32_t pplc_swo(CLI_command_T* command)
{
   int32_t stam;

   if (command->ArgType == GETPAR)
   {
	  if (gettxbuffer() == FALSE)
      {
	     errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
         return -3;
      }
	  snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n",command->sCommand, PLCPrintToSWO);
	  txbuff_semaphor = 1;
	  sendtxbuffer();
	  return TRUE;
   }
   else if ( command->ArgType == SETPAR)
   {
	   sscanf(command->sParameter, "%ld", (int32_t *)&stam);
	   if (stam == 1)
	   {
	   	PLCPrintToSWO = TRUE;
	   }
	   else
	   {
		   PLCPrintToSWO = FALSE;
	   }

		if (gettxbuffer() == FALSE)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		   return -3;
		}
    	snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n", command->sCommand, PLCPrintToSWO);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   	}
    return 1;
}

/*******************************************************************************
*	pplc_uart3
*	CLI
*
*******************************************************************************/

int32_t pplc_uart3(CLI_command_T* command)
{
   int32_t stam;

   if (command->ArgType == GETPAR)
   {
	  if (gettxbuffer() == FALSE)
      {
	     errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
         return -3;
      }
	  snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n",command->sCommand, PLCPrintToUart3);
	  txbuff_semaphor = 1;
	  sendtxbuffer();
	  return TRUE;
   }
   else if ( command->ArgType == SETPAR)
   {
	   sscanf(command->sParameter, "%ld", (int32_t *)&stam);
	   if (stam == 1)
	   {
	   	PLCPrintToUart3 = TRUE;
	   }
	   else
	   {
	   	PLCPrintToUart3 = FALSE;
	   }

		if (gettxbuffer() == FALSE)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		   return -3;
		}
    	snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n", command->sCommand, PLCPrintToUart3);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   	}
    return 1;
}

/*******************************************************************************
*	pbms_uart3
*	CLI
*
*******************************************************************************/

int32_t pbms_uart3(CLI_command_T* command)
{
   int32_t stam;

   if (command->ArgType == GETPAR)
   {
	  if (gettxbuffer() == FALSE)
      {
	     errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
         return -3;
      }
	  snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n",command->sCommand, BMSPrintToUart3);
	  txbuff_semaphor = 1;
	  sendtxbuffer();
	  return TRUE;
   }
   else if ( command->ArgType == SETPAR)
   {
	   sscanf(command->sParameter, "%ld", (int32_t *)&stam);
	   if (stam == 1)
	   {
	   	BMSPrintToUart3 = TRUE;
	   }
	   else
	   {
	   	BMSPrintToUart3 = FALSE;
	   }

		if (gettxbuffer() == FALSE)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		   return -3;
		}
    	snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n", command->sCommand, BMSPrintToUart3);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   	}
    return 1;
}

/*******************************************************************************
*	som_cnt
*	CLI
*
*******************************************************************************/

int32_t som_cnt(CLI_command_T* command)
{
   int32_t stam;

   if (command->ArgType == GETPAR)
   {
	  if (gettxbuffer() == FALSE)
      {
	     errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
         return -3;
      }
	  snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n",command->sCommand, SOMcnt);
	  txbuff_semaphor = 1;
	  sendtxbuffer();
	  return TRUE;
   }
   else if ( command->ArgType == SETPAR)
   {
	   sscanf(command->sParameter, "%ld", (int32_t *)&stam);
	   if (stam == 1)
	   {
	   	PLCcnt = 0;
	   }

		if (gettxbuffer() == FALSE)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		   return -3;
		}
    	snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n", command->sCommand, SOMcnt);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   	}
    return 1;
}

/*******************************************************************************
*	plc_cnt
*	CLI
*
*******************************************************************************/

int32_t plc_cnt(CLI_command_T* command)
{
   int32_t stam;

   if (command->ArgType == GETPAR)
   {
	  if (gettxbuffer() == FALSE)
      {
	     errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
         return -3;
      }
	  snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d  %d\r\n",command->sCommand, PLCcnt, PLCcntOut);
	  txbuff_semaphor = 1;
	  sendtxbuffer();
	  return TRUE;
   }
   else if ( command->ArgType == SETPAR)
   {
	   sscanf(command->sParameter, "%ld", (int32_t *)&stam);
	   if (stam == 1)
	   {
	   	PLCcnt = 0;
	   	PLCcntOut = 0;
	   }

		if (gettxbuffer() == FALSE)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		   return -3;
		}
		snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d  %d\r\n",command->sCommand, PLCcnt, PLCcntOut);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   }
   return 1;
}
