/*
 * t.c
 *
 *  Created on: Sep 9, 2020
 *      Author: avinoam.danieli
 */
#include "main.h"
#include "cmsis_os.h"

#include <stdio.h>
#include <string.h>
#include "defs.h"

#include "globals.h"
unsigned long TGG;

unsigned long getRunTimeCounterValue(void)
{
	TGG ++;
return __HAL_TIM_GET_COUNTER(&htim5);
}


#include "stm32f4xx_ll_adc.h"
#include "ADC.h"
/*
#define __LL_ADC_CALC_TEMPERATURE(__VREFANALOG_VOLTAGE__,\
                                  __TEMPSENSOR_ADC_DATA__,\
                                  __ADC_RESOLUTION__)                              \
  (((( ((int32_t)((__LL_ADC_CONVERT_DATA_RESOLUTION((__TEMPSENSOR_ADC_DATA__),     \
                                                    (__ADC_RESOLUTION__),          \
                                                    LL_ADC_RESOLUTION_12B)         \
                   * (__VREFANALOG_VOLTAGE__))                                     \
                  / TEMPSENSOR_CAL_VREFANALOG)                                     \
        - (int32_t) *TEMPSENSOR_CAL1_ADDR)                                         \
     ) * (int32_t)(TEMPSENSOR_CAL2_TEMP - TEMPSENSOR_CAL1_TEMP)                    \
    ) / (int32_t)((int32_t)*TEMPSENSOR_CAL2_ADDR - (int32_t)*TEMPSENSOR_CAL1_ADDR) \
   ) + TEMPSENSOR_CAL1_TEMP                                                        \
  )
*/

void qqw(int32_t var)
{
	int32_t a, b, c, d, e, r;

	a = (int32_t)((__LL_ADC_CONVERT_DATA_RESOLUTION((var),     \
                                                  (0),          \
                                                  0)         \
                 * (2048))                                     \
                / TEMPSENSOR_CAL_VREFANALOG);

	b = (int32_t) *TEMPSENSOR_CAL1_ADDR;
	c = (int32_t)(TEMPSENSOR_CAL2_TEMP - TEMPSENSOR_CAL1_TEMP);
	d = (int32_t)((int32_t)*TEMPSENSOR_CAL2_ADDR - (int32_t)*TEMPSENSOR_CAL1_ADDR);
	e = TEMPSENSOR_CAL1_TEMP;

//	printf("%ld  %ld  %ld  %ld  %ld\n", a,b,c,d,e);
	r = ( (a - b) * c * 10 ) / d + e * 10;
	printf("r = %ld\n", r);
}

void fff1()
{
	int a, b;

		b = ADC_DMA_Buffer[INTR_TMPR];
		qqw(b);
		a = __LL_ADC_CALC_TEMPERATURE(1650, 2048, 0);
		printf("%d\n", a);
		a = __LL_ADC_CALC_TEMPERATURE(b, 2048, 0);
		printf("a = %d\n", a);
}
