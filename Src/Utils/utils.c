/*
 * Utils.c
 *
 *  Created on: Sep 10, 2020
 *      Author: avinoam.danieli
 */




#include "main.h"
#include "cmsis_os.h"

#include <stdio.h>
#include <string.h>
#include "defs.h"
#include "CLI.h"
#include "CLIcmnds.h"
#include "uart.h"

/*******************************************************************************
*	Reset
*	CLI
*
*******************************************************************************/

int32_t reset(CLI_command_T* command)
{

   if (command->ArgType == GETPAR)
   {
	  if (gettxbuffer() == FALSE)
      {
 		 errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
         return -3;
      }
	  snprintf (txbuff, TXMSG_MAX_LEN, "%s \r\n",command->sCommand);
	  txbuff_semaphor = 1;
	  sendtxbuffer();
	  HAL_NVIC_SystemReset();
	  return TRUE;
   }
   return 1;
}
//IWDG_HandleTypeDef hiwdg;
//void MY_IWDG_Init(void)
//{
//
//  /* USER CODE BEGIN IWDG_Init 0 */
//
//  /* USER CODE END IWDG_Init 0 */
//
//  /* USER CODE BEGIN IWDG_Init 1 */
//
//  /* USER CODE END IWDG_Init 1 */
//  hiwdg.Instance = IWDG;
//  hiwdg.Init.Prescaler = IWDG_PRESCALER_32;
//  hiwdg.Init.Reload = 2500;
//  if (HAL_IWDG_Init(&hiwdg) != HAL_OK)
//  {
//    Error_Handler();
//  }
//  /* USER CODE BEGIN IWDG_Init 2 */
//
//  /* USER CODE END IWDG_Init 2 */
//
//}

void MX_IWDG_Init(void);
TaskHandle_t xTaskToNotifyIWDG;
/*******************************************************************************
*	IWDG_en
*	CLI
*
*******************************************************************************/
int32_t IWDG_en(CLI_command_T* command)
{
	int32_t stam;
	static int32_t IWDGS = 0;

	if ( command->ArgType == GETPAR)
  	{
			if (gettxbuffer() == FALSE)
			{
			   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			   return -9;
			}
			sprintf (txbuff, "%s=%ld \r\n",command->sCommand, IWDGS);
			txbuff_semaphor = 1;
			sendtxbuffer();
			return TRUE;
		}
   else if ( command->ArgType == SETPAR)
   {
	   sscanf(command->sParameter, "%ld", (int32_t *)&stam);
	   if (stam == 1)
	   {
	   	if (xTaskToNotifyIWDG != NULL)
	   	{
	   		xTaskNotifyGive( xTaskToNotifyIWDG );
	   		MX_IWDG_Init();
	   		IWDGS = 1;
	   	}
	   }

		if (gettxbuffer() == FALSE)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		   return -3;
		}
    	snprintf (txbuff, TXMSG_MAX_LEN, "%s=%ld\r\n", command->sCommand, IWDGS);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   }
   return 1;
}

/*******************************************************************************
*	IWDGTask
*	Task
*
*******************************************************************************/
void IWDGTask(void *argument)
{
	xTaskToNotifyIWDG = xTaskGetCurrentTaskHandle();										// for ISR
	ulTaskNotifyTake( pdTRUE, portMAX_DELAY );												// wait for acknowledge

	for(;;)
	{
		HAL_IWDG_Refresh(&hiwdg);
		vTaskDelay(1000);
	}
}

/*******************************************************************************
*	tasklist
*******************************************************************************/
int32_t tasklist(CLI_command_T* command)
{

	static char cStringBuffer[500];
	const static char dash[] = {"-------------------------------------------------------------\n"};
	const static char head[] = {"Name\t\tState\tPrio\tStack\n"};

	vTaskList( cStringBuffer );
//	printf( "-------------------------------------------------------------\n" );
//	printf( "Name\t\t\tState\tPrio\tStack\n");
//	printf( cStringBuffer );
	if ( command->ArgType == GETPAR)
  	{
			if (gettxbuffer() == FALSE)
			{
			   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			   return -9;
			}
			snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d \r\n",command->sCommand, strlen(cStringBuffer));
			HAL_UART_Transmit_DMA(&huart3, (uint8_t *)txbuff, strlen(txbuff));
			vTaskDelay(100);
			HAL_UART_Transmit_DMA(&huart3, (uint8_t *)dash, strlen(dash));
			vTaskDelay(10);
			HAL_UART_Transmit_DMA(&huart3, (uint8_t *)head, strlen(head));
			vTaskDelay(10);
			HAL_UART_Transmit_DMA(&huart3, (uint8_t *)dash, strlen(dash));
			vTaskDelay(10);
			HAL_UART_Transmit_DMA(&huart3, (uint8_t *)cStringBuffer, strlen(cStringBuffer));
			return TRUE;
	}
	return TRUE;
}

/*******************************************************************************
*	run time status
*	CLI
*
*******************************************************************************/

int32_t rts(CLI_command_T* command)
{

	static char cStringBuffer[500];
	const static char dash[] = {"-------------------------------------------------------------\n"};
	const static char head[] = {"Task\t\tAbs\t\t%\n"};

	/* Generate a text table from the run-time stats. This must fit into the cStringBuffer array. */
	vTaskGetRunTimeStats( cStringBuffer );
	/* Print out column headings for the run-time stats table. */
	//printf( "\nTask\t\t\tAbs\t\t\t%%\n" );
	//printf( "-------------------------------------------------------------\n" );
	/* Print out the run-time stats themselves. The table of data contains
	multiple lines, so the vPrintMultipleLines() function is called instead of
	calling printf() directly. vPrintMultipleLines() simply calls printf() on
	each line individually, to ensure the line buffering works as expected. */
//		vPrintMultipleLines( cStringBuffer );
	//printf( cStringBuffer );
	if ( command->ArgType == GETPAR)
  	{
			if (gettxbuffer() == FALSE)
			{
			   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			   return -9;
			}
			snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d \r\n",command->sCommand, strlen(cStringBuffer));
			HAL_UART_Transmit_DMA(&huart3, (uint8_t *)txbuff, strlen(txbuff));
			vTaskDelay(100);
			HAL_UART_Transmit_DMA(&huart3, (uint8_t *)dash, strlen(dash));
			vTaskDelay(10);
			HAL_UART_Transmit_DMA(&huart3, (uint8_t *)head, strlen(head));
			vTaskDelay(10);
			HAL_UART_Transmit_DMA(&huart3, (uint8_t *)dash, strlen(dash));
			vTaskDelay(10);
			HAL_UART_Transmit_DMA(&huart3, (uint8_t *)cStringBuffer, strlen(cStringBuffer));
			return TRUE;
	}
	return TRUE;
}

/*******************************************************************************
*	run time status reset
*	CLI
*
*******************************************************************************/
typedef StaticTask_t osStaticThreadDef_t;
extern osStaticThreadDef_t SOMCOMMTaskControlBlock;
extern osStaticThreadDef_t CLITaskControlBlock;
extern osStaticThreadDef_t LEDsTaskControlBlock;
extern osStaticThreadDef_t BoardInitTaskControlBlock;
extern osStaticThreadDef_t PLCTaskControlBlock;
extern osStaticThreadDef_t PBTaskControlBlock;
extern osStaticThreadDef_t IWDGControlBlock;

int32_t rrts(CLI_command_T* command)
{

	if ( command->ArgType == GETPAR)
  	{
			if (gettxbuffer() == FALSE)
			{
			   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			   return -9;
			}
			snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d \r\n",command->sCommand, 0);
			SOMCOMMTaskControlBlock.ulDummy16 = 0;
			 CLITaskControlBlock.ulDummy16 = 0;;
			 LEDsTaskControlBlock.ulDummy16 = 0;;
			 BoardInitTaskControlBlock.ulDummy16 = 0;;
			 PLCTaskControlBlock.ulDummy16 = 0;;
			 PBTaskControlBlock.ulDummy16 = 0;;
			 IWDGControlBlock.ulDummy16 = 0;;
			__HAL_TIM_SET_COUNTER(&htim5, 0);
			return TRUE;
	}
	return TRUE;
}


/*******************************************************************************
*	message
*	CLI
*
*******************************************************************************/

int32_t message(CLI_command_T* command)
{
	const static char MSG[] = {"Hello Avinoam\n"};

	if ( command->ArgType == GETPAR)
  	{
			if (gettxbuffer() == FALSE)
			{
			   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			   return -9;
			}
			snprintf (txbuff, TXMSG_MAX_LEN, "%s=%s \r\n",command->sCommand, MSG);
			txbuff_semaphor = 1;
			sendtxbuffer();
			printf(MSG);
			return TRUE;
	}
	return TRUE;
}


/*******************************************************************************
*	bootloader
*	CLI
*
*******************************************************************************/

int32_t bootloader(CLI_command_T* command)
{
	void (*SysMemBootJump)(void);

	if ( (command->ArgType == GETPAR) || ( command->ArgType == SETPAR) )
  	{
		if (gettxbuffer() == FALSE)
		{
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			return -9;
		}
		snprintf (txbuff, TXMSG_MAX_LEN, "%s=%s \r\n",command->sCommand, " Go ");
		txbuff_semaphor = 1;
		sendtxbuffer();
		vTaskDelay(10);

		HAL_RCC_DeInit();
      SysMemBootJump = (void (*)(void)) (*((uint32_t *)(0x1fff0000 + 4)));
/**
* Step: Disable systick timer and reset it to default values
*/
		SysTick->CTRL = 0;
		SysTick->LOAD = 0;
		SysTick->VAL = 0;
		__disable_irq();
/**
* Step: Remap system memory to address 0x0000 0000 in address space
*       For each family registers may be different.
*       Check reference manual for each family.
*
*       For STM32F4xx, MEMRMP register in SYSCFG is used (bits[1:0])
*       For STM32F0xx, CFGR1 register in SYSCFG is used (bits[1:0])
*       For others, check family reference manual
*/
//Remap by hand...
		SYSCFG->MEMRMP = 0x01;

/**
  * Step: Set main stack pointer.
  *       This step must be done last otherwise local variables in this function
  *       don't have proper value since stack pointer is located on different position
  *
  *       Set direct address location which specifies stack pointer in SRAM location
*/
  		__set_MSP(*(uint32_t *)0x1fff0000);
		SysMemBootJump();

		return TRUE;
	}
	return TRUE;
}

