/*
 * cmdpower.c
 *
 *  Created on: Aug 12, 2020
 *      Author: avinoam.danieli
 */

#include "main.h"
#include "cmsis_os.h"

#include <stdio.h>
#include <string.h>
#include "defs.h"
#include "CLI.h"
#include "BoardInit.h"
#include "uart.h"
#include "clicmnds.h"
#include "BoardInit.h"

/*******************************************************************************
*	prechrg_en
*
*******************************************************************************/

int32_t prechrg_en(CLI_command_T* command)
{
   GPIO_PinState stam;
   int32_t var;

   if (command->ArgType == GETPAR)
   {
	  if (gettxbuffer() == FALSE)
      {
	     errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
         return -3;
      }
	  stam = HAL_GPIO_ReadPin(PRECHRG_EN_GPIO_Port, PRECHRG_EN_Pin);
	  snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n",command->sCommand, stam);
	  txbuff_semaphor = 1;
	  sendtxbuffer();
	  return TRUE;
   }
   else if ( command->ArgType == SETPAR)
   {
	   sscanf(command->sParameter, "%ld", (int32_t *)&stam);
	   if (stam == 1)
	   {
		   var = PrechargeOn();
	   }
	   else
	   {
		   PrechargeOff();
		   var = 0;
	   }

		if (gettxbuffer() == FALSE)
		{
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		   return -3;
		}
    	snprintf (txbuff, TXMSG_MAX_LEN, "%s=%ld\r\n", command->sCommand, var);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   }
   return 1;
}

/*******************************************************************************
*	vsw_en
*
*******************************************************************************/

int32_t vsw_en(CLI_command_T* command)
{
   GPIO_PinState stam;
   int32_t var;

   if (command->ArgType == GETPAR)
   {
	  if (gettxbuffer() == FALSE)
      {
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
            return -3;
      }
	  stam = HAL_GPIO_ReadPin(VSW_EN_GPIO_Port, VSW_EN_Pin);
	  snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n",command->sCommand, stam);
	  txbuff_semaphor = 1;
	  sendtxbuffer();
	  return TRUE;
   }
   else if ( command->ArgType == SETPAR)
   {
	   sscanf(command->sParameter, "%ld", (int32_t *)&stam);
	   if (stam == 1)
	   {
		   var = VSWOn();
	   }
	   else
	   {
		   VSWOff();
		   var = 0;
	   }

		if (gettxbuffer() == FALSE)
		{
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			return -3;
		}
    	snprintf (txbuff, TXMSG_MAX_LEN, "%s=%ld\r\n", command->sCommand, var);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   }
   return 1;
}
/*******************************************************************************
*	DCDC24V_en
*
*******************************************************************************/

int32_t DCDC24V_en(CLI_command_T* command)
{
   GPIO_PinState stam;
   int32_t var;

   if (command->ArgType == GETPAR)
   {
	  if (gettxbuffer() == FALSE)
      {
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
            return -3;
      }
	  stam = HAL_GPIO_ReadPin(DCDC24V_EN_GPIO_Port, DCDC24V_EN_Pin);
	  snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n",command->sCommand, stam);
	  txbuff_semaphor = 1;
	  sendtxbuffer();
	  return TRUE;
   }
   else if ( command->ArgType == SETPAR)
   {
	   sscanf(command->sParameter, "%ld", (int32_t *)&stam);
	   if (stam == 1)
	   {
		   var = DCDC24VOn();
	   }
	   else
	   {
		   DCDC24VOff();
		   var = 0;
	   }

		if (gettxbuffer() == FALSE)
		{
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			return -3;
		}
    	snprintf (txbuff, TXMSG_MAX_LEN, "%s=%ld\r\n", command->sCommand, var);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   }
   return 1;
}
/*******************************************************************************
*	P5V_en
*
*******************************************************************************/

int32_t P5V_en(CLI_command_T* command)
{
   GPIO_PinState stam;
   int32_t var;

   if (command->ArgType == GETPAR)
   {
	  if (gettxbuffer() == FALSE)
      {
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
            return -3;
      }
	  stam = HAL_GPIO_ReadPin(P5V_EN_GPIO_Port, P5V_EN_Pin);
	  snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n",command->sCommand, stam);
	  txbuff_semaphor = 1;
	  sendtxbuffer();
	  return TRUE;
   }
   else if ( command->ArgType == SETPAR)
   {
	   sscanf(command->sParameter, "%ld", (int32_t *)&stam);
	   if (stam == 1)
	   {
		   var = P5VOn();
	   }
	   else
	   {
		   P5VOff();
		   var = 0;
	   }

		if (gettxbuffer() == FALSE)
		{
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			return -3;
		}
    	snprintf (txbuff, TXMSG_MAX_LEN, "%s=%ld\r\n", command->sCommand, var);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   }
   return 1;
}
/*******************************************************************************
*	P24V_en
*
*******************************************************************************/

int32_t P24V_en(CLI_command_T* command)
{
   GPIO_PinState stam;
   int32_t var;

   if (command->ArgType == GETPAR)
   {
	  if (gettxbuffer() == FALSE)
      {
 		 errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
         return -3;
      }
	  stam = HAL_GPIO_ReadPin(P24V_EN_GPIO_Port, P24V_EN_Pin);
	  snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n",command->sCommand, stam);
	  txbuff_semaphor = 1;
	  sendtxbuffer();
	  return TRUE;
   }
   else if ( command->ArgType == SETPAR)
   {
	   sscanf(command->sParameter, "%ld", (int32_t *)&stam);
	   if (stam == 1)
	   {
		   var = P24VOn();
	   }
	   else
	   {
		   P24VOff();
		   var = 0;
	   }

		if (gettxbuffer() == FALSE)
		{
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			return -3;
		}
    	snprintf (txbuff, TXMSG_MAX_LEN, "%s=%ld\r\n", command->sCommand, var);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   }
   return 1;
}

/*******************************************************************************
*	Puller_en
*
*******************************************************************************/

int32_t Puller_en(CLI_command_T* command)
{
   GPIO_PinState stam;
   int32_t var;

   if (command->ArgType == GETPAR)
   {
	  if (gettxbuffer() == FALSE)
      {
 		 errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
         return -3;
      }
	  stam = HAL_GPIO_ReadPin(PULLER_EN_GPIO_Port, PULLER_EN_Pin);
	  snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n",command->sCommand, stam);
	  txbuff_semaphor = 1;
	  sendtxbuffer();
	  return TRUE;
   }
   else if ( command->ArgType == SETPAR)
   {
	   sscanf(command->sParameter, "%ld", (int32_t *)&stam);
	   if (stam == 1)
	   {
		   var = PullerOn();
	   }
	   else
	   {
		   PullerOff();
		   var = 0;
	   }

		if (gettxbuffer() == FALSE)
		{
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			return -3;
		}
    	snprintf (txbuff, TXMSG_MAX_LEN, "%s=%ld\r\n", command->sCommand, var);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   }
   return 1;
}

/*******************************************************************************
*	Charger_en
*
*******************************************************************************/

int32_t Charger_en(CLI_command_T* command)
{
   GPIO_PinState stam;
   int32_t var;

   if (command->ArgType == GETPAR)
   {
	  if (gettxbuffer() == FALSE)
      {
 		 errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
         return -3;
      }
	  stam = HAL_GPIO_ReadPin(CHARGE_DIS_GPIO_Port, CHARGE_DIS_Pin);
	  snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n",command->sCommand, stam);
	  txbuff_semaphor = 1;
	  sendtxbuffer();
	  return TRUE;
   }
   else if ( command->ArgType == SETPAR)
   {
	   sscanf(command->sParameter, "%ld", (int32_t *)&stam);
	   if (stam == 1)
	   {
		   var = ChargeOn();
	   }
	   else
	   {
		   ChargeOff();
		   var = 0;
	   }

		if (gettxbuffer() == FALSE)
		{
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			return -3;
		}
    	snprintf (txbuff, TXMSG_MAX_LEN, "%s=%ld\r\n", command->sCommand, var);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   }
   return 1;
}

/*******************************************************************************
*	brake_en
*
*******************************************************************************/

int32_t brake_en(CLI_command_T* command)
{
	//HAL_StatusTypeDef retval;
   int32_t var;

   if (command->ArgType == GETPAR)
   {
	  if (gettxbuffer() == FALSE)
      {
		 errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
         return -3;
      }
	  var = htim4.Instance->CCER & TIM_CCER_CC1E;
	  snprintf (txbuff, TXMSG_MAX_LEN, "%s=%ld\r\n",command->sCommand, var);
	  txbuff_semaphor = 1;
	  sendtxbuffer();
	  return TRUE;
   }
   else if ( command->ArgType == SETPAR)
   {
	   sscanf(command->sParameter, "%ld", (int32_t *)&var);
	   if (var == 1)
	   {
	   	BrakeOn();
	   }
	   else
	   {
	   	BrakeOff();
	   }
		if (gettxbuffer() == FALSE)
		{
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			return -3;
		}
    	snprintf (txbuff, TXMSG_MAX_LEN, "%s=%ld\r\n", command->sCommand, var);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   }
   return 1;
}

/*******************************************************************************
*	charge_en
*
*******************************************************************************/

int32_t charge_en(CLI_command_T* command)
{
   GPIO_PinState stam;

   if (command->ArgType == GETPAR)
   {
	  if (gettxbuffer() == FALSE)
      {
 		 errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
         return -3;
      }
	  stam = HAL_GPIO_ReadPin(CHARGE_DIS_GPIO_Port, CHARGE_DIS_Pin);		// enable
	  snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n",command->sCommand, stam);
	  txbuff_semaphor = 1;
	  sendtxbuffer();
	  return TRUE;
   }
   else if ( command->ArgType == SETPAR)
   {
	   sscanf(command->sParameter, "%ld", (int32_t *)&stam);
	   if (stam == 0)
	   {
		   ChargeOn();

	   }
	   else
	   {
		   ChargeOff();
	   }

		if (gettxbuffer() == FALSE)
		{
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			return -3;
		}
	   stam = HAL_GPIO_ReadPin(CHARGE_DIS_GPIO_Port, CHARGE_DIS_Pin);
    	snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n", command->sCommand, stam);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   }
   return 1;
}

/*******************************************************************************
*	drv_en
*	CLI
*
*******************************************************************************/

int32_t drv_en(CLI_command_T* command)
{
   GPIO_PinState stam;
   int32_t var;

   if (command->ArgType == GETPAR)
   {
	  if (gettxbuffer() == FALSE)
      {
 		 errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
         return -3;
      }
	  stam = HAL_GPIO_ReadPin(DRV_EN_GPIO_Port, DRV_EN_Pin);
	  snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n",command->sCommand, stam);
	  txbuff_semaphor = 1;
	  sendtxbuffer();
	  return TRUE;
   }
   else if ( command->ArgType == SETPAR)
   {
	   sscanf(command->sParameter, "%ld", (int32_t *)&stam);
	   if (stam == 1)
	   {
		   var = DriverOn();
	   }
	   else
	   {
	   	DriverOff();
		   var = 0;
	   }

		if (gettxbuffer() == FALSE)
		{
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			return -3;
		}
    	snprintf (txbuff, TXMSG_MAX_LEN, "%s=%ld\r\n", command->sCommand, var);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   }
   return 1;
}


/*******************************************************************************
*	locker_en
*	CLI
*
*******************************************************************************/

int32_t locker_en(CLI_command_T* command)
{
   GPIO_PinState stam;
   int32_t var;

   if (command->ArgType == GETPAR)
   {
	  if (gettxbuffer() == FALSE)
      {
 		 errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
         return -3;
      }
	  stam = HAL_GPIO_ReadPin(STP_EN_GPIO_Port, STP_EN_Pin);
	  snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n",command->sCommand, stam);
	  txbuff_semaphor = 1;
	  sendtxbuffer();
	  return TRUE;
   }
   else if ( command->ArgType == SETPAR)
   {
	   sscanf(command->sParameter, "%ld", (int32_t *)&stam);
	   if (stam == 1)
	   {
		   var = LockerOn();
	   }
	   else
	   {
	   	LockerOff();
		   var = 0;
	   }

		if (gettxbuffer() == FALSE)
		{
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			return -3;
		}
    	snprintf (txbuff, TXMSG_MAX_LEN, "%s=%ld\r\n", command->sCommand, var);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   }
   return 1;
}

/*******************************************************************************
*	som_rst
*	CLI
*
*******************************************************************************/

int32_t som_rst(CLI_command_T* command)
{
   GPIO_PinState stam;
   int32_t var;

   if (command->ArgType == GETPAR)
   {
	  if (gettxbuffer() == FALSE)
      {
 		 errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
         return -3;
      }
	  stam = HAL_GPIO_ReadPin(SOM_nMR_GPIO_Port, SOM_nMR_Pin);
	  snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n",command->sCommand, stam);
	  txbuff_semaphor = 1;
	  sendtxbuffer();
	  return TRUE;
   }
   else if ( command->ArgType == SETPAR)
   {
	   sscanf(command->sParameter, "%ld", &var);
	   if (var == 1)
	   {
	   	HAL_GPIO_WritePin(SOM_nMR_GPIO_Port, SOM_nMR_Pin, GPIO_PIN_RESET);
	   }
	   else
	   {
	   	HAL_GPIO_WritePin(SOM_nMR_GPIO_Port, SOM_nMR_Pin, GPIO_PIN_SET);
	   }

		if (gettxbuffer() == FALSE)
		{
			errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
			return -3;
		}
    	snprintf (txbuff, TXMSG_MAX_LEN, "%s=%ld\r\n", command->sCommand, var);
		txbuff_semaphor = 1;
		sendtxbuffer();
		return TRUE;
   }
   return 1;
}

/*******************************************************************************
*	getkey
*
*******************************************************************************/

int32_t getkey(CLI_command_T* command)
{
   GPIO_PinState stam;

   if (command->ArgType == GETPAR)
   {
	  if (gettxbuffer() == FALSE)
      {
	     errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
         return -3;
      }
	  stam = HAL_GPIO_ReadPin(KEY_MCU_GPIO_Port, KEY_MCU_Pin);
	  snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n",command->sCommand, stam);
	  txbuff_semaphor = 1;
	  sendtxbuffer();
	  return TRUE;
   }
   return 1;
}


/*******************************************************************************
*	getsto
*
*******************************************************************************/

int32_t getsto(CLI_command_T* command)
{
   GPIO_PinState stam;

   if (command->ArgType == GETPAR)
   {
	  if (gettxbuffer() == FALSE)
      {
	     errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
         return -3;
      }
	  stam = HAL_GPIO_ReadPin(STO_GPIO_Port, STO_Pin);
	  snprintf (txbuff, TXMSG_MAX_LEN, "%s=%d\r\n",command->sCommand, stam);
	  txbuff_semaphor = 1;
	  sendtxbuffer();
	  return TRUE;
   }
   return 1;
}



