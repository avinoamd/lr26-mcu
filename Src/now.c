/*
 * now.c
 *
 *  Created on: Nov 5, 2020
 *      Author: avinoam.danieli
 */

/*
 * startup
adcdat3a[0]	uint16_t	3309		internal battery voltage
adcdat3a[1]	uint16_t	1315		battery 1/50
adcdat3a[2]	uint16_t	30			VSW 1/50
adcdat3a[3]	uint16_t	1543		internal temperature	{ ((sense / 2) - 0.76 ) / 2.5 } + 25
adcdat3a[4]	uint16_t	610		vcharge
adcdat3a[5]	uint16_t	16			Imon
adcdat3a[6]	uint16_t	2430		internal reference	1.201V
adcdat3a[7]	uint16_t	82			P24V
adcdat3a[8]	uint16_t	3503		BRKCURR
adcdat3a[9]	uint16_t	3311		internal battery voltage
adcdat3a[10]	uint16_t	584	P5V
adcdat3a[11]	uint16_t	6		Vbrake

on
adcdat3a[0]	uint16_t	3315
adcdat3a[1]	uint16_t	1321
adcdat3a[2]	uint16_t	1317
adcdat3a[3]	uint16_t	1503
adcdat3a[4]	uint16_t	2
adcdat3a[5]	uint16_t	37
adcdat3a[6]	uint16_t	2412
adcdat3a[7]	uint16_t	1728
adcdat3a[8]	uint16_t	4095
adcdat3a[9]	uint16_t	3315
adcdat3a[10]	uint16_t	2047
adcdat3a[11]	uint16_t	61
*/
