/*
 * shift.c
 *
 *  Created on: 7 Sep 2020
 *      Author: avinoam.danieli
 */

#include "main.h"
#include "cmsis_os.h"
#include "defs.h"
#include <stdio.h>
#include <string.h>

typedef union {
	uint64_t ul64;
	uint8_t  uc[8];
} SHFTREG;
uint64_t mask;

SHFTREG lo = (SHFTREG){0x0987654321FEDCBA};
SHFTREG hi = (SHFTREG)0xA5C35A3CDEFA1234;
//SHFTREG hio, loo;

int32_t shftreg(int32_t shft, uint16_t num)
{
	if ( (shft > 128) || (num == 0) )
		return TRUE;
	if ( (shft == 0) );
	else
	{
		if (shft > 63)
		{
			lo.ul64 = hi.ul64 >> (shft - 64);
			hi.ul64 = 0;
		}
		else
		{
			lo.ul64 = (lo.ul64 >> shft) | ( hi.ul64 << (64 - shft) );
			hi.ul64 = hi.ul64 >> shft;
		}
	}
	if (num < 64)
	{
		hi.ul64 = 0;
		mask = ~(0xffffffff << num);
		lo.ul64 &= mask;
	}
	else
	{
		if (num <= 128)
		{
			num -= 64;
			mask = ~(0xffffffff << num);
			hi.ul64 &= mask;
		}
	}
  	//printf("%llx    %llx", lo.ul64, hi.ul64);
	return TRUE;
}

void getcoils()
{
	static int32_t togg = 0;

	if (togg == 0)
		togg = 1;
	else
		togg = 0;

	lo = (SHFTREG){0};
	hi = (SHFTREG){0};
	hi.ul64 |= ( HAL_GPIO_ReadPin(STO_GPIO_Port, STO_Pin) );    								// 64
	hi.ul64 |= ( HAL_GPIO_ReadPin(SAFETY_ALERT_GPIO_Port, SAFETY_ALERT_Pin) << 1 );    	// 65
	lo.ul64 |= ((uint64_t)( HAL_GPIO_ReadPin(PBtoMCU_GPIO_Port, PBtoMCU_Pin)) << 33 );    				//
	lo.ul64 |= ((uint64_t)( HAL_GPIO_ReadPin(P3V3_PG_GPIO_Port, P3V3_PG_Pin)) << 34 );    				//

	lo.ul64 |= ((uint64_t)( 1 ) << 35 );    				//
	lo.ul64 |= ((uint64_t)( HAL_GPIO_ReadPin(P3V3_PG_GPIO_Port, P24V_PG_Pin)) << 36 );    				//
	lo.ul64 |= ((uint64_t)( HAL_GPIO_ReadPin(DRV_EN_GPIO_Port, GPIO_PIN_3)) << 38 );    				//
	lo.ul64 |= ((uint64_t)( HAL_GPIO_ReadPin(PULLER_EN_GPIO_Port, PULLER_EN_Pin)) << 39 );    				//
	lo.ul64 |= ((uint64_t)( HAL_GPIO_ReadPin(STP_EN_GPIO_Port, STP_EN_Pin)) << 40 );    				//

	lo.ul64 |= ((uint64_t)( HAL_GPIO_ReadPin(KEY_MCU_GPIO_Port, KEY_MCU_Pin)) << 53 );    				//
	hi.ul64 |= (1 << 8) | (1 << 10) | (1 << 13) | (1 << 15);
	hi.ul64 |= togg << 12;


/*
 * #define DRV_EN_Pin GPIO_PIN_3
#define DRV_EN_GPIO_Port GPIOD
#define PULLER_EN_Pin GPIO_PIN_4
#define PULLER_EN_GPIO_Port GPIOD
 * #define STP_EN_Pin GPIO_PIN_7
#define STP_EN_GPIO_Port GPIOD
 *
 * #define P24V_PG_Pin GPIO_PIN_13
#define P24V_PG_GPIO_Port GPIOF
 *
 * #define P3V3_PG_Pin GPIO_PIN_15
#define P3V3_PG_GPIO_Port GPIOF
 *
#define KEY_MCU_Pin GPIO_PIN_14
#define KEY_MCU_GPIO_Port GPIOG
#define PBtoMCU_Pin GPIO_PIN_15
#define PBtoMCU_GPIO_Port GPIOG
*/


}
