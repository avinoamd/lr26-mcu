/**
  ******************************************************************************
  * File Name          : UART.c
  * Date               :
  * Description        :
  *
  ******************************************************************************
  *
  * COPYRIGHT(c) 2014 V-Gen
  *
  *
  *
  ******************************************************************************
  */

#include "main.h"
#include "cmsis_os.h"
#include <stdio.h>
#include <string.h>
#include "stm32f4xx_hal.h"
#include "uart.h"

char txbuff[TXMSG_MAX_LEN];				// tx buffer for CLI
int32_t txbuff_semaphor = 0;				// semaphor
char txebuff[TXMSG_MAX_LEN];				// tx buffer for CLI errors
char msg[MSG_MAX_LEN];						// rx buffer for CLI

TaskHandle_t xTaskToNotifyU5Rx;

/*******************************************************************************
*	user callback
*	This will work from DMA, and Cyclic, IT
*
*******************************************************************************/
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	if (huart == &huart5)
	{
		if( xTaskToNotifyU5Rx != NULL )
		{
			vTaskNotifyGiveFromISR( xTaskToNotifyU5Rx, &xHigherPriorityTaskWoken );
			portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
		}
	}
}


/*******************************************************************************
*	user callback
*******************************************************************************/
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	if (huart == &huart3)
	{
		huart->gState = HAL_UART_STATE_READY;
		txbuff_semaphor = 0;								// release the CLI uart buffer
	}
}

void UART_EndRxTransfer(UART_HandleTypeDef *huart);
int ovfcnt;
int ovfcntBCR, ovfcntTRM;
void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
  /* Prevent unused argument(s) compilation warning */
	if (huart == &huart4)		// PLC
	{
		ovfcnt ++;
//		  CLEAR_BIT(huart->Instance->CR1, (USART_SR_PE));
//		  CLEAR_BIT(huart->Instance->CR1, (USART_SR_FE));
//		  CLEAR_BIT(huart->Instance->CR1, (USART_SR_NE));
//		  CLEAR_BIT(huart->Instance->CR1, (USART_SR_ORE));
	}
	else if (huart == &huart3)		// TERMINAL
	{
		ovfcntTRM ++;
//		  CLEAR_BIT(huart->Instance->CR1, (USART_SR_PE));
//		  CLEAR_BIT(huart->Instance->CR1, (USART_SR_FE));
//		  CLEAR_BIT(huart->Instance->CR1, (USART_SR_NE));
//		  CLEAR_BIT(huart->Instance->CR1, (USART_SR_ORE));
	}
	else if (huart == &huart5)		// BAR CODE READER
	{
		ovfcntBCR ++;
//		  CLEAR_BIT(huart->Instance->CR1, (USART_SR_PE));
//		  CLEAR_BIT(huart->Instance->CR1, (USART_SR_FE));
//		  CLEAR_BIT(huart->Instance->CR1, (USART_SR_NE));
//		  CLEAR_BIT(huart->Instance->CR1, (USART_SR_ORE));
	}
	  /* NOTE: This function should not be modified, when the callback is needed,
           the HAL_UART_ErrorCallback could be implemented in the user file
   */
}



